-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2018 at 03:13 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `adventure`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_head_section`
--

CREATE TABLE `about_head_section` (
  `id` int(11) NOT NULL,
  `heading` text NOT NULL,
  `paragraph` text NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_head_section`
--

INSERT INTO `about_head_section` (`id`, `heading`, `paragraph`, `image`) VALUES
(3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,', 'https://www.youtube.com/v/EU7PRmCpx-0&list/PLillGF-RfqbYhQsN5WMXy6VsDMKGadrJ-');

-- --------------------------------------------------------

--
-- Table structure for table `about_history`
--

CREATE TABLE `about_history` (
  `id` int(11) NOT NULL,
  `year` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `paragraph` text NOT NULL,
  `history_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_history`
--

INSERT INTO `about_history` (`id`, `year`, `name`, `paragraph`, `history_image`) VALUES
(8, '2017', 'New possibilities', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', 'Layer-1111.jpg'),
(9, '2016', 'Global Scale', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', 'Layer-13_(1).jpg'),
(10, '2014', 'New branch', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', 'Layer-30_(2)11.jpg'),
(11, '2012', 'First award', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', 'Layer-29_(1).jpg'),
(12, '2008', 'Investments', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo', 'Layer-321.jpg'),
(13, '2002', 'FOUNDED', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.', 'Layer-7-768x5121.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `about_license`
--

CREATE TABLE `about_license` (
  `id` int(11) NOT NULL,
  `heading` text NOT NULL,
  `logo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_license`
--

INSERT INTO `about_license` (`id`, `heading`, `logo`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod', 'Layer-51011.jpg'),
(2, '', 'Layer-5281.jpg'),
(3, '', 'Layer-5231.jpg'),
(4, '', 'Layer-5181.jpg'),
(5, '', 'Layer-5101.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `about_partners`
--

CREATE TABLE `about_partners` (
  `id` int(11) NOT NULL,
  `logo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_partners`
--

INSERT INTO `about_partners` (`id`, `logo`) VALUES
(3, '3docean-light-background11131.png'),
(5, '3docean-light-background321.png'),
(9, '3docean-light-background32.png'),
(10, '3docean-light-background26.png'),
(12, '3docean-light-background12.png'),
(13, '3docean-light-background5.png'),
(14, '3docean-light-background3211.png'),
(15, '3docean-light-background1114.png');

-- --------------------------------------------------------

--
-- Table structure for table `about_value_section`
--

CREATE TABLE `about_value_section` (
  `id` int(11) NOT NULL,
  `heading` text NOT NULL,
  `paragraph` text NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_value_section`
--

INSERT INTO `about_value_section` (`id`, `heading`, `paragraph`, `image`) VALUES
(5, 'Honesty', 'Lorem ipsum dolor sit amet, consectetur\r\nadipiscing elit, sed do eiusmod tempor\r\nincididunt ut labore et dolore magna aliqua.', 'honesty.PNG'),
(6, 'Tolerance', 'Lorem ipsum dolor sit amet, consectetur\r\nadipiscing elit, sed do eiusmod tempor\r\nincididunt ut labore et dolore magna aliqua.', 'tolrance.PNG'),
(7, 'Transparency', 'Lorem ipsum dolor sit amet, consectetur\r\nadipiscing elit, sed do eiusmod tempor\r\nincididunt ut labor', 'transparency.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `addcontect`
--

CREATE TABLE `addcontect` (
  `id` int(11) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addcontect`
--

INSERT INTO `addcontect` (`id`, `phone`, `email`, `address`) VALUES
(1, '+1 212-736-31004565465', 'usa@example.com', '350 5th Ave, NY 10118');

-- --------------------------------------------------------

--
-- Table structure for table `careers_section_1`
--

CREATE TABLE `careers_section_1` (
  `sec_in_id` int(11) NOT NULL,
  `sec_in_h` varchar(255) NOT NULL,
  `sec_in_p` text NOT NULL,
  `sec_in_video` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `careers_section_1`
--

INSERT INTO `careers_section_1` (`sec_in_id`, `sec_in_h`, `sec_in_p`, `sec_in_video`, `created_at`, `updated_at`, `status`) VALUES
(1, 'A career that fits your calling', 'Problem solvers and creative thinkers. Engineers and new business builders. Put your talents to use where opportunities are limitless and every day makes a difference.Whether you’re an experienced professional or a recent graduate, working with Advice could be a challenging and rewarding next step in your career.\r\n', 'https://www.youtube.com/embed/M7lc1UVf-VE?rel=0&showinfo=0&autoplay=1', '2018-10-02 08:59:52', '2018-10-08 15:37:43', 0);

-- --------------------------------------------------------

--
-- Table structure for table `careers_vacancies_section`
--

CREATE TABLE `careers_vacancies_section` (
  `id` int(11) NOT NULL,
  `top_heading` varchar(255) NOT NULL,
  `inner_heading` varchar(255) NOT NULL,
  `paragraph` text NOT NULL,
  `location` varchar(255) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `careers_vacancies_section`
--

INSERT INTO `careers_vacancies_section` (`id`, `top_heading`, `inner_heading`, `paragraph`, `location`, `create_at`, `update_at`, `status`) VALUES
(1, 'Environmental Field Consultant', 'Energy & Resources', 'Our people are our secret to delivering excellence as promised! We have fostered a culture promoting progressive thinking to tackle complex engineering challenges transforming creative ideas into reality! We are committed to supporting your career goals by providing professional development and training, the opportunity to work with respected industry thought leaders, while at the same time encouraging a healthy work life balance. Our benefits and perks are competitive because we want to see a smile on your face as we change the world together.', 'Melbourne', '2018-10-09 10:28:42', '0000-00-00 00:00:00', 0),
(3, 'Tax Consulting', 'Tax Consulting', 'The Tax Consultant will apply basic tax and accounting concepts to prepare income tax provisions and federal and state tax compliance for the Operating Companies. This includes working in tax systems and preparing analyses and workpapers to calculate book/tax differences including depreciation.', 'New York', '2018-10-09 13:57:19', '2018-10-09 14:11:52', 0),
(4, 'Real World Evidence Research Consultant', 'Real Estate Consulting', 'Advice Consultants deliver high-quality observational research and evidence generation projects to clients. Consultants possess unstructured problem solving skills as well as strong analytic, synthesis and communication skills. Consultants may work on multiple projects simultaneously.', 'New York', '2018-10-09 14:12:52', '0000-00-00 00:00:00', 0),
(5, 'Quidem possimus a nesciunt magnam mollit id aliqua Laborum quam nulla', 'Esse ab mollit aut reiciendis minim sunt reprehenderit perferendis consectetur', 'Nostrud nulla obcaecati exercitation incidunt voluptas aut voluptatem voluptatem ab', 'Laborum Sapiente quia ab sed rerum adipisicing non quos dolore mollitia velit tenetur ullam', '2018-10-10 15:23:28', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `expert_team_categories`
--

CREATE TABLE `expert_team_categories` (
  `expert_team_categories_id` int(11) NOT NULL,
  `expert_team_categories_title` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expert_team_categories`
--

INSERT INTO `expert_team_categories` (`expert_team_categories_id`, `expert_team_categories_title`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Advanced Analytics', '2018-10-06 13:42:42', '0000-00-00 00:00:00', 0),
(2, 'Analytics', '2018-10-06 13:42:42', '0000-00-00 00:00:00', 0),
(3, 'Energy', '2018-10-06 13:42:42', '0000-00-00 00:00:00', 0),
(4, 'Finance', '2018-10-06 13:42:42', '0000-00-00 00:00:00', 0),
(5, 'Oil', '2018-10-06 13:42:42', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `head_office`
--

CREATE TABLE `head_office` (
  `id` int(11) NOT NULL,
  `office_name` varchar(255) NOT NULL,
  `office_country` varchar(255) NOT NULL,
  `office_day_from` varchar(255) NOT NULL,
  `office_time_from` varchar(255) NOT NULL,
  `office_day_to` varchar(255) NOT NULL,
  `office_time_to` varchar(255) NOT NULL,
  `office_email` varchar(255) NOT NULL,
  `office_number` varchar(255) NOT NULL,
  `office_address` varchar(255) NOT NULL,
  `offset` float DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `head_office`
--

INSERT INTO `head_office` (`id`, `office_name`, `office_country`, `office_day_from`, `office_time_from`, `office_day_to`, `office_time_to`, `office_email`, `office_number`, `office_address`, `offset`) VALUES
(30, 'india', 'india', '16', '00:25', '7', '21:16', 'merocahan@mailinator.net', '304', 'New dehli', 3),
(31, 'London', 'London', '16', '20:10', '23', '23:36', 'molu@mailinator.com', '955', 'Greenwich', 1),
(32, 'America', 'America', '2', '02:57', '4', '17:01', 'kutiparoz@mailinator.net', '569', 'New York', 2),
(33, 'Europe Office', 'London', 'Mon', '09:00', 'Sat', '09:00', 'london@example.com', '+1 212-736-3100', '350 5th BackSide str, London 10118', 5.5),
(34, 'US Office', 'New Yourk', 'Mon', '09:00', 'sat', '21:00', 'usa@example.com', '+1 212-736-3100', '350 5th Ave, NY 10118', 1);

-- --------------------------------------------------------

--
-- Table structure for table `head_section`
--

CREATE TABLE `head_section` (
  `id` int(11) NOT NULL,
  `head_section_h` varchar(255) NOT NULL,
  `head_section_p` text NOT NULL,
  `head_section_image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `head_section`
--

INSERT INTO `head_section` (`id`, `head_section_h`, `head_section_p`, `head_section_image`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Contact Us', 'Blockchain is moving fast. Our teams are expanding. Please contact us if you would like to provide expertise in any of the fields we operate in and join our global project.', 'ContactUs.jpg', '2018-10-05 10:10:35', '2018-10-05 10:52:35', 0),
(2, 'About us', 'We Are a Global Partnership\r\n', 'Layer-62.jpg', '2018-10-05 11:17:48', '2018-10-05 20:15:04', 0),
(3, 'Careers', '', '1.jpg', '2018-10-05 11:20:18', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `home_head_section`
--

CREATE TABLE `home_head_section` (
  `id` int(11) NOT NULL,
  `home_head_section_h` varchar(255) NOT NULL,
  `home_head_section_text_1` varchar(255) NOT NULL,
  `home_head_section_image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_head_section`
--

INSERT INTO `home_head_section` (`id`, `home_head_section_h`, `home_head_section_text_1`, `home_head_section_image`, `created_at`, `updated_at`, `status`) VALUES
(1, '', 'Stay with us You gonna love it!', 'Background@2x5.png', '2018-10-05 07:12:54', '2018-10-17 17:48:03', 0),
(2, 'Posts', '', 'Layer-33_03.jpg', '2018-10-05 12:29:03', '2018-10-17 17:53:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `home_sections_heading`
--

CREATE TABLE `home_sections_heading` (
  `home_sec_id` int(11) NOT NULL,
  `sec_h` varchar(255) NOT NULL,
  `sec_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_sections_heading`
--

INSERT INTO `home_sections_heading` (`home_sec_id`, `sec_h`, `sec_image`) VALUES
(1, 'Why choose us ?', ''),
(2, 'Industries\r\n', 'Background12.png'),
(3, 'Which industries do we serve?\r\n', ''),
(4, 'Meet our expert team', ''),
(5, 'Our publications', ''),
(6, 'Careers', 'Layer-23_03.jpg'),
(7, 'How can we help?', 'Layer-171_03.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `home_section_1`
--

CREATE TABLE `home_section_1` (
  `sec_in_id` int(11) NOT NULL,
  `sec_in_h` varchar(255) NOT NULL,
  `sec_in_p` text NOT NULL,
  `sec_in_image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_section_1`
--

INSERT INTO `home_section_1` (`sec_in_id`, `sec_in_h`, `sec_in_p`, `sec_in_image`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Expertise', 'We believe in smart technologies. Our expertise and passion in Blockchain constantly push us forward to help you grow. Fuelled by your achievements, and challenged by progress.', 'enterprise21.PNG', '2018-10-02 08:59:52', '2018-10-09 11:32:41', 0),
(2, 'Vision', 'Changing the world is a strong claim. Molding the future while driving enterprise and growth is what we do. Help us make it happen.', 'target.png', '2018-10-02 08:59:52', '2018-10-02 12:13:42', 0),
(5, 'Industries', 'Operating already in a large number of sectors, we foresee ever-growing markets and industries that will benefit from blockchain solutions.', 'chess2.png', '2018-10-02 14:15:59', '2018-10-02 14:55:24', 0);

-- --------------------------------------------------------

--
-- Table structure for table `home_section_2_tabs`
--

CREATE TABLE `home_section_2_tabs` (
  `home_section_2_tab_id` int(11) NOT NULL,
  `home_section_2_tab_icon` varchar(255) NOT NULL,
  `home_section_2_tab_tittle` varchar(255) NOT NULL,
  `home_section_2_tab_web_link` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_section_2_tabs`
--

INSERT INTO `home_section_2_tabs` (`home_section_2_tab_id`, `home_section_2_tab_icon`, `home_section_2_tab_tittle`, `home_section_2_tab_web_link`, `created_at`, `updated_at`, `status`) VALUES
(1, 'blockchain.png', 'Venturex Labs', 'http://venturexlabs.com', '2018-10-03 07:54:07', '2018-10-04 15:04:48', 0),
(2, 'exchange.png', 'Venturex Wire', 'http://venturexlabs.com', '2018-10-03 07:54:07', '2018-10-04 15:09:56', 0),
(3, 'mining.png', 'Venturex Solution', 'http://www.venturexsolutions.com', '2018-10-03 08:26:34', '2018-10-04 15:09:12', 0),
(4, 'seo.png', 'Venturex  Digital', 'http://www.venturexdigital.com', '2018-10-03 08:26:34', '2018-10-04 15:09:37', 0),
(5, 'ICO.png', 'Ventcher', 'http://venturexlabs.com', '2018-10-03 08:26:34', '2018-10-04 15:10:02', 0),
(14, 'Group_104.png', 'Venchex', 'www.Venturexlab.com', '2018-10-04 14:20:33', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `home_section_2_tabs_data`
--

CREATE TABLE `home_section_2_tabs_data` (
  `home_section_2_tabs_data_id` int(11) NOT NULL,
  `home_section_2_tab_id` int(11) NOT NULL,
  `home_section_2_tabs_data_text` varchar(255) NOT NULL,
  `home_section_2_tabs_data_img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_section_2_tabs_data`
--

INSERT INTO `home_section_2_tabs_data` (`home_section_2_tabs_data_id`, `home_section_2_tab_id`, `home_section_2_tabs_data_text`, `home_section_2_tabs_data_img`) VALUES
(1, 1, 'Blockchain', 'Untitled-3.jpg'),
(2, 1, 'Applications', 'layer13e21dd.jpg'),
(3, 1, 'index', '151.jpg'),
(4, 1, 'Growth', 'Untitled-4.jpg'),
(5, 2, 'Cryptocurrencies', 'Untitled-5.jpg'),
(6, 2, 'Transactions', '1_3hyWN8UhcrL7P0Opbu7IQg1.jpeg'),
(7, 2, 'Security', 'Untitled-4.jpg'),
(11, 2, 'Blockchain', 'Layer-30_(2).jpg'),
(12, 3, 'Cryptomining,', 'Untitled-32.jpg'),
(13, 3, 'cryptocurrencies', 'Untitled-811.jpg'),
(14, 3, 'Hardware Solutions', 'capture121.PNG'),
(15, 3, 'Blockchain', 'layer13e21dd-175x1451.jpg'),
(16, 4, 'Digital Marketing', 'a502d50a30c02aa5cf1c182aa74638652111.png'),
(18, 4, 'SEO', 'capture1211.PNG'),
(19, 4, 'Web-Design', 'Capture321.PNG'),
(20, 4, 'Coding', 'Capture22.PNG'),
(21, 5, 'Exchange', 'download_(1).jpg'),
(22, 5, 'Tokens', 'layer13e21dd-175x14511.jpg'),
(23, 5, 'Fund Raising', 'capture1212.PNG'),
(24, 5, 'Blockchain', '1_3hyWN8UhcrL7P0Opbu7IQg2.jpeg'),
(25, 14, 'Exchange', 'layer13e21dd1.jpg'),
(26, 14, 'Trading', 'capture122.PNG'),
(27, 14, 'cryptocurrencies', 'Untitled-8111.jpg'),
(28, 14, 'Margin', 'images_(1).jpg');

-- --------------------------------------------------------

--
-- Table structure for table `home_section_3`
--

CREATE TABLE `home_section_3` (
  `home_section_3_id` int(11) NOT NULL,
  `home_section_3_img` varchar(255) NOT NULL,
  `home_section_3_h` varchar(255) NOT NULL,
  `home_section_3_p` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_section_3`
--

INSERT INTO `home_section_3` (`home_section_3_id`, `home_section_3_img`, `home_section_3_h`, `home_section_3_p`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Untitled-5.jpg', 'Financial Services', 'Optimising financial transactions\r\nData protection and legal operations', '2018-10-02 09:10:50', '2018-10-02 09:51:00', 0),
(2, 'Untitled-6.jpg', 'Social Services', 'Optimising the implementation of social causes through accuracy and transparency', '2018-10-02 09:10:50', '0000-00-00 00:00:00', 0),
(3, 'Untitled-821.jpg', 'E-commerce', 'Laying efficient electronic frameworks taking online businesses to the next level', '2018-10-02 09:10:50', '2018-10-02 15:42:35', 0),
(4, 'Untitled-31.jpg', 'Health', 'Ensuring security and protection of sensitive data in medical facilities', '2018-10-02 09:10:50', '2018-10-02 15:41:50', 0);

-- --------------------------------------------------------

--
-- Table structure for table `home_section_4`
--

CREATE TABLE `home_section_4` (
  `home_section_4_id` int(11) NOT NULL,
  `home_section_4_post_cat` varchar(255) NOT NULL,
  `home_section_4_img` varchar(255) NOT NULL,
  `home_section_4_name` varchar(255) NOT NULL,
  `home_section_4_role` varchar(255) NOT NULL,
  `home_section_4_location_title` varchar(255) NOT NULL,
  `home_section_4_location_url` text NOT NULL,
  `home_section_4_p` text NOT NULL,
  `home_section_4_link` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_section_4`
--

INSERT INTO `home_section_4` (`home_section_4_id`, `home_section_4_post_cat`, `home_section_4_img`, `home_section_4_name`, `home_section_4_role`, `home_section_4_location_title`, `home_section_4_location_url`, `home_section_4_p`, `home_section_4_link`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Insurance,Medical,USA,Products,Prediction', 'capture.PNG', 'Saeed Zuberi', 'CEO', 'London', 'https://www.google.com/maps/place/%20332%20Orchard%20Road,%20#07-303%20Tong%20Building,%20Singapore%202383862%20', 'The speed of implementation will become a competitive advantage in itself. Advice shows…', 'Advanced Analytics, Analytics, Big Data, Energy, Finance, Oil', '2018-10-02 10:11:15', '2018-10-06 18:15:10', 0),
(2, 'USA,Products,Prediction,Foods', 'alina.jpg', 'Anna Kendall', 'Client Manager', 'London', 'https://www.google.com/maps/place/%20332%20Orchard%20Road,%20#07-303%20Tong%20Building,%20Singapore%202383862%20', 'The speed of implementation will become a competitive advantage in itself. Advice shows…', 'Advanced Analytics, Analytics, Big Data, Energy, Finance, Oil', '2018-10-02 10:11:15', '2018-10-06 18:14:58', 0),
(6, 'Health,Insurance,Medical,USA', 'alina35.jpg', 'Hilary Klein', 'Senior Manager', 'Landon', 'https://www.google.com/maps/place/London,+UK/data=!4m2!3m1!1s0x47d8a00baf21de75:0x52963a5addd52a99?sa=X&ved=2ahUKEwjTsfeZ-undAhWjsaQKHfiPCh8Q8gEwAHoECAAQAQ', 'The speed of implementation will become a competitive advantage in itself. Advice shows…', 'Corporis sed ea ut in aut quis voluptas temporibus proident', '2018-10-03 07:08:14', '2018-10-06 18:14:47', 0),
(7, '', 'alina25.jpg', 'Jessica Black', 'Consultant', 'Melbourne', 'https://www.google.com/maps/place/Melbourne+VIC,+Australia/@-37.970154,144.492678,9z/data=!3m1!4b1!4m5!3m4!1s0x6ad646b5d2ba4df7:0x4045675218ccd90!8m2!3d-37.8136276!4d144.9630576', 'The speed of implementation will become a competitive advantage in itself. Advice shows…', 'Advanced Analytics, Analytics, Big Data, Energy, Finance, Oil', '2018-10-03 07:10:54', '2018-10-18 09:01:52', 0);

-- --------------------------------------------------------

--
-- Table structure for table `home_section_5_post`
--

CREATE TABLE `home_section_5_post` (
  `home_section_5_post_id` int(11) NOT NULL,
  `home_section_5_post_cat` varchar(255) NOT NULL,
  `home_section_5_post_title` varchar(255) NOT NULL,
  `home_section_5_post_writer` varchar(255) NOT NULL,
  `home_section_5_post_image` varchar(255) NOT NULL,
  `home_section_5_post_date` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_section_5_post`
--

INSERT INTO `home_section_5_post` (`home_section_5_post_id`, `home_section_5_post_cat`, `home_section_5_post_title`, `home_section_5_post_writer`, `home_section_5_post_image`, `home_section_5_post_date`, `created_at`, `updated_at`, `status`) VALUES
(4, 'Insurance,Medical,USA', 'Iste elit velit voluptatem dolorem dignissimos dolores aut sunt dolor possimus perspiciatis ut eos natus Nam laboriosam in dolor', 'Voluptates vel ut omnis', 'client1.jpg', 'Oct  06,  2018', '2018-10-05 17:53:03', '2018-10-06 11:22:50', 0),
(5, 'Health,Insurance,Medical,Advanced Analytics', 'Quisquam quis sunt sed ea exercitation ad reprehenderit officia aut quibusdam totam tempor qui quaerat', 'Et voluptas Nam porro dolor reiciendis sint asperiores aut sed rem magna labore nobis maiores ea', 'Doctar.jpg', 'Oct  06,  2018', '2018-10-05 17:54:04', '2018-10-06 11:22:42', 0),
(6, 'Health,Insurance,Medical,Advanced Analytics', 'What can we expect in China in 2017?', 'Vel et quis at ea natus possimus ex odio culpa proident adipisci nesciunt aliquid', 'a502d50a30c02aa5cf1c182aa746386533.png', 'Oct  06,  2018', '2018-10-05 18:49:46', '2018-10-08 11:26:48', 0),
(14, 'Insurance,Medical,USA,Foods', 'Fuga Modi sed officiis at in sunt', 'Voluptates', 'Capture322.PNG', 'Oct  12,  2018', '2018-10-12 08:17:41', '2018-10-12 16:59:45', 0),
(15, 'Medical,USA,Products,Prediction,Foods', 'Libero eius maxime delectus iusto dolore non eligendi aut vel molestiae ut laborum accusantium praesentium', 'Et dolor hic quis ut qui ut eius praesentium consectetur facere alias asperiores voluptas eos aperiam', '011.jpg', 'Oct  12,  2018', '2018-10-12 17:43:13', '0000-00-00 00:00:00', 0),
(16, 'Insurance,USA,Products,Prediction', 'Molestiae placeat assumenda dignissimos expedita odio modi molestiae magnam est amet magna sed nostrum quaerat omnis officia illum', 'Fugit ', 'alina71.jpg', 'Oct  12,  2018', '2018-10-12 17:46:54', '2018-10-12 17:47:05', 0),
(17, 'USA,Products,Prediction,Foods', 'Tempor saepe ut aut consectetur', 'Quia', 'a502d50a30c02aa5cf1c182aa7463865331.png', 'Oct  12,  2018', '2018-10-12 17:47:52', '0000-00-00 00:00:00', 0),
(18, 'Products,Prediction,Foods', 'Medicaid Managed Care Oversight Consultin', 'Brian Thoma', 'Capture32111.PNG', 'Oct  18,  2018', '2018-10-18 09:41:10', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `home_section_5_post_article`
--

CREATE TABLE `home_section_5_post_article` (
  `home_section_5_post_article_id` int(11) NOT NULL,
  `home_section_5_post_cat` varchar(255) NOT NULL,
  `home_section_5_post_article_title` varchar(255) NOT NULL,
  `home_section_5_post_article_writer` varchar(255) NOT NULL,
  `home_section_5_post_article_image` varchar(255) NOT NULL,
  `home_section_5_post_article_date` varchar(255) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_section_5_post_article`
--

INSERT INTO `home_section_5_post_article` (`home_section_5_post_article_id`, `home_section_5_post_cat`, `home_section_5_post_article_title`, `home_section_5_post_article_writer`, `home_section_5_post_article_image`, `home_section_5_post_article_date`, `create_at`, `update_at`, `status`) VALUES
(1, 'Health,Insurance,Medical,USA,Prediction,Food,Consumer ', 'Advice attended the 2nd Asian Marine Casualty Forum', 'Anna Kendall', 'client.jpg', 'Oct  06,  2018', '2018-10-06 08:00:14', '2018-10-06 14:59:44', 0),
(3, 'Health,Insurance,Medical,USA,Products,Prediction,Food,Consumer ,Advanced Analytics', 'Advice attended the 2nd Asian Marine Casualty Foru...', 'aliraza', 'Capture3211.PNG', 'Oct  06,  2018', '2018-10-06 15:07:33', '2018-10-08 11:12:33', 0);

-- --------------------------------------------------------

--
-- Table structure for table `home_section_5_post_categories`
--

CREATE TABLE `home_section_5_post_categories` (
  `home_section_5_post_categories_id` int(11) NOT NULL,
  `home_section_5_post_categorie_text` varchar(255) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_section_5_post_categories`
--

INSERT INTO `home_section_5_post_categories` (`home_section_5_post_categories_id`, `home_section_5_post_categorie_text`, `create_at`, `update_at`, `status`) VALUES
(8, 'Health', '2018-10-05 19:50:36', '0000-00-00 00:00:00', 0),
(9, 'Insurance', '2018-10-05 19:50:36', '0000-00-00 00:00:00', 0),
(10, 'Medical', '2018-10-05 19:50:36', '0000-00-00 00:00:00', 0),
(11, 'USA', '2018-10-05 19:50:36', '0000-00-00 00:00:00', 0),
(12, 'Products', '2018-10-05 19:50:36', '0000-00-00 00:00:00', 0),
(13, 'Prediction', '2018-10-05 19:50:36', '0000-00-00 00:00:00', 0),
(14, 'Foods', '2018-10-05 19:50:36', '2018-10-06 17:07:15', 0);

-- --------------------------------------------------------

--
-- Table structure for table `home_slider`
--

CREATE TABLE `home_slider` (
  `home_slider_id` int(11) NOT NULL,
  `home_slider_p` text NOT NULL,
  `home_slider_img` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home_slider`
--

INSERT INTO `home_slider` (`home_slider_id`, `home_slider_p`, `home_slider_img`, `name`, `role`, `country`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Since the implementation of Blockchain in our internal delivery service, the impact on data transmission, and security has been huge.\r\n', 'shutterstock_440517250-100x100.jpg', 'Nicolas Gonine', 'Manager', 'Paris', '2018-10-06 20:10:36', '0000-00-00 00:00:00', 0),
(2, 'Consectetur explicabo Est sed ducimus aliquip laborum labore eveniet id ut consequatur et fugit quidem perferendis', 'download_(2)2.jpg', 'Kaden Berger', 'Ut beatae numquam qui quisquam ab esse in autem ipsum', 'Mollit harum quia et esse accusamus quia eos quia elit sit velit mollit cum provident delectus', '2018-10-10 20:03:40', '2018-10-18 09:35:37', 0),
(3, 'Facere eu adipisci sed in cillum mollitia nostrud recusandae Sint impedit error quasi et', 'alina121.jpg', 'Irma Allison', 'Numquam in deserunt accusamus assumenda blanditiis adipisci', 'Amet aut repudiandae qui provident', '2018-10-10 20:04:32', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `index_footer`
--

CREATE TABLE `index_footer` (
  `index_footer_id` int(11) NOT NULL,
  `index_footer_text` text NOT NULL,
  `index_footer_newsletter` varchar(255) NOT NULL,
  `index_footer_linkedin` varchar(255) NOT NULL,
  `index_footer_twitter` varchar(255) NOT NULL,
  `index_footer_facebook` varchar(255) NOT NULL,
  `index_footer_power_by` varchar(255) NOT NULL,
  `index_footer_logo_image` varchar(255) NOT NULL,
  `index_footer_background_image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `index_footer`
--

INSERT INTO `index_footer` (`index_footer_id`, `index_footer_text`, `index_footer_newsletter`, `index_footer_linkedin`, `index_footer_twitter`, `index_footer_facebook`, `index_footer_power_by`, `index_footer_logo_image`, `index_footer_background_image`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam et ratione expedita vel, necessitatibus provident beatae!', 'NEWSLETTER', 'https://linkedin.com', 'https://twitter.com', 'https://facebook.com', 'Powered By AK-techZone', 'Logo2.png', 'Group18.png', '2018-10-02 08:51:06', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `index_navbar`
--

CREATE TABLE `index_navbar` (
  `index_navbar_id` int(11) NOT NULL,
  `index_navbar_info` varchar(255) NOT NULL,
  `index_navbar_phone` varchar(255) NOT NULL,
  `index_navbar_logo_image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `index_navbar`
--

INSERT INTO `index_navbar` (`index_navbar_id`, `index_navbar_info`, `index_navbar_phone`, `index_navbar_logo_image`, `created_at`, `updated_at`, `status`) VALUES
(1, ' info@venturexglobal.com', '+339 70 73 66 64', 'Logo210.png', '2018-10-02 08:18:27', '2018-10-05 16:08:21', 0);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `phone`, `message`) VALUES
(1, 'Fletcher Cardenas', 'xabu@mailinator.com', '+356-39-1184193', 'Eos eaque excepturi mollit molestiae excepteur fugiat autem fuga Ducimus aliquam aperiam'),
(2, 'Shea Dominguez', 'pihe@mailinator.net', '+633-59-5626083', 'Rerum aut neque voluptatum id consectetur et odit accusamus cum excepturi quaerat est quo excepturi dolore'),
(3, 'Katell Patterson', 'fukovybilu@mailinator.net', '+785-23-5204877', 'Sequi eligendi a dolorem ex quod dolor voluptatem minima velit quae assumenda in ut quos voluptate'),
(4, 'Ingrid Bolton', 'husi@mailinator.net', '+979-72-3022932', 'Aut libero totam do est officiis minim'),
(5, 'saad', 'admin1@gmail.com', 'ytutyu', 'yutuyt'),
(6, 'Harper Ingram', 'vyhi@mailinator.com', '+978-14-2852540', 'Eos omnis non ipsum velit molestias aut voluptas'),
(7, 'Graiden Albert', 'xakur@mailinator.net', '+174-41-5668246', 'Fuga Dolorum atque quod velit iure et laborum labore sed quisquam non facere libero amet laboriosam');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `subscriber_id` int(11) NOT NULL,
  `subscriber_email` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`subscriber_id`, `subscriber_email`, `created_at`, `updated_at`, `status`) VALUES
(1, 'aliraza@gmial.com', '2018-10-05 07:55:56', '0000-00-00 00:00:00', 0),
(2, 'ali@gmail.com', '2018-10-05 08:00:06', '0000-00-00 00:00:00', 0),
(3, 'ali@gmail.com', '2018-10-05 08:00:59', '0000-00-00 00:00:00', 0),
(4, 'a@gmail.com', '2018-10-05 08:03:25', '0000-00-00 00:00:00', 0),
(5, 'aliraza@gmail.com', '2018-10-12 13:41:08', '0000-00-00 00:00:00', 0),
(6, 'shah@gmail.com', '2018-10-12 13:41:59', '0000-00-00 00:00:00', 0),
(7, 'subscribe@gamail.com', '2018-10-12 13:56:49', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `twiter`
--

CREATE TABLE `twiter` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `twiter`
--

INSERT INTO `twiter` (`id`, `name`, `message`) VALUES
(2, 'https://t.co/d0k17zfdi1', 'Mollit et sit excepteur corrupti culpa delectus at nemo esse odit molestiae sequi tempore veritatis'),
(3, 'https://t.co/WqBZzsaqTD', 'Creepin\' it real with @placeitapp'),
(4, 'https://t.co/fyr9QdDa3D', 'Animated infographics are a great way to visualize data and explain something complex in a simple, engaging way. But making them from scratch can be daunting. And that’s where @AdobeAE templates come in handy.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `user_address` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_phone`, `user_address`, `user_email`, `user_password`, `role`, `image`, `date`, `created_at`, `updated_at`, `code`) VALUES
(1, 'ALI', '03314394966', 'ichra lahore', 'admin@gmail.com', '1234567', 'admin', 'alina36.jpg', '2018-08-30', '2018-08-29 19:00:00', '2018-10-05 16:19:23', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_head_section`
--
ALTER TABLE `about_head_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `about_history`
--
ALTER TABLE `about_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `about_license`
--
ALTER TABLE `about_license`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `about_partners`
--
ALTER TABLE `about_partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `about_value_section`
--
ALTER TABLE `about_value_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addcontect`
--
ALTER TABLE `addcontect`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `careers_section_1`
--
ALTER TABLE `careers_section_1`
  ADD PRIMARY KEY (`sec_in_id`);

--
-- Indexes for table `careers_vacancies_section`
--
ALTER TABLE `careers_vacancies_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expert_team_categories`
--
ALTER TABLE `expert_team_categories`
  ADD PRIMARY KEY (`expert_team_categories_id`);

--
-- Indexes for table `head_office`
--
ALTER TABLE `head_office`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `head_section`
--
ALTER TABLE `head_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_head_section`
--
ALTER TABLE `home_head_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_sections_heading`
--
ALTER TABLE `home_sections_heading`
  ADD PRIMARY KEY (`home_sec_id`);

--
-- Indexes for table `home_section_1`
--
ALTER TABLE `home_section_1`
  ADD PRIMARY KEY (`sec_in_id`);

--
-- Indexes for table `home_section_2_tabs`
--
ALTER TABLE `home_section_2_tabs`
  ADD PRIMARY KEY (`home_section_2_tab_id`);

--
-- Indexes for table `home_section_2_tabs_data`
--
ALTER TABLE `home_section_2_tabs_data`
  ADD PRIMARY KEY (`home_section_2_tabs_data_id`);

--
-- Indexes for table `home_section_3`
--
ALTER TABLE `home_section_3`
  ADD PRIMARY KEY (`home_section_3_id`);

--
-- Indexes for table `home_section_4`
--
ALTER TABLE `home_section_4`
  ADD PRIMARY KEY (`home_section_4_id`);

--
-- Indexes for table `home_section_5_post`
--
ALTER TABLE `home_section_5_post`
  ADD PRIMARY KEY (`home_section_5_post_id`);

--
-- Indexes for table `home_section_5_post_article`
--
ALTER TABLE `home_section_5_post_article`
  ADD PRIMARY KEY (`home_section_5_post_article_id`);

--
-- Indexes for table `home_section_5_post_categories`
--
ALTER TABLE `home_section_5_post_categories`
  ADD PRIMARY KEY (`home_section_5_post_categories_id`);

--
-- Indexes for table `home_slider`
--
ALTER TABLE `home_slider`
  ADD PRIMARY KEY (`home_slider_id`);

--
-- Indexes for table `index_footer`
--
ALTER TABLE `index_footer`
  ADD PRIMARY KEY (`index_footer_id`);

--
-- Indexes for table `index_navbar`
--
ALTER TABLE `index_navbar`
  ADD PRIMARY KEY (`index_navbar_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`subscriber_id`);

--
-- Indexes for table `twiter`
--
ALTER TABLE `twiter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_head_section`
--
ALTER TABLE `about_head_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `about_history`
--
ALTER TABLE `about_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `about_license`
--
ALTER TABLE `about_license`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `about_partners`
--
ALTER TABLE `about_partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `about_value_section`
--
ALTER TABLE `about_value_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `addcontect`
--
ALTER TABLE `addcontect`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `careers_section_1`
--
ALTER TABLE `careers_section_1`
  MODIFY `sec_in_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `careers_vacancies_section`
--
ALTER TABLE `careers_vacancies_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `expert_team_categories`
--
ALTER TABLE `expert_team_categories`
  MODIFY `expert_team_categories_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `head_office`
--
ALTER TABLE `head_office`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `head_section`
--
ALTER TABLE `head_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `home_head_section`
--
ALTER TABLE `home_head_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `home_sections_heading`
--
ALTER TABLE `home_sections_heading`
  MODIFY `home_sec_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `home_section_1`
--
ALTER TABLE `home_section_1`
  MODIFY `sec_in_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `home_section_2_tabs`
--
ALTER TABLE `home_section_2_tabs`
  MODIFY `home_section_2_tab_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `home_section_2_tabs_data`
--
ALTER TABLE `home_section_2_tabs_data`
  MODIFY `home_section_2_tabs_data_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `home_section_3`
--
ALTER TABLE `home_section_3`
  MODIFY `home_section_3_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `home_section_4`
--
ALTER TABLE `home_section_4`
  MODIFY `home_section_4_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `home_section_5_post`
--
ALTER TABLE `home_section_5_post`
  MODIFY `home_section_5_post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `home_section_5_post_article`
--
ALTER TABLE `home_section_5_post_article`
  MODIFY `home_section_5_post_article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `home_section_5_post_categories`
--
ALTER TABLE `home_section_5_post_categories`
  MODIFY `home_section_5_post_categories_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `home_slider`
--
ALTER TABLE `home_slider`
  MODIFY `home_slider_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `index_footer`
--
ALTER TABLE `index_footer`
  MODIFY `index_footer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `index_navbar`
--
ALTER TABLE `index_navbar`
  MODIFY `index_navbar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `subscriber_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `twiter`
--
ALTER TABLE `twiter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
