<?php

class pdf 
{

	public function __construct()
	{
		require_once APPPATH.'third_party/fpdf/fpdf.php';
		$pdf=new FPDF();
		$pdf->Addpage();
		$CI=& get_instance();
		$CI->fpdf=$pdf;
	}
}