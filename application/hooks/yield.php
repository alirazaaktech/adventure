<?php 

class _Yield{
	
	public function doYield(){
		global $OUT;
		$CI =& get_instance();
		$output = $CI->output->get_output();
		$default = APPPATH.'views/layouts/admin/default.php';
		$view = '';
		if(isset($CI->layout)){
			if(!preg_match('/(.+).php$/', $CI->layout)){
				$CI->layout .= '.php';
			}
			$requested = APPPATH.'views/layouts/'.$CI->layout;
			if(file_exists($requested)){
				$layout = $CI->load->file($requested, true);
				$view = str_replace("{_yield}", $output, $layout);
			}
		}elseif(file_exists($default)){
			$layout = $CI->load->file($default, true);
			$view = str_replace("{_yield}", $output, $layout);
		}else{
			$view = $output;
		}
		$OUT->_display($view);
	}
}
?>