<?php
class Contact_us extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('image_lib');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Top_nav_model');
		$this->load->model('Footer_model');
		$this->load->model('Head_section_model');
		 $this->load->model('contect_us_model','cm');
		  $this->load->model('addcontect_model','ac');
		   $this->load->model('layout_model');
		// $this->load->model('Home_section_2_model');
		// $this->load->model('Home_section_3_model');
		// $this->load->model('Home_section_4_model');
		
		$this->layout = "admin/user_2_dashboard";
	}

	public function index()
	{
		$data['products'] = $this->ac->get_all('','','id','DESC');
		$data['head_section'] = $this->layout_model->get_all('','','id','DESC');
		$data['head_section'] = $this->Head_section_model->get_all();
		// $data['section_1'] = $this->Home_section_1_model->get_all();
		// $data['section_2_tabs'] = $this->Home_section_2_model->get_all();
		// $data['section_3'] = $this->Home_section_3_model->get_all();
		// $data['section_4'] = $this->Home_section_4_model->get_all();
		// echo "<pre>";
		// print_r($data);
		// exit;
		
		$this->load->view('Contact_us/index', $data);
	}
	public function subscribe()
	{
		$data = array( 'subscriber_email' =>$this->input->post('EMAIL') );
		$this->Home_head_section_model->insert_subscribe('subscribers',$data);
		$this->session->set_flashdata('success_data', 'you has been successfully subscribers');
		redirect('home');
	}
	 public function process_add()
     {  $data = array();
        if ($this->input->post()) {

      // debug($data,true);
       $this->form_validation->set_rules('email','email','required');
        $this->form_validation->set_rules('name','name','required');
          // $this->form_validation->set_rules('logo','logo','required');
          
         
         if ($this->form_validation->run() === TRUE ){
                $data = $this->input->post();
               unset($data['_wpcf7']);
               unset($data['_wpcf7_version']);
               unset($data['_wpcf7_locale']);
               unset($data['_wpcf7_unit_tag']);
               unset($data['_wpcf7_container_post']);
             		if($category_id = $this->cm->save($data)) {
                      $this->session->set_flashdata('success_message', 'Data has been saved successfully');

                      redirect('Contact_us/');
                  } else {
                      $this->session->set_flashdata('error_message', 'Error occured while saving Data.');
                      redirect('Contact_us/add');
                  }
              }else{
              	$data['head_section'] = $this->Head_section_model->get_all();
                 $this->load->view('contact_us/index',$data);
              }
          } else {
              $this->session->set_flashdata('error_message', 'Error occured while saving category.');
              redirect('Contact_us/');
          }
      }

}
?>		