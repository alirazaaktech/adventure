<?php
class Home extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('image_lib');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Top_nav_model');
		$this->load->model('Footer_model');
		$this->load->model('Home_head_section_model');
		$this->load->model('Home_section_1_model');
		$this->load->model('Home_section_2_model');
		$this->load->model('Home_section_3_model');
		$this->load->model('Home_section_4_model');
		$this->load->model('Home_section_5_model');
		$this->load->model('Home_slider_model');
		$this->load->model('layouthome_model');
		$this->load->model('career_section_1_model');
		
		$this->load->model('twiter_model');
		$this->load->model('Timesection_model');
		$this->layout = "admin/user_dashboard";
	}

	public function index()
	{
			
			 // debug($data['videosection'],true);
		$data['head_section'] = $this->layouthome_model->get_all();
		// debug($data['head_section'],true);
		$data['section_1'] = $this->Home_section_1_model->get_all('','','sec_in_id','',3);
		$data['section_2_tabs'] = $this->Home_section_2_model->get_all();
		$data['section_3'] = $this->Home_section_3_model->get_all('','','','',4);
		$data['section_4'] = $this->Home_section_4_model->get_all('','','','',4);
		$data['section_5_post'] = $this->Home_section_5_model->limted_colum('home_section_5_post',3,'home_section_5_post_id','desc');
		$data['section_5_leatset_post'] = $this->Home_section_5_model->limted_colum('home_section_5_post',1,'home_section_5_post_id','desc');
		$data['section_5_leatset_article'] = $this->Home_section_5_model->limted_colum('home_section_5_post_article',1,'home_section_5_post_article_id','desc');
		$data['videosection'] = $this->Home_slider_model->get_all();
		$data['products'] = $this->twiter_model->get_all('','','id','DESC');
		$data['section_video'] = $this->career_section_1_model->limted_colums(3,'sec_in_id','desc');
		$data['headoffice'] = $this->Timesection_model->get_all('','','id','DESC');
		// echo "<pre>";
		// print_r($data['headoffice']);
		// exit;
	
		$this->load->view('index/index', $data);
	}
	public function subscribe()
	{
		$data = array( 'subscriber_email' =>$this->input->post('EMAIL') );
		$this->Home_head_section_model->insert_subscribe('subscribers',$data);
		$this->session->set_flashdata('success_data', 'you has been successfully subscribers');
		redirect('home');
	}

}
?>		