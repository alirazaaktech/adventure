<?php
class Post extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('image_lib');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Top_nav_model');
		$this->load->model('Footer_model');
		$this->load->model('Home_head_section_model');
		$this->load->model('Home_section_1_model');
		$this->load->model('Home_section_2_model');
		$this->load->model('Home_section_3_model');
		$this->load->model('Home_section_4_model');
		$this->load->model('Home_section_5_model');
		$this->load->model('Home_section_5_cat_model');
		
		$this->load->library("pagination");
		$this->load->helper("url");
		
		$this->layout = "admin/user_dashboard";
	}

	public function index()
	{

   $config = array();
   $config["base_url"] = base_url('Post/index');
   $config['total_rows'] =   $this->Home_section_5_model->row();//here we will count all the data from the table
 
   $config['per_page'] = 7;//number of data to be shown on single page
   $config["uri_segment"] = 3;
 
	$config['cur_tag_open'] = '&nbsp;<a class="current">';
	$config['cur_tag_close'] = '</a>';
	$config['next_link'] = 'Next';
	$config['prev_link'] = 'Previous';
   $this->pagination->initialize($config);
 
   $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
   $data["head_sections"] = $this->Home_section_5_model->show($config["per_page"], $this->uri->segment(3));
 
   $data["links"] = $this->pagination->create_links();//create the link for pagination
     
	$data['head_section'] = $this->Home_head_section_model->get_all();
	// $data['head_sections'] = $this->Home_section_5_model->get_all();
 // debug($data["records2"],true);
	$data['section_5_post'] = $this->Home_section_5_model->limted_colum('home_section_5_post',5,'home_section_5_post_id','desc');
	$data['categories'] =  $this->Home_section_5_cat_model->get_all(); 
		$this->load->view('post/index', $data);
	}
	public function subscribe()
	{
		$data = array( 'subscriber_email' =>$this->input->post('EMAIL') );
		$this->Home_head_section_model->insert_subscribe('subscribers',$data);
		$this->session->set_flashdata('success_data', 'you has been successfully subscribers');
		redirect('home');
	}

	public function pagination()
	{
		// $this->load->library('pagination');
  //        $config=
		// 	[
		// 		'base_url'=>base_url()."Post/index",
		// 		'per_page' =>3,
		// 		'total_rows'=>$this->Home_section_5_model->row(),
		// 	];
			
  //         $this->pagination->initialize($config);
  //         return $config['per_page'];
	}
	

}
?>		