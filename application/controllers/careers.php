<?php
class Careers extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('image_lib');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Top_nav_model');
		$this->load->model('Footer_model');
		$this->load->model('Head_section_model');
		$this->load->model('career_section_1_model');
		$this->load->model('Career_vacancies_section_model');
		// $this->load->model('Home_section_2_model');
		// $this->load->model('Home_section_3_model');
		// $this->load->model('Home_section_4_model');
		
		$this->layout = "admin/user_2_dashboard";
	}

	public function index()
	{
		$data['head_section'] = $this->Head_section_model->get_all();
		$data['section_1'] = $this->career_section_1_model->limted_colums(3,'sec_in_id','desc');
		
		$data['vacancies_section'] = $this->Career_vacancies_section_model->get_all('','','id','desc',3);
		// echo "<pre>";print_r($data['vacancies_section']);
		// exit;
		$data['vacancies_top_heading'] = $this->Career_vacancies_section_model->get_where ('top_heading','',true,'','','top_heading');
		$data['vacancies_inner_heading'] = $this->Career_vacancies_section_model->get_where ('inner_heading','',true,'','','inner_heading');
		$data['vacancies_location'] = $this->Career_vacancies_section_model->get_where ('location','',true,'','','location');

		// echo "<pre>";
		// print_r($data['section_1']);
		// exit;
		// $data['section_2_tabs'] = $this->Home_section_2_model->get_all();
		// $data['section_3'] = $this->Home_section_3_model->get_all();
		// $data['section_4'] = $this->Home_section_4_model->get_all();
		// echo "<pre>";
		// print_r($data);
		// exit;
		//$this->load->view('careers/index');
		$this->load->view('careers/index', $data);
	}
	public function subscribe()
	{
		$data = array( 'subscriber_email' =>$this->input->post('EMAIL') );
		$this->Home_head_section_model->insert_subscribe('subscribers',$data);
		$this->session->set_flashdata('success_data', 'you has been successfully subscribers');
		redirect('home');
	}
	public function get_value_by_ajax()
	{
		$data = array();
		$data['response'] = false;
		if($this->input->post('top_heading')){
			$industry_id= $this->input->post('top_heading');

			$result = $this->Career_vacancies_section_model->specfic_data('careers_vacancies_section',$industry_id);
			if(!empty($result)){
				$data['response'] = true;
				$data['results'] = $result;
			}
		}

		echo json_encode($data);
		exit;
		
	}

	public function get_all_data()
	{
		$data = array();
		$data['response'] = false;
		if($this->input->post('all_data')){
			$id= $this->input->post('all_data');

			$result = $this->Career_vacancies_section_model->get_all();
			if(!empty($result)){
				$data['response'] = true;
				$data['results'] = $result;
			}
		}

		echo json_encode($data);
		exit;

	}
	/*
action: advice_filter_post_type
post_type: advice_job
filters: {"job_industries":{"value":[367],"parameters":"industries"}}
_wpnonce: 05e48b1c3b
params: {"posts_per_page":3}

	*/


	public function get_careers_ajax()
	{
		$filter= $this->input->post();
		// print_r($filter[0]); exit();
		 $data = explode("{", $filter['filters']);
		 print_r($data);
		 $data2 = explode("value",  $data[2]);
		 print_r($data2);



		exit;
		
		

		// $job_industries = $this->input->post('job_industries');
		// $job_location = $this->input->post('job_location');
		// $job_department = $this->input->post('job_department');
		// $job_role = $this->input->post('job_role');
		// $job_industries = new stdClass;
		// print_r($job_industries);
		// echo "<br>";

		// echo $job_industries;
		// exit;

		$content_object = new stdClass;
		$content_object = $this->Career_vacancies_section_model->get_search($job_industries,$job_location,$job_department,$job_role) ;
		$contact = new stdClass;
		 echo "<pre>";
		print_r($content_object);
		exit;
	
		foreach ($content_object as $key) {
		
		$contact->title = $key['top_heading'];
		$contact->link =  $key['inner_heading'];
		$contact->excerpt =  $key['paragraph'];
		$contact->department = $key['top_heading'];
		$contact->location =  $key['location'];
		}
		// $content_object->link = 'https:///';
		// $content_object->excerpt = 'this is testint';
		// $content_object->department = 'testing';
		// $content_object->location = 'Lahore';

		$success_response = new stdClass;


		$success_response->post_count = false;
		$success_response->pagination_html = false;
		$success_response->success[0] = $contact;

		echo json_encode($success_response);
		exit;

		$error_response = new stdClass; 
        $no_result = new stdClass;
        $no_result->title ='No results found';
        $no_result->text ='Please check for correctness';
        $error_response->error = $no_result;

        echo json_encode($error_response);
        exit;
	}

}
?>		