<?php
class About_us extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('image_lib');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Top_nav_model');
		$this->load->model('Footer_model');
		$this->load->model('Head_section_model');
		$this->load->model('Home_section_1_model');

		  $this->load->model('about_model','v');
		  $this->load->model('value_model');
		 $this->load->model('Home_section_1_model');
		// $this->load->model('Home_section_2_model');
		$this->load->model('Home_slider_model');
		$this->load->model('Home_section_4_model');
		 $this->load->model('partner_model');
		 $this->load->model('licenese_model');
		 $this->load->model('history_model');
		
		$this->layout = "admin/user_2_dashboard";
	}

	public function index()
	{
		 
		 
		   $data['logo'] = $this->partner_model->get_all('','','id','DESC');
		   // debug($data['logo'],true);
       $data['productss'] = $this->v->get_all('','','id','DESC');
       $data['valuess'] = $this->value_model->get_all();
       // debug($data['valuess'],true);
		$data['head_section'] = $this->Head_section_model->get_all();
		$data['section_1'] = $this->Home_section_1_model->get_all();
		$data['section_4'] = $this->Home_section_4_model->get_all();
		$data['videosection'] = $this->Home_slider_model->get_all();
		$data['productsss'] = $this->licenese_model->get_all('','','id','DESC');
        $data['licenese_head'] = $this->licenese_model->get_para('','','id','DESC');
         $data['licenese_img'] = $this->licenese_model->get_all('','','id','DESC');
         $data['history'] = $this->history_model->get_all('','','id','DESC');

		// $data['section_2_tabs'] = $this->Home_section_2_model->get_all();
		// $data['section_3'] = $this->Home_section_3_model->get_all();
		// $data['section_4'] = $this->Home_section_4_model->get_all();
		// echo "<pre>";
		// print_r($data['history']);
		// exit;
		//$this->load->view('careers/index');
		$this->load->view('about_us/index', $data);
	}
	public function subscribe()
	{
		$data = array( 'subscriber_email' =>$this->input->post('EMAIL') );
		$this->Home_head_section_model->insert_subscribe('subscribers',$data);
		$this->session->set_flashdata('success_data', 'you has been successfully subscribers');
		redirect('home');
	}

}
?>		