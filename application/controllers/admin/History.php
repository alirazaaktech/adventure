<?php
class History extends CI_Controller
{
  public function __construct()
  {
      parent::__construct();
      $this->load->library('image_lib');
      $this->load->model('history_model','h');
      $this->load->library('form_validation');
      $this->layout = "admin/dashboard";
  }

  public function add()
  {
    if($this->session->userdata('user_email'))
    {
      $this->load->view('aboutpage/history/add');
    }
    else
    {
      redirect('admin/login');
    }
  }

  public function index()
  {
    if($this->session->userdata('user_email'))
    {
       $data['products'] = $this->h->get_all('','','id','DESC');
        $this->load->view('aboutpage/history/index',$data);
    }
    else
    {
      redirect('admin/login');
    }
  }
  
  public function process_add()
   {  
      if($this->session->userdata('user_email'))
      {
        $this->form_validation->set_rules('year',' Name','required');
        $this->form_validation->set_rules('heading','Heading','required');
        $this->form_validation->set_rules('paragraph','Paragraph','required');
        if ($this->form_validation->run()==false) 
        {
          $this->add();
        }
        else
        {
          if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
              {
                
            $config['upload_path']   = BASEPATH.'../assets1/images/';
            $config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
            $config['max_size']      = 10000;
            $config['max_width']     = 4000;
            $config['max_height']    = 4000;
            $this->load->library('upload',$config);
            $this->load->initialize($config);
            if(!$this->upload->do_upload('photo'))
            {
              $error = array('error' => $this->upload->display_errors());
            // debug($error,true);
            }
            else
            {
              $uploaded_image = $this->upload->data(); // end image upload
              $photo = $uploaded_image['file_name'];
            }
              }
          $no_img_select = $this->input->post('no_img_select');
          

          $data = array( 
          'year' =>$this->input->post('year'),
          'name' => $this->input->post('heading'),
          'paragraph' => $this->input->post('paragraph'),
          'history_image' =>!empty($photo)?$photo:$no_img_select,
          );
          $this->h->save($data);
          $this->session->set_flashdata('success_data', 'data has been Added successfully');
          redirect('admin/History');
        }
    }
    else
    {
      redirect('admin/login');
    }
    
  }

  public function delete($id)
  {
    if($this->session->userdata('user_email'))
    {
    // debug($id,true);
      if (isset($id) && !empty($id)) {
            
           $this->h->delete_data("about_history","id",$id);

          // $this->v->delete_by('id', $id);
          // $this->db->last_query();exit();
          $this->session->set_flashdata('success_message', 'Data has been deleted successfully');
      } else {
          $this->session->set_flashdata('error_message', 'Invalid request to delete heading.');
      }
      redirect('admin/History/index/');
    }
    else
    {
      redirect('admin/login');
    }
  }


   public function update($id)
  {
    if($this->session->userdata('user_email'))
    {
      $data = array();
      if (isset($id) && !empty($id)) {
          $data['data'] = $this->h->get_by('id', $id);
          if (isset($data['data']) && !empty($data['data'])) {
              $data['data'] = $data['data'][0];
               $all_activ = $this->h->get_by('id', $id);
              $data['all_activ'] = $all_activ[0];
              $this->load->view('aboutpage/history/update',$data);
          } else {
              $this->session->set_flashdata('error_message', 'Data not found.');
              redirect('admin/History/');
          }
      } else {
          $this->session->set_flashdata('error_message', 'Invalid request to update product.');
        
          redirect('admin/History/',$data);
      }
    }
    else
    {
      redirect('admin/login');
    }
  }

  public function process_update()
  {
     if($this->session->userdata('user_email'))
    {
      $this->form_validation->set_rules('year',' Name','required');
      $this->form_validation->set_rules('name','Heading','required');
      $this->form_validation->set_rules('paragraph','Paragraph','required');
      $id =  $this->input->post('id');
      if ($this->form_validation->run()==false) 
      {
        $this->update($id);
      }
      else
      {
        if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
        {
          
          $config['upload_path']   = BASEPATH.'../assets1/images/';
          $config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
          $config['max_size']      = 10000;
          $config['max_width']     = 4000;
          $config['max_height']    = 4000;
          $this->load->library('upload',$config);
          $this->load->initialize($config);
          if(!$this->upload->do_upload('photo'))
          {
            $error = array('error' => $this->upload->display_errors());
          // debug($error,true);
          }
          else
          {
            $uploaded_image = $this->upload->data(); // end image upload
            $photo = $uploaded_image['file_name'];
          }
        }
          $old_image = $this->input->post('old_image');
          
          $data = array( 

          'year' =>$this->input->post('year'),
          'paragraph' =>$this->input->post('paragraph'),
          'name' => $this->input->post('name'),
          'history_image' =>!empty($photo)?$photo:$old_image
          );
          // echo "<pre>";
          // print_r($data);
          // exit;

            $this->h->update_by('id', $id, $data);
            redirect('admin/History');
        }
      
    }
    else
    {
      redirect('admin/login');
    }
  }

}
?>