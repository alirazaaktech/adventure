<?php
class Footer extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('image_lib');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Footer_model');
		$this->layout = "admin/dashboard";
	}

		public function index()
	{	
		if($this->session->userdata('user_email'))
		{
			$data['footer'] = $this->Footer_model->all_footer('index_footer');
			$this->load->view('footer/index',$data);
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}
	
	public function update($index_footer_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($index_footer_id))
	       	{
	            $data['footer_update'] = $this->Footer_model->update_all_footer('index_footer',$index_footer_id);
				$this->load->view('footer/update',$data);
	        }
        }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
    public function update_data()
    {
    	if($this->session->userdata('user_email'))
		{
	    	$this->form_validation->set_rules('index_footer_text','Text','required');
			$this->form_validation->set_rules('index_footer_newsletter','NEWSLETTER','required');
			$this->form_validation->set_rules('index_footer_linkedin','LINKEDIN' ,'required');
			$this->form_validation->set_rules('index_footer_twitter','TWITTER','required');
			$this->form_validation->set_rules('index_footer_facebook','FACEBOOK','required');

			$index_footer_id = $this->input->post('index_footer_id');
			if ($this->form_validation->run()==false) 
			{
				$this->update($index_footer_id);
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
		        if(isset($_FILES['background_photo']['tmp_name']) && !empty($_FILES['background_photo']['tmp_name']))
				{
					// $config['quality'] = '50%';
			  //       $config['width'] = 150;
			  //       $config['height'] = 80;
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					
					if(!$this->upload->do_upload('background_photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$background_photo = $uploaded_image['file_name'];
					}
				}
	         
				$old_image = $this->input->post('old_image');
				$old_image_background = $this->input->post('old_image_background');

				$data = array( 
				'index_footer_text' =>$this->input->post('index_footer_text'),
				'index_footer_newsletter' => $this->input->post('index_footer_newsletter'),
				'index_footer_linkedin' =>$this->input->post('index_footer_linkedin'),
				'index_footer_twitter' =>$this->input->post('index_footer_twitter'),
				'index_footer_facebook' =>$this->input->post('index_footer_facebook'),
				'index_footer_power_by' =>$this->input->post('index_footer_power_by'),
				'index_footer_logo_image' =>!empty($photo)?$photo:$old_image,
				'index_footer_background_image' =>!empty($background_photo)?$background_photo:$old_image_background
				);
		    	
		    	$this->Footer_model->update_by('index_footer_id', $index_footer_id, $data);
		    	$this->session->set_flashdata('success_update', 'Successfully Record update');
		    	redirect('admin/footer');
		    }
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}
	
}

?>