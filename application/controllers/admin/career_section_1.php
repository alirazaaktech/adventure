<?php 

class Career_section_1 extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('user_model');
		$this->load->model('Career_section_1_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if($this->session->userdata('user_email'))
		{
			$data['section_1'] = $this->Career_section_1_model->get_all();
			// echo "<pre>";print_r($data);
			// exit;
			$this->load->view('careers/section_1/index',$data);
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function add()
	{
		if($this->session->userdata('user_email'))
		{
			$this->load->view('careers/section_1/add');
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function insert()
	{
		if($this->session->userdata('user_email'))
		{
			$this->form_validation->set_rules('sec_in_h',' Name','required');
			$this->form_validation->set_rules('sec_in_p','Phone Number','required');
			$this->form_validation->set_rules('sec_in_video','Phone Number','required');
			if ($this->form_validation->run()==false) 
			{
				$this->add();
			}
			else
			{
				
				$no_video_select = 'https://www.youtube.com/watch?time_continue=72&v=M7lc1UVf-VE';
				$video =  $this->input->post('sec_in_video');

				$data = array( 
				'sec_in_h' =>$this->input->post('sec_in_h'),
				'sec_in_p' => $this->input->post('sec_in_p'),
				'sec_in_video' =>!empty($video)?$video:$no_video_select,
				);
				$this->Career_section_1_model->save($data);
				$this->session->set_flashdata('success_data', 'data has been Added successfully');
			 	redirect('admin/Career_section_1');
			}
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	
	}
	
	public function delete($sec_in_id)
    {
    	if($this->session->userdata('user_email'))
		{
       		if(isset($sec_in_id) && !empty($sec_in_id))
	       	{
	            $this->Career_section_1_model->delete_data('careers_section_1','sec_in_id',$sec_in_id);
	            $this->session->set_flashdata('success_message', 'User has been deleted successfully');
	        }
	        else
	        {
	            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
	        }
	        redirect('admin/Career_section_1');
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
	public function update($sec_in_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($sec_in_id))
	       	{
	            $data['data'] = $this->Career_section_1_model->get_by('sec_in_id',$sec_in_id);
				$this->load->view('careers/section_1/update',$data);
	        }
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
    public function update_data()
    {
    	if($this->session->userdata('user_email'))
		{
	    	$this->form_validation->set_rules('sec_in_h',' Name','required');
			$this->form_validation->set_rules('sec_in_p','Phone Number','required');
			$this->form_validation->set_rules('sec_in_video','Phone Number','required');

			$sec_in_id = $this->input->post('sec_in_id');
			if ($this->form_validation->run()==false) 
			{
				$this->update($sec_in_id);
			}
			else
			{
				
				$no_video_select = 'https://www.youtube.com/watch?time_continue=72&v=M7lc1UVf-VE';
				$video = $this->input->post('sec_in_video');

				$data = array( 

				'sec_in_h' =>$this->input->post('sec_in_h'),
				'sec_in_p' => $this->input->post('sec_in_p'),
				'sec_in_video' =>!empty($video)?$video:$no_video_select
				);
				
		    	$this->Career_section_1_model->update_by('sec_in_id', $sec_in_id, $data);
		    	redirect('admin/Career_section_1');
		    }
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	
}

?>