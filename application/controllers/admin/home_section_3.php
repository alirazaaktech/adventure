<?php 

class Home_section_3 extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('user_model');
		$this->load->model('Home_section_3_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if($this->session->userdata('user_email'))
		{
			$data['section_3'] = $this->Home_section_3_model->get_all();
			$this->load->view('home/section_3/index',$data);
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function add()
	{
		if($this->session->userdata('user_email'))
		{
			$this->load->view('home/section_3/add');
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function insert()
	{
		if($this->session->userdata('user_email'))
		{
			$this->form_validation->set_rules('home_section_3_h',' Name','required');
			$this->form_validation->set_rules('home_section_3_p','paragraphr','required');
			if ($this->form_validation->run()==false) 
			{
				$this->add();
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$no_img_select = $this->input->post('no_img_select');
				

				$data = array( 
				'home_section_3_h' =>$this->input->post('home_section_3_h'),
				'home_section_3_p' => $this->input->post('home_section_3_p'),
				'home_section_3_img' =>!empty($photo)?$photo:$no_img_select,
				);
				$this->Home_section_3_model->insert_data($data);
				$this->session->set_flashdata('success_data', 'data has been Added successfully');
			 	redirect('admin/Home_section_3');
			}
		}
	    else
	    {
	    	redirect('admin/login');
	    }

	}
	
	public function delete($home_section_3_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($home_section_3_id) && !empty($home_section_3_id))
	       	{
	            $this->Home_section_3_model->delete_data('home_section_3','home_section_3_id',$home_section_3_id);
	            $this->session->set_flashdata('success_message', 'Data has been deleted successfully');
	        }
	        else
	        {
	            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
	        }
	        redirect('admin/Home_section_3');
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
	public function update($home_section_3_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       if(isset($home_section_3_id) && !empty($home_section_3_id))
	       	{
	            $data['data'] = $this->Home_section_3_model->get_by('home_section_3_id',$home_section_3_id);
				$this->load->view('home/section_3/update',$data);
	        }
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
    public function update_data()
    {
    	if($this->session->userdata('user_email'))
		{
	    	$this->form_validation->set_rules('home_section_3_h',' Name','required');
			$this->form_validation->set_rules('home_section_3_p','paragraph','required');
			$home_section_3_id = $this->input->post('home_section_3_id');
			if ($this->form_validation->run()==false) 
			{
				$this->update($home_section_3_id);
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$old_image = $this->input->post('old_image');
				$data = array( 

				'home_section_3_h' =>$this->input->post('home_section_3_h'),
				'home_section_3_p' => $this->input->post('home_section_3_p'),
				'home_section_3_img' =>!empty($photo)?$photo:$old_image
				);
		    	$this->Home_section_3_model->update_by('home_section_3_id', $home_section_3_id, $data);
		    	redirect('admin/Home_section_3');
		    }
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	
}

?>