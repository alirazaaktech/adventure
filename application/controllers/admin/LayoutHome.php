<?php
class LayoutHome extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
       
        // $this->load->library('image_lib');
        $this->load->model('layouthome_model','tw');
         $this->load->library('form_validation');
          $this->layout = "admin/dashboard";
    }
 
      public function index()
    {
        if($this->session->userdata('user_email'))
      {
       $data['products'] = $this->tw->get_contects();
        $this->load->view('home/index',$data);
      }
        else
        {
          redirect('admin/login');
        }
    }

  

         public function post()
    {
        if($this->session->userdata('user_email'))
      {
       $data['products'] = $this->tw->get_contectss();
       // debug($data['products'],true);
        $this->load->view('post/header/index',$data);
      }
        else
        {
          redirect('admin/login');
        }
    }

        public function carears()
    {
        if($this->session->userdata('user_email'))
      {
       $data['products'] = $this->tw->get_contectss();
       // debug($data['products'],true);
        $this->load->view('careers/updates',$data);
      }
        else
        {
          redirect('admin/login');
        }
    }


    
 public function update($id)
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
   
        if (isset($id) && !empty($id)) {
            $data['data'] = $this->tw->get_by('id', $id);
            if (isset($data['data']) && !empty($data['data'])) {
                $data['data'] = $data['data'][0];
              
                $this->load->view('home/update',$data);
            } else {
                $this->session->set_flashdata('error_message', 'Data not found.');
                redirect('LayoutHome/');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to update product.');
          
            redirect('LayoutHome/',$data);
        }
      }
      else
      {
         redirect('admin/login');
      } 
    }
     public function updates($id)
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
   
        if (isset($id) && !empty($id)) {
            $data['data'] = $this->tw->get_by('id', $id);
            if (isset($data['data']) && !empty($data['data'])) {
                $data['data'] = $data['data'][0];
              
                $this->load->view('post/header/update',$data);
            } else {
                $this->session->set_flashdata('error_message', 'Data not found.');
                redirect('admin/LayoutHome/post');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to update product.');
          
            redirect('admin/LayoutHome/post',$data);
        }
      }
      else
      {
         redirect('admin/login');
      } 
    }
     public function process_update()
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
        if ($this->input->post('id')) {
        $id = $this->input->post('id');
  
                $data = $this->input->post();
        if(isset($_FILES['home_head_section_image']['tmp_name']) && !empty($_FILES['home_head_section_image']['tmp_name']))
        {
          $config['upload_path']   =BASEPATH.'../assets1/images/'; 
   
          $config['allowed_types'] = 'gif|jpg|png|docs|jpeg'; 
          $config['max_size']      = 10000; 
          $config['max_width']     = 4000; 
          $config['max_height']    = 4000;  
          $this->load->library('upload',$config);
          $this->load->initialize($config);
          if(!$this->upload->do_upload('home_head_section_image')){
            $error = array('error' => $this->upload->display_errors());
            // debug($error,true);
          }
          else{
                        $uploaded_image = $this->upload->data(); // end image upload
                        $data['home_head_section_image'] = $uploaded_image['file_name'];
                    }
              }

                if ($this->tw->update_by('id', $id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('admin/LayoutHome/');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('admin/LayoutHome');
                }
          
            
        }
      }
      else
      {
         redirect('admin/login');
      } 
        
    }


      public function process_updates()
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
        if ($this->input->post('id')) {
        $id = $this->input->post('id');
  
                $data = $this->input->post();
        if(isset($_FILES['head_section_image']['tmp_name']) && !empty($_FILES['head_section_image']['tmp_name']))
        {
          $config['upload_path']   =BASEPATH.'../assets1/images/'; 
   
          $config['allowed_types'] = 'gif|jpg|png|docs|jpeg'; 
          $config['max_size']      = 10000; 
          $config['max_width']     = 4000; 
          $config['max_height']    = 4000;  
          $this->load->library('upload',$config);
          $this->load->initialize($config);
          if(!$this->upload->do_upload('head_section_image')){
            $error = array('error' => $this->upload->display_errors());
            // debug($error,true);
          }
          else{
                        $uploaded_image = $this->upload->data(); // end image upload
                        $data['head_section_image'] = $uploaded_image['file_name'];
                    }
              }

                if ($this->tw->update_by('id', $id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('admin/LayoutHome/post');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('admin/LayoutHome/post');
                }
          
            
        }
      }
      else
      {
         redirect('admin/login');
      } 
        
    }
    
    }

?>