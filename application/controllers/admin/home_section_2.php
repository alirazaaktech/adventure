<?php 

class Home_section_2 extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('user_model');
		$this->load->model('Home_section_2_model');
		$this->load->model('Home_section_2_tab_data_model');

		$this->load->library('form_validation');
	}

	public function index($tab='')
	{
		if($this->session->userdata('user_email'))
		{
			$data['tab_1'] = $this->Home_section_2_model->get_all();
			$data['tab_2'] = $this->Home_section_2_tab_data_model->all_data();
			$data['tab'] = $tab;
			// echo "<pre>";
			// print_r($data);
			// exit;
			$this->load->view('home/section_2/tab',$data);
			//$this->load->view('home/section_2/index',$data);
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function add_sec_1()
	{
		if($this->session->userdata('user_email'))
		{
			$this->load->view('home/section_2/add_sec_1');
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function insert_sec_1()
	{
		if($this->session->userdata('user_email'))
		{
			$this->form_validation->set_rules('home_section_2_tab_tittle',' Web Title','required');
			$this->form_validation->set_rules('home_section_2_tab_web_link','website','required');
			if ($this->form_validation->run()==false) 
			{
				$this->add();
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$no_img_select = $this->input->post('no_img_select');
				

				$data = array( 
				'home_section_2_tab_tittle' =>$this->input->post('home_section_2_tab_tittle'),
				'home_section_2_tab_web_link' => $this->input->post('home_section_2_tab_web_link'),
				'home_section_2_tab_icon' =>!empty($photo)?$photo:$no_img_select,
				);
				$this->Home_section_2_model->insert_data($data);
				$this->session->set_flashdata('success_data', 'data has been Added successfully');
			 	redirect('admin/Home_section_2');
			}
		}
	    else
	    {
	    	redirect('admin/login');
	    }	
	}
	
	public function delete($home_section_2_tab_id,$tab='')
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($home_section_2_tab_id) && !empty($home_section_2_tab_id))
	       	{
	            $this->Home_section_2_model->delete_data('home_section_2_tabs','home_section_2_tab_id',$home_section_2_tab_id);
	            $this->session->set_flashdata('success_message', 'Data has been deleted successfully');
	        }
	        else
	        {
	            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
	        }
	        redirect('admin/Home_section_2');
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
	public function update_sec_1($home_section_2_tab_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       if(isset($home_section_2_tab_id) && !empty($home_section_2_tab_id))
	       	{
	            $data['data'] = $this->Home_section_2_model->get_by('home_section_2_tab_id',$home_section_2_tab_id);
				$this->load->view('home/section_2/update_sec_1',$data);
	        }
        }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
    public function update_data_sec_1()
    {
    	if($this->session->userdata('user_email'))
		{
	    	$this->form_validation->set_rules('home_section_2_tab_tittle',' Web Title','required');
			$this->form_validation->set_rules('home_section_2_tab_web_link','website','required');
			$home_section_2_tab_id = $this->input->post('home_section_2_tab_id');
			if ($this->form_validation->run()==false) 
			{
				$this->update_sec_1($home_section_2_tab_id);
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$old_image = $this->input->post('old_image');
				$data = array( 

				'home_section_2_tab_tittle' =>$this->input->post('home_section_2_tab_tittle'),
				'home_section_2_tab_web_link' => $this->input->post('home_section_2_tab_web_link'),
				'home_section_2_tab_icon' =>!empty($photo)?$photo:$old_image
				);
		    	$this->Home_section_2_model->update_by('home_section_2_tab_id', $home_section_2_tab_id, $data);
		    	redirect('admin/Home_section_2');
		    }
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function add_sec_2()
	{
		if($this->session->userdata('user_email'))
		{
			$data['tab_1'] = $this->Home_section_2_model->get_all();
			// echo "<pre>";
			// print_r($data);
			$this->load->view('home/section_2/add_sec_2',$data);
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function insert_sec_2($tab='')
	{
		if($this->session->userdata('user_email'))
		{
			$this->form_validation->set_rules('home_section_2_tabs_data_text','website','required');
			$tab = $this->input->post('tab');
			$home_section_2_tab_id = $this->input->post('home_section_2_tab_id');
			if ($this->form_validation->run()==false) 
			{
				$this->add_sec_2();
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$no_img_select = $this->input->post('no_img_select');

				$data = array( 
				'home_section_2_tab_id' =>$home_section_2_tab_id,
				'home_section_2_tabs_data_text' => $this->input->post('home_section_2_tabs_data_text'),
				'home_section_2_tabs_data_img' =>!empty($photo)?$photo:$no_img_select,
				);
				$this->Home_section_2_tab_data_model->insert_data($data);
				$this->session->set_flashdata('success_data', 'data has been Added successfully');
			 	redirect('admin/Home_section_2/index/'.$tab);
			}
		}
	    else
	    {
	    	redirect('admin/login');
	    }	
	}
	public function delete_tab_2($home_section_2_tabs_data_id,$tab='')
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($home_section_2_tabs_data_id) && !empty($home_section_2_tabs_data_id))
	       	{
	            $this->Home_section_2_tab_data_model->delete_data('home_section_2_tabs_data','home_section_2_tabs_data_id',$home_section_2_tabs_data_id);
	            $this->session->set_flashdata('success_message', 'Data has been deleted successfully');
	        }
	        else
	        {
	            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
	        }
	        redirect('admin/Home_section_2/index/'.$tab);
        }
	    else
	    {
	    	redirect('admin/login');
	    }
    }

    public function update_sec_2($home_section_2_tabs_data_id,$tab='')
    {
    	if($this->session->userdata('user_email'))
		{
	       if(isset($home_section_2_tabs_data_id) && !empty($home_section_2_tabs_data_id))
	       	{
	            $data['tab_1'] = $this->Home_section_2_tab_data_model->get_by('home_section_2_tabs_data_id',$home_section_2_tabs_data_id);
	            $data['tab'] = $tab;
				$this->load->view('home/section_2/update_sec_2',$data);
	        }
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
    }

    public function update_data_sec_2()
    {
    	if($this->session->userdata('user_email'))
		{
	    	$this->form_validation->set_rules('home_section_2_tabs_data_text','Image Title','required');
			$home_section_2_tabs_data_id = $this->input->post('home_section_2_tabs_data_id');
			$tab = $this->input->post('tab');
			if ($this->form_validation->run()==false) 
			{
				$this->update_sec_2($home_section_2_tabs_data_id,$tab);
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$old_image = $this->input->post('old_image');
				$data = array( 

				'home_section_2_tabs_data_text' =>$this->input->post('home_section_2_tabs_data_text'),
				'home_section_2_tabs_data_img' =>!empty($photo)?$photo:$old_image
				);
				// echo "<pre>";
				// print_r($data);exit;
		    	$this->Home_section_2_tab_data_model->update_by('home_section_2_tabs_data_id', $home_section_2_tabs_data_id, $data);
		    	redirect('admin/Home_section_2/index/'.$tab);
		    }
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	
}

?>