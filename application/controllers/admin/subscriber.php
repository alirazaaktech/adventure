<?php 

class Subscriber extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('user_model');
		$this->load->model('Subscriber_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if($this->session->userdata('user_email'))
		{
			$data['subscriber'] = $this->Subscriber_model->get_all();
			// echo "<pre>";print_r($data);
			// exit;
			$this->load->view('home/subscriber/index',$data);
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	
}

?>