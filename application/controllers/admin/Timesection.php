<?php
class Timesection extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
       
        // $this->load->library('image_lib');
        $this->load->model('Timesection_model','tm');
         $this->load->library('form_validation');
          $this->layout = "admin/dashboard";
    }
 
    public function add()
    {
      if($this->session->userdata('user_email'))
      {
        $this->load->view('home/timesection/add');
      }
      else
      {
        redirect('admin/login');
      }
    }
      public function index()
    {
      if($this->session->userdata('user_email'))
      {
        $data['products'] = $this->tm->get_all('','','id','DESC');
        $this->load->view('home/timesection/index',$data);
       }
      else
      {
        redirect('admin/login');
      }
    }
    
    
    public function process_add()
    {
      if($this->session->userdata('user_email'))
      {

          $data = array();
          if ($this->input->post()) {

        
         $this->form_validation->set_rules('office_name','office_name','required');
            // $this->form_validation->set_rules('logo','logo','required');
            
           
           if ($this->form_validation->run() === TRUE ){
                  $data = $this->input->post();
                  // debug($data,true);
                if($category_id = $this->tm->save($data)) {
                        $this->session->set_flashdata('success_message', 'Data has been saved successfully');

                        redirect('admin/Timesection/index');
                    } else {
                        $this->session->set_flashdata('error_message', 'Error occured while saving Data.');
                        redirect('admin/Timesection/add');
                    }
                }else{
                   $this->load->view('home/timesection/add',$data);
                }
            } else {
                $this->session->set_flashdata('error_message', 'Error occured while saving category.');
                redirect('admin/Timesection/add/');
            }
      }
      else
      {
        redirect('admin/login');
      }
    }
    public function delete($id)
    {
      if($this->session->userdata('user_email'))
      {
      // debug($id,true);
        if (isset($id) && !empty($id)) {
              
             $this->tm->delete_data("head_office","id",$id);

            // $this->v->delete_by('id', $id);
            // $this->db->last_query();exit();
            $this->session->set_flashdata('success_message', 'heading has been deleted successfully');
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to delete heading.');
        }
        redirect('admin/Timesection/index/');
      }
      else
      {
        redirect('admin/login');
      }
    }


     public function update($id)
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
   
        if (isset($id) && !empty($id)) {
            $data['data'] = $this->tm->get_by('id', $id);
            if (isset($data['data']) && !empty($data['data'])) {
                $data['data'] = $data['data'][0];
                 $all_activ = $this->tm->get_by('id', $id);
                $data['all_activ'] = $all_activ[0];
                $this->load->view('home/timesection/update',$data);
            } else {
                $this->session->set_flashdata('error_message', 'Data not found.');
                redirect('admin/Timesection/');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to update product.');
          
            redirect('admin/Timesection/',$data);
        }
      }
      else
      {
        redirect('admin/login');
      }
    }

    public function process_update()
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
        if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $this->form_validation->set_rules('office_name','office_name','required');
        if ($this->form_validation->run() === TRUE) {
                $data = $this->input->post();
     

                if ($this->tm->update_by('id', $id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('admin/Timesection/index');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('admin/Timesection/');
                }
            }
            else{
              
              $data['data'] = $this->tm->get_by('id', $id);  
              
            
            $data['data'] = $data['data'][0];
          
            $this->load->view('home/timesection/update', $data);
            }
            
        }
      }
      else
      {
        redirect('admin/login');
      }
        
    }
}

?>