<?php
class Award extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
       
        // $this->load->library('image_lib');
        $this->load->model('award_model','aw');
         $this->load->library('form_validation');
          $this->layout = "admin/dashboard";
    }
 
      public function add()
    {
      if($this->session->userdata('user_email'))
      {
        $this->load->view('aboutpage/topsection/add');
      }

      else
      {
        redirect('admin/login');
      }
    }
      public function index()
    {
      if($this->session->userdata('user_email'))
      {
       $data['products'] = $this->aw->get_all('','','id','DESC');
        $this->load->view('aboutpage/topsection/index',$data);
        }

      else
      {
        redirect('admin/login');
      }
    }
    
    
    public function process_add()
     {
      if($this->session->userdata('user_email'))
        {
        // $data = array();
        if ($this->input->post()) {
          $heading= $this->input->post('heading');
          $para= $this->input->post('paragraph');
         $video= $this->input->post('image');
         // $status= $this->input->post('status');          
              if (!empty($video)) {
                          $chk=explode("/", $video);
                          $chk=count($chk);
                         if ($chk == 4){
                         if (strpos( $video,"watch?")) {
                            $vid_src=explode("?", $video);
                            $vid_src='https://www.youtube.com/'.$vid_src[1];
                            $video2=str_replace("=", '/', $vid_src);
                          }
                          else{
                            $vid_src=explode("/", $video);
                            $video2='https://www.youtube.com/v/'.$vid_src[3];
                            // debug($video2,true);
                          }
                         }
                         else{
                          $this->session->set_flashdata('error_message', 'Error occured while saving video.');
                          redirect('admin/About/add');
                         }
                    }
                    else{
                        $video2='';
                    }
                    $data=array('image'=>$video2,'heading'=>$heading,'paragraph'=>$para);             
                   if($category_id = $this->aw->save($data)) {
                          $this->session->set_flashdata('success_message', 'Data has been saved successfully');

                          redirect('admin/About/');
                      } else {
                          $this->session->set_flashdata('error_message', 'Error occured while saving Data.');
                          redirect('admin/About/add');
                      }
                 
              } else {
                  $this->session->set_flashdata('error_message', 'Error occured while saving category.');
                  redirect('admin/About/add/');
              }
           }

      else
      {
        redirect('admin/login');
      }
    }
        public function delete($id)
    {
      if($this->session->userdata('user_email'))
      {
        if (isset($id) && !empty($id)) {
            $this->aw->delete_by('id', $id);
            $this->session->set_flashdata('success_message', 'heading has been deleted successfully');
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to delete heading.');
        }
        redirect('admin/About/index/');

      }

      else
      {
        redirect('admin/login');
      }
    }


     public function update($id)
    {
      if($this->session->userdata('user_email'))
        {
          $data = array();
     
          if (isset($id) && !empty($id)) {
              $data['data'] = $this->aw->get_by('id', $id);
              if (isset($data['data']) && !empty($data['data'])) {
                  $data['data'] = $data['data'][0];
                   $all_activ = $this->aw->get_by('id', $id);
                  $data['all_activ'] = $all_activ[0];
                  $this->load->view('aboutpage/topsection/update',$data);
              } else {
                  $this->session->set_flashdata('error_message', 'Data not found.');
                  redirect('admin/About/');
              }
          } else {
              $this->session->set_flashdata('error_message', 'Invalid request to update product.');
            
              redirect('admin/About/',$data);
          }
        }

      else
      {
        redirect('admin/login');
      }
    }

    public function process_update()
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
        if ($this->input->post('id')) {

       

              $id = $this->input->post('id');
             $this->form_validation->set_rules('heading','heading','required');
       
            if ($this->form_validation->run() === TRUE) {
                $data = $this->input->post();
                //debug($data,true);
       
                if ($this->aw->update_by('id', $id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('admin/About/');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('admin/About/');
                }
            }
            else{
                //  $data['Category'] = $this->Category->get_all();
                // $data['sub'] = $this->sub_Category->get_all();
                $data['data'] = $this->aw->get_by('id', $id);
                $data['data'] = $data['data'][0];
              
                $this->load->view('aboutpage/topsection/update', $data);
            }
            
        }
      }

      else
      {
        redirect('admin/login');
      }
        
    }

    }


?>