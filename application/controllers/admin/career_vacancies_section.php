<?php 

class Career_vacancies_section extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('user_model');
		$this->load->model('Career_vacancies_section_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if($this->session->userdata('user_email'))
		{
			$data['vacancies_section'] = $this->Career_vacancies_section_model->get_all();
			// echo "<pre>";print_r($data);
			// exit;
			$this->load->view('careers/vacancies_section/index',$data);
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function add()
	{
		if($this->session->userdata('user_email'))
		{
			$this->load->view('careers/vacancies_section/add');
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function insert()
	{
		if($this->session->userdata('user_email'))
		{
			$this->form_validation->set_rules('top_heading','Top Heading','required');
			$this->form_validation->set_rules('inner_heading','Inner Heading','required');
			$this->form_validation->set_rules('paragraph','Paragraph','required');
			$this->form_validation->set_rules('location','Location Country','required');
			if ($this->form_validation->run()==false) 
			{
				$this->add();
			}
			else
			{
				$data = array( 
				'top_heading' =>$this->input->post('top_heading'),
				'inner_heading' => $this->input->post('inner_heading'),
				'paragraph' => $this->input->post('paragraph'),
				'location' => $this->input->post('location'),
				
				);
				// echo "<pre>";
				// print_r($data);exit;
				$this->Career_vacancies_section_model->save($data);
				$this->session->set_flashdata('success_data', 'data has been Added successfully');
			 	redirect('admin/Career_vacancies_section');
			}
		}
	    else
	    {
	    	redirect('admin/login');
	    }	
	}
	
	public function delete($id)
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($id) && !empty($id))
	       	{
	            $this->Career_vacancies_section_model->delete_data('careers_vacancies_section','id',$id);
	            $this->session->set_flashdata('success_message', 'User has been deleted successfully');
	        }
	        else
	        {
	            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
	        }
	        redirect('admin/Career_vacancies_section');
        }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
	public function update($id)
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($id))
	       	{
	            $data['data'] = $this->Career_vacancies_section_model->get_by('id',$id);
	   //         	echo "<pre>";
				// print_r($data);exit;
				$this->load->view('careers/vacancies_section/update',$data);
	        }
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
    public function update_data()
    {
    	if($this->session->userdata('user_email'))
		{
	    	$this->form_validation->set_rules('top_heading','Top Heading','required');
			$this->form_validation->set_rules('inner_heading','Inner Heading','required');
			$this->form_validation->set_rules('paragraph','Paragraph','required');
			$this->form_validation->set_rules('location','Location Country','required');
			$id = $this->input->post('id');
			if ($this->form_validation->run()==false) 
			{
				$this->update($id);
			}
			else
			{

				$data = array( 

				'top_heading' =>$this->input->post('top_heading'),
				'inner_heading' => $this->input->post('inner_heading'),
				'paragraph' => $this->input->post('paragraph'),
				'location' => $this->input->post('location'),
				);
				// echo "<pre>";
				// print_r($data);exit;

		    	$this->Career_vacancies_section_model->update_by('id', $id, $data);
		    	redirect('admin/Career_vacancies_section');
		    }
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	
}

?>