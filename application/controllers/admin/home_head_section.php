<?php
class Home_head_section extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('image_lib');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('Home_head_section_model');
		$this->layout = "admin/dashboard";
	}

	public function index()
	{	
		if($this->session->userdata('user_email'))
    	{
			$data['top_nav'] = $this->Home_head_section_model->all_top_nav('index_navbar');
			$this->load->view('top_navbar/index',$data);
		}
	    else
	    {
	      redirect('admin/login');
	    }
	}
	
	public function update($index_navbar_id)
    {
    	if($this->session->userdata('user_email'))
    	{
	       	if(isset($index_navbar_id))
	       	{
	            $data['top_nav_update'] = $this->Home_head_section_model->update_top_nav('index_navbar',$index_navbar_id);
				$this->load->view('top_navbar/update',$data);
	        }
	    }
	    else
	    {
	      redirect('admin/login');
	    }
    }
    
    public function update_data()
    {
    	if($this->session->userdata('user_email'))
    	{
	    	$this->form_validation->set_rules('index_navbar_info','Info','required');
			$this->form_validation->set_rules('index_navbar_phone','Phone Number','required');
			$index_navbar_id = $this->input->post('index_navbar_id');
			
			if ($this->form_validation->run()==false) 
			{
				$this->update($index_navbar_id);
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
					$config['upload_path']   = BASEPATH.'../assets1/images/';

					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);

					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
				
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
	         
	  
				$old_image = $this->input->post('old_image');
				$data = array( 
				'index_navbar_info' =>$this->input->post('index_navbar_info'),
				'index_navbar_phone' => $this->input->post('index_navbar_phone'),
				'index_navbar_logo_image' =>!empty($photo)?$photo:$old_image
				);

		    	$this->Home_head_section_model->update_by('index_navbar_id', $index_navbar_id, $data);
		    	$this->session->set_flashdata('success_update', 'Successfully Record update');
		    	redirect('admin/top_navbar');
		    }
	    }
	    else
	    {
	      redirect('admin/login');
	    }
	}

	
}

?>