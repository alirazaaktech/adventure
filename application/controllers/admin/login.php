<?php 

class Login extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->model('Admin_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$this->layout = "";
		$this->load->view('login/login');
	}

	// public function login()
	// {
	// 	$this->layout = "";
	// 	$this->load->view('login/login');
	// }

	public function login_process()
	{  

        $this->layout = "";
		$email=$this->input->post('email');
		$password=$this->input->post('password');

		$data=$this->Admin_model->admin_login($email,$password);
		

		if(isset($data) && !empty($data))
		{
      		$sessiondata =  array
           		(
           			'user_id' =>$data[0]['user_id'],
           			'image' =>$data[0]['image'],

					'user_email' =>$data[0]['user_email'],
					'role' =>$data[0]['role'],
					'user_name' =>$data[0]['user_name'],
            	);
            
		        $this->session->set_userdata($sessiondata);
		        $this->load->view('layouts/admin/dashboard',$sessiondata);
                redirect('admin/dashboard/Dashboard');
        }
        else
        {
        	redirect('admin/login');
        }

	}
  
	public function available_email()
	{
		$this->layout = "";
		if(!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL))  
		{  
			echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Invalid Email</span></label>';
		}
		else
		{
			echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span>valid Email</span></label>';  
		} 
	}

	public function unset_session()
	{
         $this->session->unset_userdata('user_email');
         $this->session->unset_userdata('role');
        redirect('admin/login');
	}
	
	public function setpassword($code)
	{
		$this->layout = "";
		$data['code']=$code;
		$this->load->view('login/resetpasword',$data);
	}

	public function resetpasword()
	{
		$this->layout = "";
		$code=$this->input->post('code');
		$this->form_validation->set_rules('npassword','New password','required');
		$this->form_validation->set_rules('cpassword','Confrim password','required');
		
		if($this->form_validation->run()==false)
		{
			$this->setpassword($code);
		}
		else
		{
			$npassword=$this->input->post('npassword');
			$cpassword=$this->input->post('cpassword');
			
				if($npassword==$cpassword)
				{
					$data = array('user_password' => $npassword);
					$this->Admin_model->update_by('code',$code,$data);
					//redirect('follow_upC/allFollowUp');	
				}
				else
				{
					$this->session->set_flashdata('message','Password does not match');
					$this->setpassword($code);
				}
		}
	}

	public function forgetpassword_load()
	{ 
		$this->layout = "";
		$this->load->view('login/forgetpassword');
	}

	public function sendmail()
	{
		$this->form_validation->set_rules('email','Email','required|valid_email');
		
		if($this->form_validation->run()==false)
		{
			$this->forgetpassword_load();
		}
		else
		{
			$email=$this->input->post('email');
			$data=$this->Admin_model->comparemail($email);
				
				if($data)
				{
					$config = array
					(
						'protocal' => 'smtp',
						'smtp_host'=>'ssl://smtp.googlemail.com',
						'smtp_port'=>465,
						'smtp_user'=>'webdeveloper631@gmail.com',
						'smtp_pass'=>'rescue1122',
					);
					$code=uniqid();
					$path=base_url('admin/login/setpassword/'.$code);
					$this->load->library('email');
					$this->email->from('webdeveloper631@gmail.com', 'Mudassir');
					$this->email->set_newline("\r\n");
					$this->email->to($email);
					$this->email->subject('Password Reset');
					$this->email->message($path);
						
						if($this->email->send())
						{
							$data = array
							('code' =>$code );
							$this->Admin_model->update_by_where($data,$email);
							$this->session->set_flashdata('msg','Check You Email');
							$this->forgetpassword_load();
						}
				}
				else
				{
					$this->session->set_flashdata('msg','Email not Exit');
					$this->forgetpassword_load();
				}
		}
	}
	

}
?>