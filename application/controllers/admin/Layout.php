<?php
class Layout extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
       
        // $this->load->library('image_lib');
        $this->load->model('layout_model','tw');
         $this->load->library('form_validation');
          $this->layout = "admin/dashboard";
    }
 
      public function index()
    {
        if($this->session->userdata('user_email'))
      {
       $data['products'] = $this->tw->get_all('','','id','DESC');
        $this->load->view('layout/index',$data);
      }
        else
        {
          redirect('admin/login');
        }
    }

      public function aboutus()
    {
        if($this->session->userdata('user_email'))
      {
       $data['products'] = $this->tw->get_contects();
       // debug($data['products'],true);
        $this->load->view('aboutpage/updates',$data);
      }
        else
        {
          redirect('admin/login');
        }
    }

         public function contectus()
    {
        if($this->session->userdata('user_email'))
      {
       $data['products'] = $this->tw->get_contect();
       // debug($data['products'],true);
        $this->load->view('contectpage/contect/updates',$data);
      }
        else
        {
          redirect('admin/login');
        }
    }

        public function carears()
    {
        if($this->session->userdata('user_email'))
      {
       $data['products'] = $this->tw->get_contectss();
       // debug($data['products'],true);
        $this->load->view('careers/updates',$data);
      }
        else
        {
          redirect('admin/login');
        }
    }

 public function updatess($id)
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
   
        if (isset($id) && !empty($id)) {
            $data['data'] = $this->tw->get_by('id', $id);
            if (isset($data['data']) && !empty($data['data'])) {
                $data['data'] = $data['data'][0];
              
                $this->load->view('careers/update',$data);
            } else {
                $this->session->set_flashdata('error_message', 'Data not found.');
                redirect('admin/Layout/carears');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to update product.');
          
            redirect('admin/Layout/carears',$data);
        }
      }
      else
      {
         redirect('admin/login');
      } 
    }

    
 public function update($id)
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
   
        if (isset($id) && !empty($id)) {
            $data['data'] = $this->tw->get_by('id', $id);
            if (isset($data['data']) && !empty($data['data'])) {
                $data['data'] = $data['data'][0];
              
                $this->load->view('layout/update',$data);
            } else {
                $this->session->set_flashdata('error_message', 'Data not found.');
                redirect('admin/Layout/');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to update product.');
          
            redirect('admin/Layout/',$data);
        }
      }
      else
      {
         redirect('admin/login');
      } 
    }
     public function updates($id)
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
   
        if (isset($id) && !empty($id)) {
            $data['data'] = $this->tw->get_by('id', $id);
            if (isset($data['data']) && !empty($data['data'])) {
                $data['data'] = $data['data'][0];
              
                $this->load->view('aboutpage/update',$data);
            } else {
                $this->session->set_flashdata('error_message', 'Data not found.');
                redirect('admin/Layout/');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to update product.');
          
            redirect('admin/Layout/',$data);
        }
      }
      else
      {
         redirect('admin/login');
      } 
    }
     public function process_update()
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
        if ($this->input->post('id')) {
        $id = $this->input->post('id');
  
                $data = $this->input->post();
        if(isset($_FILES['head_section_image']['tmp_name']) && !empty($_FILES['head_section_image']['tmp_name']))
        {
          $config['upload_path']   =BASEPATH.'../assets1/images/'; 
   
          $config['allowed_types'] = 'gif|jpg|png|docs|jpeg'; 
          $config['max_size']      = 10000; 
          $config['max_width']     = 4000; 
          $config['max_height']    = 4000;  
          $this->load->library('upload',$config);
          $this->load->initialize($config);
          if(!$this->upload->do_upload('head_section_image')){
            $error = array('error' => $this->upload->display_errors());
            // debug($error,true);
          }
          else{
                        $uploaded_image = $this->upload->data(); // end image upload
                        $data['head_section_image'] = $uploaded_image['file_name'];
                    }
              }

                if ($this->tw->update_by('id', $id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('admin/Layout/contectus');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('admin/Layout/contectus');
                }
          
            
        }
      }
      else
      {
         redirect('admin/login');
      } 
        
    }


      public function process_updates()
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
        if ($this->input->post('id')) {
        $id = $this->input->post('id');
  
                $data = $this->input->post();
        if(isset($_FILES['head_section_image']['tmp_name']) && !empty($_FILES['head_section_image']['tmp_name']))
        {
          $config['upload_path']   =BASEPATH.'../assets1/images/'; 
   
          $config['allowed_types'] = 'gif|jpg|png|docs|jpeg'; 
          $config['max_size']      = 10000; 
          $config['max_width']     = 4000; 
          $config['max_height']    = 4000;  
          $this->load->library('upload',$config);
          $this->load->initialize($config);
          if(!$this->upload->do_upload('head_section_image')){
            $error = array('error' => $this->upload->display_errors());
            // debug($error,true);
          }
          else{
                        $uploaded_image = $this->upload->data(); // end image upload
                        $data['head_section_image'] = $uploaded_image['file_name'];
                    }
              }

                if ($this->tw->update_by('id', $id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('admin/Layout/aboutus');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('admin/Layout/aboutus');
                }
          
            
        }
      }
      else
      {
         redirect('admin/login');
      } 
        
    }
     public function process_updatess()
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
        if ($this->input->post('id')) {
        $id = $this->input->post('id');
  
                $data = $this->input->post();
        if(isset($_FILES['head_section_image']['tmp_name']) && !empty($_FILES['head_section_image']['tmp_name']))
        {
          $config['upload_path']   =BASEPATH.'../assets1/images/'; 
   
          $config['allowed_types'] = 'gif|jpg|png|docs|jpeg'; 
          $config['max_size']      = 10000; 
          $config['max_width']     = 4000; 
          $config['max_height']    = 4000;  
          $this->load->library('upload',$config);
          $this->load->initialize($config);
          if(!$this->upload->do_upload('head_section_image')){
            $error = array('error' => $this->upload->display_errors());
            // debug($error,true);
          }
          else{
                        $uploaded_image = $this->upload->data(); // end image upload
                        $data['head_section_image'] = $uploaded_image['file_name'];
                    }
              }

                if ($this->tw->update_by('id', $id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('admin/Layout/carears');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('admin/Layout/carears');
                }
          
            
        }
      }
      else
      {
         redirect('admin/login');
      } 
        
    }
    }

?>