<?php 

class Home_section_4 extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('user_model');
		$this->load->model('Home_section_4_model');
		$this->load->model('Home_section_5_cat_model');
		$this->load->library('form_validation');
	}
	

	public function index()
	{
		if($this->session->userdata('user_email'))
		{
			$data['section_4'] = $this->Home_section_4_model->get_all();
			
			$this->load->view('home/section_4/index',$data);
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function add()
	{
		if($this->session->userdata('user_email'))
		{
			$data['categories'] = $this->Home_section_5_cat_model->get_all();
			$this->load->view('home/section_4/add',$data);
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function insert()
	{
		if($this->session->userdata('user_email'))
		{
			$this->form_validation->set_rules('home_section_4_name',' Name','required');
			$this->form_validation->set_rules('home_section_4_role','Role','required');
			$this->form_validation->set_rules('home_section_4_location_title',' Location','required');
			$this->form_validation->set_rules('home_section_4_location_url',' Location','required');
			$this->form_validation->set_rules('home_section_4_p','paragraphr','required');
			$this->form_validation->set_rules('home_section_4_link',' Link','required');

			if ($this->form_validation->run()==false) 
			{
				$this->add();
			}
			else
			{
				$cat = $this->input->post('cat');
				if(!empty($cat) && isset($cat))
				{
			 	 	$categories = implode(",", $cat);
				}
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
		        $notcat = 'no Select categories';
				$no_img_select = $this->input->post('no_img_select');
				

				$data = array( 
				'home_section_4_name' =>$this->input->post('home_section_4_name'),
				'home_section_4_role' => $this->input->post('home_section_4_role'),
				'home_section_4_location_title' =>$this->input->post('home_section_4_location_title'),
				'home_section_4_location_url' => $this->input->post('home_section_4_location_url'),
				'home_section_4_p' =>$this->input->post('home_section_4_p'),
				'home_section_4_link' => $this->input->post('home_section_4_link'),
				'home_section_4_img' =>!empty($photo)?$photo:$no_img_select,
				'home_section_4_post_cat' =>!empty($categories)?$categories:$notcat
				);
				// echo "<pre>";
				// print_r($data);
				// exit;
				$this->Home_section_4_model->insert_data($data);
				$this->session->set_flashdata('success_data', 'data has been Added successfully');
			 	redirect('admin/Home_section_4');
			}
		}
	    else
	    {
	    	redirect('admin/login');
	    }	
	}
	
	public function delete($home_section_4_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($home_section_4_id) && !empty($home_section_4_id))
	       	{
	            $this->Home_section_4_model->delete_data('home_section_4','home_section_4_id',$home_section_4_id);
	            $this->session->set_flashdata('success_message', 'Data has been deleted successfully');
	        }
	        else
	        {
	            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
	        }
	        redirect('admin/Home_section_4');
	    }
	    else
	    {
	    	redirect('admin/login');
	    }	
    }
    
	public function update($home_section_4_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       if(isset($home_section_4_id) && !empty($home_section_4_id))
	       	{
	       		$data['categories'] = $this->Home_section_5_cat_model->get_all();
	            $data['data'] = $this->Home_section_4_model->get_by('home_section_4_id',$home_section_4_id);
	            // echo "<pre>";
	            // print_r($data);
	            // exit;
				$this->load->view('home/section_4/update',$data);
	        }
        }
	    else
	    {
	    	redirect('admin/login');
	    }	
    }
    
    public function update_data()
    {
    	if($this->session->userdata('user_email'))
		{
	    	$this->form_validation->set_rules('home_section_4_name',' Name','required');
			$this->form_validation->set_rules('home_section_4_role','Role','required');
			$this->form_validation->set_rules('home_section_4_location_title',' Location','required');
			$this->form_validation->set_rules('home_section_4_location_url',' Location','required');
			$this->form_validation->set_rules('home_section_4_p','paragraphr','required');
			$this->form_validation->set_rules('home_section_4_link',' Link','required');
			$home_section_4_id = $this->input->post('home_section_4_id');
			if ($this->form_validation->run()==false) 
			{
				$this->update($home_section_4_id);
			}
			else
			{
				$cat = $this->input->post('cat');
			 	 $categories = implode(",", $cat);
			 	  $old_cat = $this->input->post('old_cat');
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$old_image = $this->input->post('old_image');
				$data = array( 
				'home_section_4_name' =>$this->input->post('home_section_4_name'),
				'home_section_4_role' => $this->input->post('home_section_4_role'),
				'home_section_4_location_title' =>$this->input->post('home_section_4_location_title'),
				'home_section_4_location_url' => $this->input->post('home_section_4_location_url'),
				'home_section_4_p' =>$this->input->post('home_section_4_p'),
				'home_section_4_link' => $this->input->post('home_section_4_link'),
				'home_section_4_img' =>!empty($photo)?$photo:$old_image,
				'home_section_4_post_cat' =>!empty($categories)?$categories:$old_cat
				);
		    	$this->Home_section_4_model->update_by('home_section_4_id', $home_section_4_id, $data);
		    	redirect('admin/Home_section_4');
		    }
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	
}

?>