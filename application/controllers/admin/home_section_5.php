<?php 

class Home_section_5 extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('user_model');
		$this->load->model('Home_section_5_model');
		$this->load->model('Home_section_5_cat_model');
		
		$this->load->library('form_validation');
	}

	public function index()
	{
		if($this->session->userdata('user_email'))
		{
			$data['section_5'] = $this->Home_section_5_model->get_all();
			// echo "<pre>";
			// print_r($data);exit;
			$this->load->view('home/section_5/index',$data);
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
	}



	public function add()
	{
		if($this->session->userdata('user_email'))
		{
			$data['section_5_cat'] = $this->Home_section_5_cat_model->get_all();

			$this->load->view('home/section_5/add',$data);
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function insert()
	{
		if($this->session->userdata('user_email'))
		{

			$this->form_validation->set_rules('home_section_5_post_title',' Post Title','required');
			$this->form_validation->set_rules('home_section_5_post_writer','Post Writer Name','required');
			$this->form_validation->set_rules('date','Date','required');
			if ($this->form_validation->run()==false) 
			{
				$this->index();
			}
			else
			{
			
				$cat = $this->input->post('cat');
				if(!empty($cat) && isset($cat))
				{
			 	 	$categories = implode(",", $cat);
				}
			 	 
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
		        $notcat = 'no Select categories';
		        $no_img_select = $this->input->post('no_img_select');
				$data = array( 
					'home_section_5_post_title' =>$this->input->post('home_section_5_post_title'),
					'home_section_5_post_writer' => $this->input->post('home_section_5_post_writer'),
					'home_section_5_post_image' =>!empty($photo)?$photo:$no_img_select,
					'home_section_5_post_date' => $this->input->post('date'),
					'home_section_5_post_cat' =>!empty($categories)?$categories:$notcat

				);
				$this->Home_section_5_model->save($data);
				
					$this->session->set_flashdata('success_message', 'successfully inserted data.');
					redirect('admin/Home_section_5');	
			}
		}
	    else
	    {
	    	redirect('admin/login');
	    }	
	}
	
	public function delete($home_section_5_post_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($home_section_5_post_id) && !empty($home_section_5_post_id))
	       	{
	            $this->Home_section_5_model->delete_data('home_section_5_post','home_section_5_post_id',$home_section_5_post_id);
	            $this->session->set_flashdata('success_message', 'Data has been deleted successfully');
	        }
	        else
	        {
	            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
	        }
	        redirect('admin/Home_section_5');
        }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
	public function update($home_section_5_post_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       if(isset($home_section_5_post_id) && !empty($home_section_5_post_id))
	       	{
	       		$data['section_5_cat'] = $this->Home_section_5_cat_model->get_all();
	            $data['data'] = $this->Home_section_5_model->get_by('home_section_5_post_id',$home_section_5_post_id);
				$this->load->view('home/section_5/update',$data);
	        }
	    }
	    else
	    {
	    	redirect('admin/login');
	    } 
    }
    
    public function update_data()
    {
    	if($this->session->userdata('user_email'))
		{
	    	$this->form_validation->set_rules('home_section_5_post_title',' Name','required');
			$this->form_validation->set_rules('home_section_5_post_writer','Role','required');

			$home_section_5_post_id = $this->input->post('home_section_5_post_id');

			$cat = $this->input->post('cat');
			 	 $categories = implode(",", $cat);
			 	  $old_cat = $this->input->post('old_cat');

			if ($this->form_validation->run()==false) 
			{
				$this->update($home_section_5_post_id);
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$old_image = $this->input->post('old_image');
				$data = array( 

				'home_section_5_post_title' => $this->input->post('home_section_5_post_title'),
				'home_section_5_post_writer' =>$this->input->post('home_section_5_post_writer'),
				'home_section_5_post_date' =>$this->input->post('date'),
				'home_section_5_post_image' =>!empty($photo)?$photo:$old_image,
				'home_section_5_post_cat' =>!empty($categories)?$categories:$old_cat

				);

		    	$this->Home_section_5_model->update_by('home_section_5_post_id', $home_section_5_post_id, $data);
		    	redirect('admin/Home_section_5');
		    }
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function all_article()
	{
		if($this->session->userdata('user_email'))
		{
			$data['section_5_article'] = $this->Home_section_5_model->all_data('home_section_5_post_article');
			$this->load->view('home/section_5/all_article',$data);
		}
	    else
	    {
	    	redirect('admin/login');
	    }	
	}
	public function add_article()
	{
		if($this->session->userdata('user_email'))
		{
			$data['section_5_cat'] = $this->Home_section_5_cat_model->get_all();
			$this->load->view('home/section_5/add_article',$data);
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}
	public function insert_article()
	{
		if($this->session->userdata('user_email'))
		{
			$this->form_validation->set_rules('home_section_5_post_article_title',' Name','required');
			$this->form_validation->set_rules('home_section_5_post_article_writer','Role','required');

			if ($this->form_validation->run()==false) 
			{
				$this->add_article();
			}
			else
			{
				$cat = $this->input->post('cat');
			 	 $categories = implode(",", $cat);

				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$no_img_select = $this->input->post('no_img_select');
				$notcat = 'no Select categories';

				$data = array( 
				'home_section_5_post_article_title' => $this->input->post('home_section_5_post_article_title'),
				'home_section_5_post_article_writer' =>$this->input->post('home_section_5_post_article_writer'),
				'home_section_5_post_article_date' => $this->input->post('date'),
				'home_section_5_post_article_image' =>!empty($photo)?$photo:$no_img_select,
				'home_section_5_post_cat' =>!empty($photo)?$categories:$notcat

				);
				// echo "<pre>";
				// print_r($data);
				// exit;
				$this->Home_section_5_model->insert_into('home_section_5_post_article',$data);
				$this->session->set_flashdata('success_data', 'data has been Added successfully');
			 	redirect('admin/Home_section_5/all_article');
			}
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}


	public function delete_article($home_section_5_post_article_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($home_section_5_post_article_id) && !empty($home_section_5_post_article_id))
	       	{
	            $this->Home_section_5_model->delete_data('home_section_5_post_article','home_section_5_post_article_id',$home_section_5_post_article_id);
	            $this->session->set_flashdata('success_message', 'Data has been deleted successfully');
	        }
	        else
	        {
	            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
	        }
	        redirect('admin/Home_section_5/all_article');
       	}
	    else
	    {
	    	redirect('admin/login');
	    }
    }

    public function update_article($home_section_5_post_article_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       if(isset($home_section_5_post_article_id) && !empty($home_section_5_post_article_id))
	       	{
	       		$data['section_5_cat'] = $this->Home_section_5_cat_model->get_all();
	            $data['data'] = $this->Home_section_5_model->specfic_data('home_section_5_post_article','home_section_5_post_article_id',$home_section_5_post_article_id);
	            // echo "<pre>"; print_r($data);
	            // exit();
				$this->load->view('home/section_5/update_article',$data);
			}
        }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
    public function update_article_process()
    {
    	if($this->session->userdata('user_email'))
		{	
	    	$this->form_validation->set_rules('home_section_5_post_article_title',' Name','required');
			$this->form_validation->set_rules('home_section_5_post_article_writer','Role','required');

			$home_section_5_post_article_id = $this->input->post('home_section_5_post_article_id');

			$cat = $this->input->post('cat');
			if(isset($cat) && !empty($cat))
			{
			 	$categories = implode(",", $cat);
			}
			 	  $old_cat = $this->input->post('old_cat');

			if ($this->form_validation->run()==false) 
			{
				$this->update_article($home_section_5_post_article_id);
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$old_image = $this->input->post('old_image');
				$data = array( 

				'home_section_5_post_article_title' => $this->input->post('home_section_5_post_article_title'),
				'home_section_5_post_article_writer' =>$this->input->post('home_section_5_post_article_writer'),
				'home_section_5_post_article_date' => $this->input->post('date'),
				'home_section_5_post_article_image' =>!empty($photo)?$photo:$old_image,
				'home_section_5_post_cat' =>!empty($categories)?$categories:$old_cat

				);
				// echo "<pre>";
				// print_r($data);
				// exit();

		    	$this->Home_section_5_model->updated_data('home_section_5_post_article','home_section_5_post_article_id', $home_section_5_post_article_id, $data);
		    	redirect('admin/Home_section_5/all_article');
		    }
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function all_cat()
	{
		if($this->session->userdata('user_email'))
		{
			$data['cat'] = $this->Home_section_5_cat_model->get_all();
			// echo "<pre>";
			// print_r($data);exit;
			$this->load->view('home/section_5_cat/index',$data);
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function add_cat()
	{
		if($this->session->userdata('user_email'))
		{
			$this->load->view('home/section_5_cat/add');
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function insert_cat()
	{
		if($this->session->userdata('user_email'))
		{
			$this->form_validation->set_rules('home_section_5_post_categorie_text',' category','required');
			
			if ($this->form_validation->run()==false) 
			{
				$this->add_cat();
			}
			else
			{
				$data = array( 
					'home_section_5_post_categorie_text' =>$this->input->post('home_section_5_post_categorie_text')
				);
				$this->Home_section_5_cat_model->save($data);
				
					$this->session->set_flashdata('success_message', 'successfully inserted data.');
					redirect('admin/Home_section_5/all_cat');
			}
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}


	public function delete_cat($home_section_5_post_categories_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($home_section_5_post_categories_id) && !empty($home_section_5_post_categories_id))
	       	{
	            $this->Home_section_5_cat_model->delete_data('home_section_5_post_categories','home_section_5_post_categories_id',$home_section_5_post_categories_id);
	            $this->session->set_flashdata('success_message', 'Data has been deleted successfully');
	        }
	        else
	        {
	            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
	        }
	        redirect('admin/Home_section_5/all_cat');
        }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    public function update_Cat($home_section_5_post_categories_id)
    {
    	if($this->session->userdata('user_email'))
		{
	    	if(isset($home_section_5_post_categories_id) && !empty($home_section_5_post_categories_id))
	       	{
	            $data['data'] = $this->Home_section_5_model->specfic_data('home_section_5_post_categories','home_section_5_post_categories_id',$home_section_5_post_categories_id);
	            // echo "<pre>"; print_r($data);
	            // exit();
				$this->load->view('home/section_5_cat/update',$data);
			}
        }
	    else
	    {
	    	redirect('admin/login');
	    }
    }

     public function update_cat_process()
    {
    	if($this->session->userdata('user_email'))
		{
	    	$this->form_validation->set_rules('home_section_5_post_categorie_text',' Name','required');

			$home_section_5_post_categories_id = $this->input->post('home_section_5_post_categories_id');
			if ($this->form_validation->run()==false) 
			{
				$this->update_Cat($home_section_5_post_categories_id);
			}
			else
			{
				$data = array( 

				'home_section_5_post_categorie_text' => $this->input->post('home_section_5_post_categorie_text')

				);
				// echo "<pre>";
				// print_r($data);
				// exit();

		    	$this->Home_section_5_model->updated_data('home_section_5_post_categories','home_section_5_post_categories_id', $home_section_5_post_categories_id, $data);
		    	redirect('admin/Home_section_5/all_cat');
		    }
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
	}







}
?>