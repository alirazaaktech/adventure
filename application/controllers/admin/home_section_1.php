<?php 

class Home_section_1 extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('user_model');
		$this->load->model('Home_section_1_model');
		$this->load->library('form_validation');
	}

	public function index()
	{
		if($this->session->userdata('user_email'))
		{
			$data['section_1'] = $this->Home_section_1_model->get_all();
			$this->load->view('home/section_1/index',$data);
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function add()
	{
		if($this->session->userdata('user_email'))
		{
			$this->load->view('home/section_1/add');
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function insert()
	{
		if($this->session->userdata('user_email'))
		{
			$this->form_validation->set_rules('sec_in_h',' Name','required');
			$this->form_validation->set_rules('sec_in_p','Phone Number','required');
			if ($this->form_validation->run()==false) 
			{
				$this->add();
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$no_img_select = $this->input->post('no_img_select');
				

				$data = array( 
				'sec_in_h' =>$this->input->post('sec_in_h'),
				'sec_in_p' => $this->input->post('sec_in_p'),
				'sec_in_image' =>!empty($photo)?$photo:$no_img_select,
				);
				$this->Home_section_1_model->insert_data($data);
				$this->session->set_flashdata('success_data', 'data has been Added successfully');
			 	redirect('admin/Home_section_1');
			}
		}
	    else
	    {
	    	redirect('admin/login');
	    }		
	}
	
	public function delete($sec_in_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($sec_in_id) && !empty($sec_in_id))
	       	{
	            $this->Home_section_1_model->delete_data('home_section_1','sec_in_id',$sec_in_id);
	            $this->session->set_flashdata('success_message', 'User has been deleted successfully');
	        }
	        else
	        {
	            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
	        }
	        redirect('admin/Home_section_1');
        }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
	public function update($sec_in_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($sec_in_id))
	       	{
	            $data['data'] = $this->Home_section_1_model->get_by('sec_in_id',$sec_in_id);
				$this->load->view('home/section_1/update',$data);
	        }
	    }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
    public function update_data()
    {
    	if($this->session->userdata('user_email'))
		{
	    	$this->form_validation->set_rules('sec_in_h',' Name','required');
			$this->form_validation->set_rules('sec_in_p','Phone Number','required');
			$sec_in_id = $this->input->post('sec_in_id');
			if ($this->form_validation->run()==false) 
			{
				$this->update($sec_in_id);
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$old_image = $this->input->post('old_image');
				$data = array( 

				'sec_in_h' =>$this->input->post('sec_in_h'),
				'sec_in_p' => $this->input->post('sec_in_p'),
				'sec_in_image' =>!empty($photo)?$photo:$old_image
				);
		    	$this->Home_section_1_model->update_by('sec_in_id', $sec_in_id, $data);
		    	redirect('admin/Home_section_1');
		    }
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	
}

?>