<?php
class Value extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
       
        // $this->load->library('image_lib');
        $this->load->model('value_model','vm');
         $this->load->library('form_validation');
          $this->layout = "admin/dashboard";
    }
 
      public function add()
    {
      if($this->session->userdata('user_email'))
      {
        $this->load->view('aboutpage/valuesection/add');
      }
      else
      {
        redirect('admin/login');
      }
    }
      public function index()
    {
      if($this->session->userdata('user_email'))
      {
        $data['products'] = $this->vm->get_all('','','id','DESC');
        $this->load->view('aboutpage/valuesection/index',$data);
      }
      else
      {
        redirect('admin/login');
      }
    }
    
    
    public function process_add()
     {
      if($this->session->userdata('user_email'))
      {  
        $data = array();
          if ($this->input->post()) {

        
         $this->form_validation->set_rules('heading','heading','required');
            // $this->form_validation->set_rules('logo','logo','required');
            
           
           if ($this->form_validation->run() === TRUE ){
                  $data = $this->input->post();
               
                 
                  
                  if(isset($_FILES['image']['tmp_name']))
                  {
               $config['upload_path']   = BASEPATH.'../assets1/images/';
                     // echo $config['upload_path'];
                      $config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
                      $config['max_size']      = 10000;
                      $config['max_width']     = 4000;
                      $config['max_height']    = 4000;
                      $this->load->library('upload',$config);
                      $this->load->initialize($config);
                      if(!$this->upload->do_upload('image')){
                          $error = array('error' => $this->upload->display_errors());
                          debug($error,true);
                      }
               
                   else{
                            $uploaded_image = $this->upload->data(); // end image upload
                            $data['image'] = $uploaded_image['file_name'];
                        }
                  }

                    if($category_id = $this->vm->save($data)) {
                        $this->session->set_flashdata('success_message', 'Data has been saved successfully');

                        redirect('admin/Value/');
                    } else {
                        $this->session->set_flashdata('error_message', 'Error occured while saving Data.');
                        redirect('admin/Value/add');
                    }
                }else{
                   $this->load->view('aboutpage/valuesection/add',$data);
                }
            } else {
                $this->session->set_flashdata('error_message', 'Error occured while saving category.');
                redirect('admin/Value/add/');
            }
        }
        else
        {
          redirect('admin/login');
        }
      }
        
    public function delete($id)
    {
      if($this->session->userdata('user_email'))
      {  
      // debug($id,true);
        if (isset($id) && !empty($id)) {
              
             $this->vm->delete_data("about_value_section","id",$id);

            // $this->v->delete_by('id', $id);
            // $this->db->last_query();exit();
            $this->session->set_flashdata('success_message', 'heading has been deleted successfully');
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to delete heading.');
        }
        redirect('admin/Value/index/');
       }
        else
        {
          redirect('admin/login');
        }
    }


     public function update($id)
    {
      if($this->session->userdata('user_email'))
      {  
        $data = array();
   
        if (isset($id) && !empty($id)) {
            $data['data'] = $this->vm->get_by('id', $id);
            if (isset($data['data']) && !empty($data['data'])) {
                $data['data'] = $data['data'][0];
                 $all_activ = $this->vm->get_by('id', $id);
                $data['all_activ'] = $all_activ[0];
                $this->load->view('aboutpage/valuesection/update',$data);
            } else {
                $this->session->set_flashdata('error_message', 'Data not found.');
                redirect('admin/Value/');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to update product.');
          
            redirect('admin/Value/',$data);
        }
      }
      else
      {
        redirect('admin/login');
      }
    }

    public function process_update()
    {
      if($this->session->userdata('user_email'))
      { 
        $data = array();
        if ($this->input->post('id')) {
        $id = $this->input->post('id');
      $this->form_validation->set_rules('heading','heading','required');
     if ($this->form_validation->run() === TRUE) {
                $data = $this->input->post();
        if(isset($_FILES['image']['tmp_name']) && !empty($_FILES['image']['tmp_name']))
        {
          $config['upload_path']   =BASEPATH.'../assets1/images/'; 
   
          $config['allowed_types'] = 'gif|jpg|png|docs|jpeg'; 
          $config['max_size']      = 10000; 
          $config['max_width']     = 4000; 
          $config['max_height']    = 4000;  
          $this->load->library('upload',$config);
          $this->load->initialize($config);
          if(!$this->upload->do_upload('image')){
            $error = array('error' => $this->upload->display_errors());
            // debug($error,true);
          }
          else{
                        $uploaded_image = $this->upload->data(); // end image upload
                        $data['image'] = $uploaded_image['file_name'];
                    }
              }

                if ($this->vm->update_by('id', $id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('admin/Value/index');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('admin/Value/');
                }
            }
            else{
              
              $data['data'] = $this->vm->get_by('id', $id);  
              
            
            $data['data'] = $data['data'][0];
          
            $this->load->view('aboutpage/valuesection/update', $data);
            }
            
        }
      }
      else
      {
        redirect('admin/login');
       }
        
    }
}

?>