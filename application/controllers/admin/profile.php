<?php 

class Profile extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->layout = "admin/dashboard";
		$this->load->library('image_lib');
		$this->load->model('user_model');
		$this->load->library('form_validation');
		$this->load->library('session');
	}
	
	public function index($tab='')
	{		
		if($this->session->userdata('user_id'))
			{
				$user_id= $this->session->userdata('user_id');
				$profile['tab'] = $tab;
				$profile['key'] = $this->user_model->get_by('user_id',$user_id);
				$this->load->view('user/view_profile',$profile);
		    }
		    else
		    {
		    	redirect('admin/login');
		    }
	}

	public function my_load()
	{
		if($this->session->userdata('user_id'))
		{
			$config= array();
			$config['upload_path'] = './assets1/images/';
			$config['allowed_types'] = 'gif|jpg|png';
			$this->load->library('upload',$config);
			
			$this->upload->do_upload('photo');
			$data = $this->upload->data();
			if($data) 
			{
				$image = $data['file_name']; 
				return $image;
			}
		}
     	else
      	{
        	 redirect('admin/login');
      	} 
	}
	
	public function update_data()
    {
    	if($this->session->userdata('user_id'))
		{
	    	$this->form_validation->set_rules('name',' Name','required');
			$this->form_validation->set_rules('pno','Phone Number','required');
			$this->form_validation->set_rules('address','Address','required');
			if ($this->form_validation->run()==false) 
			{
				$this->index();
			}
			else
			{
				if ($_FILES)
				{
					$image = $this->my_load();

				}
					$user_id = $this->input->post('user_id');
					$old_image = $this->input->post('old_image');
				
					$data = array( 
					'user_name' =>$this->input->post('name'),
					'user_phone' => $this->input->post('pno'),
					'user_address' => $this->input->post('address'),
					'image' =>!empty($image)?$image:$old_image
					);
					
					$sessiondata =  array
	           		(
						'user_name' =>$this->input->post('name'),
						'image' =>!empty($image)?$image:$old_image
					);

			    	$this->user_model->update_by('user_id', $user_id, $data);
				    $this->session->set_userdata($sessiondata);
		    		redirect('admin/profile/index');
		    }
		}
     	else
      	{
        	 redirect('admin/login');
      	} 
	}


	public function change_password()
	{
		if($this->session->userdata('user_id'))
		{
			$user_id = $this->input->post('user_id');
			$current_password = $this->input->post('cpass');
						$tab =  $this->input->post('tab');
			$data = $this->user_model->password_compare($current_password,$user_id);
			if(isset($data))
			{
				$npass = $this->input->post('npass');
				$rpass = $this->input->post('rpass');
			

				if ($rpass==$npass) 
				{
					$update_password = array(
						'user_password' => $npass
					);
					$this->user_model->update_by('user_id',$user_id,$update_password);
					$this->session->set_flashdata('success', 'Successfully Password Changed');
					redirect('admin/profile/index/'.$tab);
				}
				else
				{
					$this->session->set_flashdata('error_new', 'New Password does not match');
					redirect('admin/profile/index/'.$tab);
				}
			}
			else
			{
				$this->session->set_flashdata('error_current', 'Current  Password does not match');
				redirect('admin/profile/index/'.$tab);
			}
		}
     	else
      	{
        	 redirect('admin/login');
      	} 	
	}

}

?>