<?php
class License extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
       
        // $this->load->library('image_lib');
        $this->load->model('licenese_model','vm');
         $this->load->library('form_validation');
          $this->layout = "admin/dashboard";
    }
 
    public function add()
    {
      if($this->session->userdata('user_email'))
      {
          $this->load->view('aboutpage/license/add');
      }
      else
      {
         redirect('admin/login');
      } 
    }
      public function index()
    {
       if($this->session->userdata('user_email'))
      {
        $data['products'] = $this->vm->get_all('','','id','DESC');
        $data['head'] = $this->vm->get_para('','','id','DESC');
        //debug($data['products'],true);
        $this->load->view('aboutpage/license/index',$data);
      }
      else
      {
         redirect('admin/login');
      } 
    }
    
    
    public function process_add()
    {  
      if($this->session->userdata('user_email'))
      {
        $data = array();
          if ($this->input->post()) {

         $data = $this->input->post();
         //debug($data,true);
               if(isset($_FILES['logo']['tmp_name']))
                  {
               $config['upload_path']   = BASEPATH.'../assets1/images/';
                     // echo $config['upload_path'];
                      $config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
                      $config['max_size']      = 10000;
                      $config['max_width']     = 4000;
                      $config['max_height']    = 4000;
                      $this->load->library('upload',$config);
                      $this->load->initialize($config);
                      if(!$this->upload->do_upload('logo')){
                          $error = array('error' => $this->upload->display_errors());
                          debug($error,true);
                      }
               
                   else{
                            $uploaded_image = $this->upload->data(); // end image upload
                            $data['logo'] = $uploaded_image['file_name'];
                        }
                  }

                    if($category_id = $this->vm->save($data)) {
                      //debug($category_id,true);
                        $this->session->set_flashdata('success_message', 'Data has been saved successfully');

                        redirect('admin/License/');
                    } else {
                        $this->session->set_flashdata('error_message', 'Error occured while saving Data.');
                        redirect('admin/License/add');
                    }
                
            } else {
                $this->session->set_flashdata('error_message', 'Error occured while saving category.');
                redirect('admin/License/add/');
            }
      }
      else
      {
         redirect('admin/login');
      } 
    }

    public function process_adds()
    { 
     if($this->session->userdata('user_email'))
      { 
        $data = array();
        if ($this->input->post()) {

        $data = $this->input->post();
       //debug($data,true);
 

                  if($category_id = $this->vm->save($data)) {
                    //debug($category_id,true);
                      $this->session->set_flashdata('success_message', 'Data has been saved successfully');

                      redirect('admin/License/');
                  } else {
                      $this->session->set_flashdata('error_message', 'Error occured while saving Data.');
                      redirect('admin/License/add');
                  }
              
          } else {
              $this->session->set_flashdata('error_message', 'Error occured while saving category.');
              redirect('admin/License/add/');
          }
      }
      else
      {
         redirect('admin/login');
      } 
    }

    public function delete($id)
    {
      if($this->session->userdata('user_email'))
      {
      // debug($id,true);
        if (isset($id) && !empty($id)) {
              
             $this->vm->delete_data("about_license","id",$id);

            // $this->v->delete_by('id', $id);
            // $this->db->last_query();exit();
            $this->session->set_flashdata('success_message', 'heading has been deleted successfully');
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to delete heading.');
        }
        redirect('admin/License/index/');
      }
      else
      {
         redirect('admin/login');
      } 
    }


     public function update($id)
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
   
        if (isset($id) && !empty($id)) {
            $data['data'] = $this->vm->get_by('id', $id);
            if (isset($data['data']) && !empty($data['data'])) {
                $data['data'] = $data['data'][0];
                 $all_activ = $this->vm->get_by('id', $id);
                $data['all_activ'] = $all_activ[0];
                $this->load->view('aboutpage/license/update',$data);
            } else {
                $this->session->set_flashdata('error_message', 'Data not found.');
                redirect('admin/License/');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to update product.');
          
            redirect('admin/License/',$data);
        }
      }
      else
      {
         redirect('admin/login');
      } 
    }

    public function updates($id)
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
   
        if (isset($id) && !empty($id)) {
            $data['data'] = $this->vm->get_by('id', $id);
            if (isset($data['data']) && !empty($data['data'])) {
                $data['data'] = $data['data'][0];
                 $all_activ = $this->vm->get_by('id', $id);
                $data['all_activ'] = $all_activ[0];
                $this->load->view('aboutpage/license/updates',$data);
            } else {
                $this->session->set_flashdata('error_message', 'Data not found.');
                redirect('admin/License/');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to update product.');
          
            redirect('admin/License/',$data);
        }
      }
      else
      {
         redirect('admin/login');
      } 
    }

    public function process_updates()
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
        if ($this->input->post('id')) {
        $id = $this->input->post('id');
  
                $data = $this->input->post();


                if ($this->vm->update_by('id', $id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('admin/License/index');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('admin/License/');
                }
          
            
        }
      }
      else
      {
         redirect('admin/login');
      } 
        
    }

    public function process_update()
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
        if ($this->input->post('id')) {
        $id = $this->input->post('id');
  
                $data = $this->input->post();
        if(isset($_FILES['logo']['tmp_name']) && !empty($_FILES['logo']['tmp_name']))
        {
          $config['upload_path']   =BASEPATH.'../assets1/images/'; 
   
          $config['allowed_types'] = 'gif|jpg|png|docs|jpeg'; 
          $config['max_size']      = 10000; 
          $config['max_width']     = 4000; 
          $config['max_height']    = 4000;  
          $this->load->library('upload',$config);
          $this->load->initialize($config);
          if(!$this->upload->do_upload('logo')){
            $error = array('error' => $this->upload->display_errors());
            // debug($error,true);
          }
          else{
                        $uploaded_image = $this->upload->data(); // end image upload
                        $data['logo'] = $uploaded_image['file_name'];
                    }
              }

                if ($this->vm->update_by('id', $id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('admin/License/index');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('admin/License/');
                }
          
            
        }
      }
      else
      {
         redirect('admin/login');
      } 
        
    }
}

?>