<?php
class Home_slider extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('image_lib');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('');
		$this->load->model('Home_slider_model');
		$this->layout = "admin/dashboard";
	}

	public function index()
	{
		if($this->session->userdata('user_email'))
		{	
			$data['slider'] = $this->Home_slider_model->get_all();
			//print_r($data);exit;
			$this->load->view('home/slider/index',$data);
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}
	
	public function add()
	{

		if($this->session->userdata('user_email'))
		{
			$this->load->view('home/slider/add');
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	public function insert()
	{
		if($this->session->userdata('user_email'))
		{
			$this->form_validation->set_rules('name',' Name','required');
			$this->form_validation->set_rules('role',' Role','required');
			$this->form_validation->set_rules('country',' Name','required');
			$this->form_validation->set_rules('home_slider_p',' Name','required');
			if ($this->form_validation->run()==false) 
			{
				$this->add();
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{
		       		
					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$no_img_select = $this->input->post('no_img_select');
				

				$data = array( 
				'name' =>$this->input->post('name'),
				'country' =>$this->input->post('country'),
				'home_slider_p' =>$this->input->post('home_slider_p'),
				'role' => $this->input->post('role'),
				'home_slider_img' =>!empty($photo)?$photo:$no_img_select,
				);
				// echo "<pre>";
				// print_r($data);exit;
				$this->Home_slider_model->save($data);
				$this->session->set_flashdata('success_data', 'data has been Added successfully');
			 	redirect('admin/Home_slider');
			}
		}
	    else
	    {
	    	redirect('admin/login');
	    }	
	}
	
	public function delete($home_slider_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($home_slider_id) && !empty($home_slider_id))
	       	{
	            $this->Home_slider_model->delete_data('home_slider','home_slider_id',$home_slider_id);
	            $this->session->set_flashdata('success_message', 'User has been deleted successfully');
	        }
	        else
	        {
	            $this->session->set_flashdata('error_message', 'Invalid request to delete product.');
	        }
	        redirect('admin/Home_slider');
        }
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
	public function update($home_slider_id)
    {
    	if($this->session->userdata('user_email'))
		{
	       	if(isset($home_slider_id))
	       	{
	            $data['data'] = $this->Home_slider_model->get_by('home_slider_id',$home_slider_id);

				$this->load->view('home/slider/update',$data);
	        }
    	}
	    else
	    {
	    	redirect('admin/login');
	    }
    }
    
    public function update_data()
    {
    	if($this->session->userdata('user_email'))
		{
	    	$this->form_validation->set_rules('name',' Name','required');
			$this->form_validation->set_rules('role',' Role','required');
			$this->form_validation->set_rules('country',' Name','required');
			$this->form_validation->set_rules('home_slider_p',' Name','required');
			 $home_slider_id =  $this->input->post('home_slider_id');
			 // debug($id,true);
			if ($this->form_validation->run()==false) 
			{
				$this->update($home_slider_id);
			}
			else
			{
				if(isset($_FILES['photo']['tmp_name']) && !empty($_FILES['photo']['tmp_name']))
		       	{

					$config['upload_path']   = BASEPATH.'../assets1/images/';
					$config['allowed_types'] = 'gif|jpg|png|docs|jpeg';
					$config['max_size']      = 10000;
					$config['max_width']     = 4000;
					$config['max_height']    = 4000;
					$this->load->library('upload',$config);
					$this->load->initialize($config);
					if(!$this->upload->do_upload('photo'))
					{
						$error = array('error' => $this->upload->display_errors());
					// debug($error,true);
					}
					else
					{
						$uploaded_image = $this->upload->data(); // end image upload
						$photo = $uploaded_image['file_name'];
					}
		        }
				$old_image = $this->input->post('old_image');
				$data = array( 

				'name' =>$this->input->post('name'),
				'country' =>$this->input->post('country'),
				'home_slider_p' =>$this->input->post('home_slider_p'),
				'role' => $this->input->post('role'),
				'home_slider_img' =>!empty($photo)?$photo:$old_image
				);
				// debug($data,true);
				
		    	$this->Home_slider_model->update_by('home_slider_id', $home_slider_id, $data);
		    	redirect('admin/Home_slider');
		    }
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	
}

?>