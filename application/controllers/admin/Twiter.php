<?php
class Twiter extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
       
        // $this->load->library('image_lib');
        $this->load->model('twiter_model','tw');
         $this->load->library('form_validation');
          $this->layout = "admin/dashboard";
    }
 
    public function add()
    {
      if($this->session->userdata('user_email'))
      {
        $this->load->view('home/twiter/add');
      }
      else
      {
          redirect('admin/login');
      }

    }
      public function index()
    {
        if($this->session->userdata('user_email'))
      {
       $data['products'] = $this->tw->get_all('','','id','DESC');
        $this->load->view('home/twiter/index',$data);
      }
        else
        {
          redirect('admin/login');
        }
    }
    
    
    public function process_add()
    {  
       if($this->session->userdata('user_email'))
      {

      $data = array();
        if ($data=$this->input->post()) {

    // debug($data,true);
       $this->form_validation->set_rules('name','name','required');
          // $this->form_validation->set_rules('logo','logo','required');
          
         
         if ($this->form_validation->run() === TRUE ){
                $data = $this->input->post();
                
              if($category_id = $this->tw->save($data)) {
                      $this->session->set_flashdata('success_message', 'Data has been saved successfully');

                      redirect('admin/Twiter/index');
                  } else {
                      $this->session->set_flashdata('error_message', 'Error occured while saving Data.');
                      redirect('admin/Twiter/add');
                  }
              }else{
                 $this->load->view('home/twiter/add',$data);
              }
          } else {
              $this->session->set_flashdata('error_message', 'Error occured while saving category.');
              redirect('admin/Twiter/add/');
          }
         }
        else
        {
          redirect('admin/login');
        }
      }
         public function delete($id)
    {
      if($this->session->userdata('user_email'))
      {
      // debug($id,true);
        if (isset($id) && !empty($id)) {
              
             $this->tw->delete_data("twiter","id",$id);

            // $this->v->delete_by('id', $id);
            // $this->db->last_query();exit();
            $this->session->set_flashdata('success_message', 'Data has been deleted successfully');
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to delete heading.');
        }
        redirect('admin/Twiter/index/');
       }
        else
        {
          redirect('admin/login');
        }
    }


     public function update($id)
    {
      if($this->session->userdata('user_email'))
      {
        $data = array();
   
        if (isset($id) && !empty($id)) {
            $data['data'] = $this->tw->get_by('id', $id);
            if (isset($data['data']) && !empty($data['data'])) {
                $data['data'] = $data['data'][0];
                 $all_activ = $this->tw->get_by('id', $id);
                $data['all_activ'] = $all_activ[0];
                $this->load->view('home/twiter/update',$data);
            } else {
                $this->session->set_flashdata('error_message', 'Data not found.');
                redirect('admin/Twiter/');
            }
        } else {
            $this->session->set_flashdata('error_message', 'Invalid request to update product.');
          
            redirect('admin/Twiter/',$data);
        }
       }
        else
        {
          redirect('admin/login');
        }
    }

    public function process_update()
    {
       if($this->session->userdata('user_email'))
      {
        $data = array();
        if ($this->input->post('id')) {
        $id = $this->input->post('id');
      $this->form_validation->set_rules('name','name','required');
     if ($this->form_validation->run() === TRUE) {
                $data = $this->input->post();
     

                if ($this->tw->update_by('id', $id, $data)) {
                    $this->session->set_flashdata('success_message', 'product has been updated successfully');
                    redirect('admin/Twiter/index');
                } else {
                    $this->session->set_flashdata('error_message', 'Error occurred while updated product.');
                    redirect('admin/Twiter/');
                }
            }
            else{
              
              $data['data'] = $this->tw->get_by('id', $id);  
              
            
            $data['data'] = $data['data'][0];
          
            $this->load->view('home/twiter/update', $data);
            }
            
        }
       }
        else
        {
          redirect('admin/login');
        }
        
    }
    }

?>