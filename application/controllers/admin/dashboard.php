

<?php 

class Dashboard extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Dashboard_model','dashboard_model');
		$this->load->library('image_lib');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->layout = "admin/dashboard";
		$this->load->model('Home_section_5_model');
		$this->load->model('Home_section_2_model');
		$this->load->model('Timesection_model');
	
	}
	public function index()
	{
		if($this->session->userdata('user_email'))
		{
			$data['total_post'] = count($this->Home_section_5_model->get_all());
			$data['total_industries'] =count($this->Home_section_2_model->get_all());
			 $data['our_officess'] = count($this->Timesection_model->get_all());
			$this->load->view('dashboard/main_dashboard',$data);
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}

	
	public function dashboard()
	{
		if($this->session->userdata('user_email'))
		{
			$data['total_post'] = count($this->Home_section_5_model->get_all());
			$data['total_industries'] = count($this->Home_section_2_model->get_all());
			$data['our_officess'] = count($this->Timesection_model->get_all());
			$this->load->view('dashboard/main_dashboard',$data);
		}
	    else
	    {
	    	redirect('admin/login');
	    }
	}
	





}
?>		