<style type="text/css">
    
    #color
    {
        color: red;
    }
</style>

  

<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
 
    <div class="row">
        <?php if($this->session->flashdata('success')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
                </div>
        <?php } ?>
        <?php if($this->session->flashdata('error_new')) { ?>
                <div class="alert alert-danger">
                    <strong>Alert!</strong> <?php echo $this->session->flashdata('error_new'); ?>
                </div>
        <?php } ?>
        <?php if($this->session->flashdata('error_current')) { ?>
                <div class="alert alert-danger">
                    <strong>Alert!</strong> <?php echo $this->session->flashdata('error_current'); ?>
                </div>
        <?php } ?>
        <div class="col-md-4">
            <!-- BEGIN PROFILE SIDEBAR -->
            <div class="profile-sidebar">
                <!-- PORTLET MAIN -->
                <div class="portlet light profile-sidebar-portlet bordered">
                    <!-- SIDEBAR USERPIC -->
                     <?php 
                    foreach ($key as $value)
                    {
                       $value['user_id'];
                       $image=  $value['image'];
                       ?>
                    <div class="profile-userpic">
                        <img src="<?php echo base_url()?>assets1/images/<?php  echo $image;  ?>" class="img-responsive" alt=""> 
                    </div>
                    <!-- END SIDEBAR USERPIC -->
                    <!-- SIDEBAR USER TITLE -->
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name uppercase"> <?php echo $value['user_name']; ?> </div>
                        <div class="profile-usertitle-job"><?php echo $value['role']; ?></div>
                    </div>
                    <!-- END SIDEBAR USER TITLE -->
                    <!-- SIDEBAR BUTTONS -->
                   
                   <!--  <div class="profile-userbuttons">
                        <button type="button" class="btn btn-circle green btn-sm">Follow</button>
                        <button type="button" class="btn btn-circle red btn-sm">Message</button>
                    </div> -->
                    <!-- END SIDEBAR BUTTONS -->
                    <!-- SIDEBAR MENU -->
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li>
                                <a>
                                    <i class="fa fa-envelope-o"></i> <?php echo $value['user_email']; ?> </a>
                            </li>
                         <!--    <li>
                                <a href="page_user_profile_1_help.html">
                                    <i class="icon-info"></i> Help </a>
                            </li> -->
                        </ul>
                    </div>
                    <!-- END MENU -->
                <?php } ?>
                </div>
            </div>

                <!-- END PORTLET MAIN -->
                <!-- PORTLET MAIN -->
               
                <!-- END PORTLET MAIN -->
            </div>

            
     
        <div class="col-md-8">
           

                        <div class="portlet light bordered">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">Profile Account</span>
                                </div>
                                <ul class="nav nav-tabs">
                                      <li '<?php if ($tab == '#tab_1_1') {
                                        echo 'class="active"';
                                      } ?> >
                                        <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                    </li>
                                    <li <?php if ($tab == '#tab_1_3') {
                                        echo 'class="active"';
                                      } ?> >
                                        <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                    </li>
                                  
                                  
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <div class="tab-content">
                                    <!-- PERSONAL INFO TAB -->
                                    <div class="tab-pane active" id="tab_1_1">

                               
                                        <form role="form" action="<?php echo base_url('admin/profile/update_data');?>" enctype= "multipart/form-data" method="POST">
                                    <?php                 
                                            foreach ($key as $value)
                                            {
                                               $value['user_id'];
                                               $image=  $value['image'];
                                    ?>
                                            <div class="form-group">
                                                <input type="hidden" name="user_id" value="<?php echo $value['user_id']; ?>">
                                                <label class="control-label">Name</label>
                                                <input type="text" name="name" class="form-control" value="<?php echo $value['user_name']; ?>" /> 
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Mobile Number</label>
                                                <input type="text" name="pno" value="<?php echo $value['user_phone']; ?>" class="form-control" /> </div>
                                                <div class="form-group">
                                                <label class="control-label">Address</label>
                                                <input type="text" name="address" value="<?php echo $value['user_address']; ?>" class="form-control" /> 
                                            </div>
                                             <div class="form-group" >
                                                <input type="hidden" name="old_image" value="<?php echo  $value['image'];  ?>">
                                            </div>
                                            
                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                 <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="<?php echo base_url()?>assets1/images/<?php  echo $image;  ?>" > 
                                                </div>
                                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                                                </div>
                                                <div>
                                                    <span class="btn default btn-file">
                                                        <span class="fileinput-new"> Select image </span>
                                                        <span class="fileinput-exists" name="photo"> Change </span>
                                                        <input type="file" name="photo"> 
                                                    </span>
                                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                </div>
                                            </div>
                                            <?php }?>
                                            <div class="margiv-top-10">
                                                <button type="submit" class="btn btn-circle green">Save </button>
                                                <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                            </div>
                                             
                                        </form>
                                    </div>
                                   
                                    <!-- END PERSONAL INFO TAB -->
                                    <!-- CHANGE AVATAR TAB -->
                                  
                                    <!-- END CHANGE AVATAR TAB -->
                                    <!-- CHANGE PASSWORD TAB -->
                                  
                                    <!-- END CHANGE PASSWORD TAB -->
                                    <!-- PRIVACY SETTINGS TAB -->
                             
                                    <div class="tab-pane" id="tab_1_4">
                                        <form  action="#">
                                           
                                            <table class="table table-light table-hover">
                                                <tr>
                                                    <td> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus.. </td>
                                                    <td>
                                                        <div class="mt-radio-inline">
                                                            <label class="mt-radio">
                                                                <input type="radio" name="optionsRadios1" value="option1" /> Yes
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-radio">
                                                                <input type="radio" name="optionsRadios1" value="option2" checked/> No
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                    <td>
                                                        <div class="mt-radio-inline">
                                                            <label class="mt-radio">
                                                                <input type="radio" name="optionsRadios11" value="option1" /> Yes
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-radio">
                                                                <input type="radio" name="optionsRadios11" value="option2" checked/> No
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                    <td>
                                                        <div class="mt-radio-inline">
                                                            <label class="mt-radio">
                                                                <input type="radio" name="optionsRadios21" value="option1" /> Yes
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-radio">
                                                                <input type="radio" name="optionsRadios21" value="option2" checked/> No
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td> Enim eiusmod high life accusamus terry richardson ad squid wolf moon </td>
                                                    <td>
                                                        <div class="mt-radio-inline">
                                                            <label class="mt-radio">
                                                                <input type="radio" name="optionsRadios31" value="option1" /> Yes
                                                                <span></span>
                                                            </label>
                                                            <label class="mt-radio">
                                                                <input type="radio" name="optionsRadios31" value="option2" checked/> No
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--end profile-settings-->
                                            <div class="margin-top-10">
                                                <a href="javascript:;" class="btn red"> Save Changes </a>
                                                <a href="javascript:;" class="btn default"> Cancel </a>
                                            </div>
                                       
                                        </form>

                                    </div>
                                    <!-- END PRIVACY SETTINGS TAB -->
                                     <div class="tab-pane" id="tab_1_3">
                                        <form method="POST" action="<?php echo base_url('admin/profile/change_password');?>">
                                             <?php 
                                             foreach ($key as $value)
                                            {
                                               $id = $value['user_id'];
                                            ?>
                                            <div class="form-group">
                                                <label class="control-label">Current Password</label>
                                                <input type="hidden" name="user_id" placeholder="Enter your Current password" class="form-control" value="<?php echo $id;?>" /> 
                                                <input type="hidden" name="tab" value="#tab_1_3">
                                                <input type="password" name="cpass" placeholder="Enter your Current password" class="form-control"  />
                                                  </div>
                                            <div class="form-group">
                                                <label class="control-label">New Password</label>
                                                <input type="password" name="npass" placeholder="Enter new password" class="form-control" /> </div>
                                            <div class="form-group">
                                                <label class="control-label">Re-type New Password</label>
                                                <input type="password" name="rpass" placeholder="Re Enter your new password" class="form-control" /> </div>
                                            <div class="margin-top-10">
                                                <input type="submit" class="btn green" value="Change Password "/>
                                                <!-- <a href="javascript:;" class="btn default"> Cancel </a> -->
                                            </div>
                                             <?php } ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
    </div>

    </div>
    
    <!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->





