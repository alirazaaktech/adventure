<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase"> User Inseration

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/user/insert');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                        <div>
                          
                                <h3>Provide details</h3>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Enter Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="name" placeholder="Provide your name" />
                                        <span id="color"><?php echo form_error('name')?></span>
                                    </div>
                                </div>
                                   <div class="form-group">
                                    <label class="control-label col-md-3">Phone Number
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="Phone" class="form-control" name="pno" placeholder="Provide your Phone Number" />
                                        <span id="color"><?php echo form_error('pno')?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Enter Email
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="mail" class="form-control" name="email" placeholder="Provide Email" />
                                        <span id="color"><?php echo form_error('email')?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Enter Password
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="password" class="form-control" name="password" placeholder="Provide Password" />
                                        <span id="color"><?php echo form_error('password')?></span>
                                    </div>
                                </div>
                              
                                <div class="form-group">
                                    <label class="control-label col-md-3">Address
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="address" placeholder="Provide your Address" />
                                        <span id="color"><?php echo form_error('address')?></span>
                                    </div>
                                </div>
                              
                             
                               <div class="form-group">
                                
                                <div class="col-md-4">
                                    <input type="hidden" class="form-control" value="<?php echo date('y-m-d');?>" name="date" />
                                </div>
                            </div>
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists" name="photo"> Change </span>
                                        <input type="file" name="photo"> </span>
                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                        </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->





