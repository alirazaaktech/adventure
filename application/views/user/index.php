<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_user')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_user'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>Users Table 
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="16%">Name</th>
                                                <th width="8%">Phone</th>
                                                <th width="12%">Address</th>
                                                <th width="12%">Email</th>
                                                <th width="8%">Role</th>
                                                <th width="12%">Date</th>
                                                <th width="10%">Image</th>
                                                <th width="10%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($user) && !empty($user))
                                                {
                                                    foreach($user as $value)
                                                    {   
                                            ?>
                                                        <tr>
                                                            <td><?php echo $value['user_name']; ?></td>
                                                            <td><?php echo $value['user_phone']; ?></td>
                                                            <td><?php echo $value['user_address']; ?></td>
                                                            <td><?php echo $value['user_email']; ?></td>
                                                            <td><?php echo $value['role']; ?></td>
                                                            <td><?php echo $value['date']; ?></td>
                                                            <td><img src="<?php echo base_url() ?>/assets1/images/<?php echo $value['image']; ?>" style="width:85%;height:20%" ></td>
                                                            <td>
                                                                <a href="<?php echo base_url('admin/user/update/'.$value['user_id']); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                              <!--   <a href="<?php //echo base_url('admin/user/delete/'.$value['user_id']); ?>" class="btn blue" type="button" >Delete</a> &nbsp; -->
                                                            </td>

                                                        </tr>

                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
