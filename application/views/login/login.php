<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/asets/css/mystyle.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Login</title>
  </head>
  <body>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <h1>Venturexglobal</h1>
      </div>
      <div class="login-box">

        <form class="login-form" action="<?php echo base_url()?>admin/login/login_process" method="POST">
           <?php
              if($this->session->flashdata('message'))
              {
            ?>
              <div class="alert alert-danger" style="font-size: 16px">
              <strong>Alert:</strong><?php echo $this->session->flashdata('message')?>
              </div>
            <?php 
              }
            ?>
            <?php
              if($this->session->flashdata('success'))
              {
            ?>
              <div class="alert alert-success" style="font-size: 16px">
              <strong>Alert:</strong><?php echo $this->session->flashdata('success')?>
              </div>
            <?php 
              }
            ?>
            <?php
            if($this->session->flashdata('error_message'))
              {
            ?>
              <div class="alert alert-danger" style="font-size: 15px">
              <strong>Alert:</strong><?php echo $this->session->flashdata('error_message')?>
              </div>
            <?php 
              }
            ?>
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>LOGIN IN</h3>
           
          <div class="form-group">
            <label class="control-label">USERNAME</label>
            <input class="form-control" type="text" name="email" placeholder="Email" autofocus>
          </div>
          <div class="form-group">
            <label class="control-label">PASSWORD</label>
            <input class="form-control" type="password" name="password" placeholder="Password">
          </div>
          <div class="form-group">
            <div class="utility">
              <div class="animated-checkbox">
               <!--  <label>
              
                  <input type="radio" name="role" value="student" id="radio_value"><span class="label-text">&nbsp;Student&nbsp;&nbsp;&nbsp;&nbsp;</span>
                  <input type="radio" name="role" value="teacher" id="radio_value"><span class="label-text">&nbsp;Teacher</span>
                </label> -->
              </div>
              <!-- <p class="semibold-text mb-2"><a href="#" data-toggle="flip">Registration</a></p> -->
            </div>
          </div>
          <div class="form-group btn-container">
              <button class="btn btn-primary btn-block"  value="login-submit" name="login-submit"><i class="fa fa-sign-in fa-lg fa-fw"></i>LOGIN IN</button>
          </div>
        </form>
    <!--  

        <form class="forget-form"  action="<?php //echo base_url()?>user/add_process" method="post">
          <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>Registration</h3>

           <div class="form-group">
            <label class="control-label">USERNAME</label>
            <input class="form-control" type="text" name="name" placeholder="Enter Name " autofocus>
          </div>

          <div class="form-group">
            <label class="control-label">USER Email</label>
            <input class="form-control" type="text" name="email" placeholder="Email" autofocus>
          </div>
          <div class="form-group">
            <label class="control-label">PASSWORD</label>
            <input class="form-control" type="password" name="password" placeholder="Password">
          </div>
          <div class="form-group">
            <label class="control-label"> Confrim PASSWORD</label>
            <input class="form-control" type="password" name="confirm_password" placeholder="Password">
          </div>
            <div class="form-group">
              <label class="control-label"> Role </label>
              <select class="form-control" name="role">
                <option value="teacher">Teacher</option>
                <option value="student">Student</option>
              </select>
          </div>
          <div class="form-group btn-container">
            <button class="btn btn-primary btn-block" value="submit" name="submit_register"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN Up</button>
          </div>
          <div class="form-group mt-3">
            <p class="semibold-text mb-0"><a href="#" data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
            <button class="btn btn-common log-btn" onclick="return validate()" value="submit" name="submit_regist" >Register</button> 
          </div>
        </form> -->


      </div>
    </section>
    <!-- Essential javascripts for application to work-->
  <!--   <script src="<?php //echo base_url()?>js/jquery-3.2.1.min.js"></script>
    <script src="<?php //echo base_url()?>js/popper.min.js"></script>
    <script src="<?php //echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php //echo base_url()?>js/main.js"></script> -->
    <!-- The javascript plugin to display page loading on top-->
 <!--    <script src="<?php //echo base_url()?>js/plugins/pace.min.js"></script> -->
    <script type="text/javascript">
      // Login Page Flipbox control
      $('.login-content [data-toggle="flip"]').click(function() {
        $('.login-box').toggleClass('flipped');
        return false;
      });
    </script>
  </body>
</html>