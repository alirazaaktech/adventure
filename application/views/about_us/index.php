<div class="page-header page-header--style-1 page-header--light page-header--large" style="background-image: url('<?php echo base_url() ?>assets1/images/<?php echo $head_section[1]['head_section_image']; ?>'); height: 598px; ">
	<div class="container" style="margin-top: 10px;">
		<div class="page-header__breadcrumbs breadcrumbs breadcrumbs--page-header-style-1">
			<span property="itemListElement" typeof="ListItem"><a class="home" href="index.html" property="item" title="Go to advice." typeof="WebPage"><i class="mdi mdi-home"></i></a></span>
			<meta content="1" property="position"><span property="itemListElement" typeof="ListItem"></span> <span class="breadcrumbs__separator"><span class="mdi mdi-chevron-right"></span></span> <span property="itemListElement" typeof="ListItem"><span property="name"><?php echo $head_section[1]['head_section_h'] ?></span></span>
			<meta content="2" property="position">
		</div>
		<div class="page-header__heading">
			<h1 class="page-header__title">
				<?php $data = explode(" ", $head_section[1]['head_section_p']);
						// print_r($data);exit();
				foreach ($data as $key=>$value) {
					if($key <=3){
						echo $value." ";
						if ($key == 3) {
							echo "<br>";
						}
					}
					else
					{
						echo $value."";
					}
				} ?>
			</h1>
		</div>
	</div>
	<div class="page-header__overlay"></div>
</div>
<br>
<br>
<br>
<br>
<div class="content content--main">
	<div class="vc_row wpb_row vc_row-fluid vc_custom_1495001524605">
		<div class="a-vc-container container">

			<div class="row">
				<?php if (isset($productss)&& !empty($productss)) {
					foreach($productss as $view)
					{

						?>
						<div class="wpb_column vc_column_container vc_col-sm-6">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="a-text-block vc_custom_1495956521899 a-text-block--simple" style="font-size:20px;font-weight:500;line-height:1.8">
										<p><?php echo $view['heading'] ?></p>
									</div>
									<div class="a-text-block a-text-block--simple" style="font-size:18px;line-height:1.89">
										<p><?php echo $view['paragraph'] ?></p>
									</div>
								</div>
							</div>
						</div>
						<div class="wpb_column vc_column_container vc_col-sm-6">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="advice-space a-js-space" data-lg="" data-md="" data-sm="30" data-xl="" data-xs="30"></div>
									<div class="a-video vc_custom_1495434797931 a-video--simple-card a-video--simple-card-style-1 a-video--align-right">
										<div class="a-video__inner">
											     <a class="a-video__action a-js-video-play" href="#">
                                                   
                                                    <embed width="470" height="264" 
                                                    src="<?php echo $view['image']; ?>"  allowfullscreen frameborder="0" allow="autoplay; encrypted-media">
                                                </a>
											<iframe allowfullscreen frameborder="0" height="264" width="470"></iframe> <a class="a-video__action a-js-video-play" data-video-src="<?php echo $view['image']; ?>" href="#" style="background-image:url('images/Layer-63.jpg')"><span class="a-video__action-overlay" style="background-color:rgba(38,37,55,0.1)"></span> <span class="a-video__action-icon mdi mdi-play-circle"></span></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php } } ?>
				</div>

			</div>
		</div>
		<div class="vc_row wpb_row vc_row-fluid vc_custom_1495001549347">
			<div class="a-vc-container container">
				<div class="row">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey vc_custom_1495001159976 vc_custom_1495001159976">
									<span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span> <span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="vc_row wpb_row vc_row-fluid vc_custom_1495001583925">
			<div class="a-vc-container container">
				<div class="row">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="a-heading a-heading--align-center">
									<h2 class="a-heading__title">Our values</h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<br>
		<div class="vc_row wpb_row vc_row-fluid vc_custom_1495001698713">
			<div class="a-vc-container container">
				<div class="row">
					<?php 
					if (isset($valuess)&& !empty($valuess)) {
					
					foreach ($valuess as $value_data) {
						# code...
					 ?>
					<div class="wpb_column vc_column_container vc_col-sm-4">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="a-icon-box a-icon-box--style-1">
									<div class="a-icon-box__wrap">
										<div class="a-icon-box__icon-container">
											<div><img style="min-height: 70px; max-height: 70px;" src="<?php echo base_url() ?>assets1/images/<?php echo $value_data['image']; ?>" ></div>
										</div>
										<div class="a-icon-box__body">
											<div class="a-icon-box__title">
												<?php echo $value_data['heading']; ?>
											</div>
											<div class="a-icon-box__text">
											<?php echo $value_data['paragraph']; ?>
											</div>
										</div>
									</div>
								</div>
								<div class="advice-space a-js-space" data-lg="30" data-md="30" data-sm="30" data-xl="30" data-xs="30" style="height: 30px;"></div>
							</div>
						</div>
					</div>
				<?php } } ?>
				
			
				</div>
			</div>
		</div>

		<div class="vc_row wpb_row vc_row-fluid vc_custom_1499783886980 vc_row-has-fill" data-vc-full-width="true" data-vc-full-width-init="true" style="position: relative; left: 15px; box-sizing: border-box; width: 100%; padding-left: 0px; padding-right: 0px; background-color: #eff1f3;">
				<div class="a-vc-container container">
					<div class="row">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner vc_custom_1495021420654">
								<div class="wpb_wrapper">
									<div class="vc_custom_1495093217973 a-timeline a-timeline--style-1">
										<br>
										<br>
										<h2 class="a-timeline__title">Our history</h2>
										<div class="a-timeline__nav a-nav a-nav--timeline">
											<a class="a-nav__arrow a-nav__arrow--prev a-js-timeline-prev-nav" href="#"><span class="ai ai-arrow-left"></span></a> <a class="a-nav__arrow a-nav__arrow--next a-js-timeline-next-nav" href="#"><span class="ai ai-arrow-right"></span></a>
											<div class="a-nav__list-container">
												<ul class="a-nav__list">

													<li class="a-nav__line a-nav__line--vertical" style="height: 3px;"></li>

													<li class="a-nav__item a-js-timeline-event-switch a-nav__item--active" data-event-id="a-timeline-event-435">
														<div class="a-nav__text a-nav__text--time">
															<?php echo (isset($history[0]['year'])?$history[0]['year']:''); ?>
															
														</div>
														<div class="a-nav__bullet aliClass"></div>
														<div class="a-nav__text a-nav__text--event">
															<?php echo (isset($history[0]['name'])?$history[0]['name']:''); ?>
														</div>
													</li>

													

													<li class="a-nav__item a-js-timeline-event-switch " data-event-id="a-timeline-event-542">
														<div class="a-nav__text a-nav__text--time">
															<?php echo (isset($history[1]['year'])?$history[1]['year']:''); ?>
														</div>
														<div class="a-nav__bullet aliClass"></div>
														<div class="a-nav__text a-nav__text--event">
															<?php echo (isset($history[1]['name'])?$history[1]['name']:''); ?>
														</div>
													</li>
													<li class="a-nav__item a-js-timeline-event-switch " data-event-id="a-timeline-event-895">
														<div class="a-nav__text a-nav__text--time">
															<?php echo (isset($history[2]['year'])?$history[2]['year']:''); ?>
														</div>
														<div class="a-nav__bullet aliClass"></div>
														<div class="a-nav__text a-nav__text--event">
															<?php echo (isset($history[2]['name'])?$history[2]['name']:''); ?>
														</div>
													</li>
													<li class="a-nav__item a-js-timeline-event-switch" data-event-id="a-timeline-event-889">
														<div class="a-nav__text a-nav__text--time">
															<?php echo (isset($history[3]['year'])?$history[3]['year']:''); ?>
														</div>
														<div class="a-nav__bullet aliClass"></div>
														<div class="a-nav__text a-nav__text--event">
															<?php echo (isset($history[3]['name'])?$history[3]['name']:''); ?>
														</div>
													</li>
													<li class="a-nav__item a-js-timeline-event-switch" data-event-id="a-timeline-event-212">
														<div class="a-nav__text a-nav__text--time">
															<?php echo (isset($history[4]['year'])?$history[4]['year']:''); ?>
														</div>
														<div class="a-nav__bullet aliClass"></div>
														<div class="a-nav__text a-nav__text--event">
															<?php echo (isset($history[4]['name'])?$history[4]['name']:''); ?>
														</div>
													</li>
													<!-- <li class="a-nav__item a-js-timeline-event-switch" data-event-id="a-timeline-event-26">
														<div class="a-nav__text a-nav__text--time">
																<?php echo (isset($history[5]['year'])?$history[5]['year']:''); ?>
														</div>
														<div class="a-nav__bullet aliClass"></div>
														<div class="a-nav__text a-nav__text--event">
															<?php echo (isset($history[5]['name'])?$history[5]['name']:''); ?>
														</div>
													</li> -->
												</ul>
											</div>
										</div>
										<div class="a-timeline__events a-timeline-events">
											<div class="a-timeline-events__container  ">
												<div class="a-timeline-events__item a-timeline-event a-timeline-event--card a-timeline-events__item--active" id="a-timeline-event-435">
													<div class="a-timeline-event__image-container a-object-fit-container">
														<img alt="" class="a-timeline-event__image a-js-has-object-fit" height="280" sizes="(max-width: 420px) 100vw, 420px" src="<?php echo base_url()?>assets1/images/<?php echo  (isset($history[0]['history_image'])?$history[0]['history_image']:''); ?>"  width="420">
													</div>
													<div class="a-timeline-event__content">
														<h3 class="a-timeline-event__title"><?php echo  (isset($history[0]['year'])?$history[0]['year']:''); ?> &nbsp; &nbsp; <?php echo (isset($history[0]['name'])?$history[0]['name']:''); ?></h3>
														<div class="a-timeline-event__description">
															<?php echo (isset($history[0]['paragraph'])?$history[0]['paragraph']:''); ?>
														</div>
													</div>
												</div>
												<div class="a-timeline-events__item a-timeline-event a-timeline-event--card " id="a-timeline-event-542">
													<div class="a-timeline-event__image-container a-object-fit-container"><img alt="" class="a-timeline-event__image a-js-has-object-fit" height="280" sizes="(max-width: 557px) 100vw, 557px" src="<?php echo base_url()?>assets1/images/<?php echo  (isset($history[1]['history_image'])?$history[1]['history_image']:''); ?>" width="557"></div>
													<div class="a-timeline-event__content">
														<h3 class="a-timeline-event__title"><?php echo (isset($history[1]['year'])?$history[1]['year']:''); ?>&nbsp; &nbsp;<?php echo (isset($history[1]['name'])?$history[1]['name']:''); ?></h3>
														<div class="a-timeline-event__description">
															<?php echo (isset($history[1]['paragraph'])?$history[1]['paragraph']:''); ?>
														</div>
													</div>
												</div>
												<div class="a-timeline-events__item a-timeline-event a-timeline-event--card" id="a-timeline-event-895">
													<div class="a-timeline-event__image-container a-object-fit-container"><img alt="" class="a-timeline-event__image a-js-has-object-fit" height="280" sizes="(max-width: 420px) 100vw, 420px" src="<?php echo base_url()?>assets1/images/<?php echo  (isset($history[2]['history_image'])?$history[2]['history_image']:''); ?>" width="420"></div>
													<div class="a-timeline-event__content">
														<h3 class="a-timeline-event__title"><?php echo (isset($history[2]['year'])?$history[2]['year']:''); ?>&nbsp; &nbsp;<?php echo (isset($history[2]['name'])?$history[2]['name']:''); ?></h3>
														<div class="a-timeline-event__description">
														<?php echo (isset($history[2]['paragraph'])?$history[2]['paragraph']:''); ?>
														</div>
													</div>
												</div>
												<div class="a-timeline-events__item a-timeline-event a-timeline-event--card" id="a-timeline-event-889">
													<div class="a-timeline-event__image-container a-object-fit-container"><img alt="" class="a-timeline-event__image a-js-has-object-fit" height="280" sizes="(max-width: 421px) 100vw, 421px" src="<?php echo base_url()?>assets1/images/<?php echo  (isset($history[3]['history_image'])?$history[3]['history_image']:''); ?>" width="421"></div>
													<div class="a-timeline-event__content">
														<h3 class="a-timeline-event__title"><?php echo (isset($history[3]['year'])?$history[3]['year']:''); ?>&nbsp; &nbsp;<?php echo (isset($history[3]['name'])?$history[3]['name']:''); ?></h3>
														<div class="a-timeline-event__description">
														<?php echo (isset($history[3]['paragraph'])?$history[3]['paragraph']:''); ?>
														</div>
													</div>
												</div>

												<div class="a-timeline-events__item a-timeline-event a-timeline-event--card " id="a-timeline-event-212">
													<div class="a-timeline-event__image-container a-object-fit-container"><img alt="" class="a-timeline-event__image a-js-has-object-fit" height="280" sizes="(max-width: 422px) 100vw, 422px" src="<?php echo base_url()?>assets1/images/<?php echo  (isset($history[4]['history_image'])?$history[4]['history_image']:''); ?>" width="422"></div>
													<div class="a-timeline-event__content">
														<h3 class="a-timeline-event__title"><?php echo (isset($history[4]['year'])?$history[4]['year']:''); ?>&nbsp; &nbsp;<?php echo (isset($history[4]['name'])?$history[4]['name']:''); ?></h3>
														<div class="a-timeline-event__description">
														<?php echo (isset($history[4]['paragraph'])?$history[4]['paragraph']:''); ?>
														</div>
													</div>
												</div>
												<!-- <div class="a-timeline-events__item a-timeline-event a-timeline-event--card" id="a-timeline-event-26">
													<div class="a-timeline-event__image-container a-object-fit-container"><img alt="" class="a-timeline-event__image a-js-has-object-fit" height="280" src="images/Layer-11.jpg" srcset="images/Layer-11.jpg" width="418"></div>
													<div class="a-timeline-event__content">
														<h3 class="a-timeline-event__title"><?php echo (isset($history[5]['year'])?$history[5]['year']:''); ?>&nbsp; &nbsp;<?php echo (isset($history[5]['name'])?$history[5]['name']:''); ?></h3>
														<div class="a-timeline-event__description">
															<?php echo (isset($history[5]['paragraph'])?$history[5]['paragraph']:''); ?>
														</div>
													</div>
												</div> -->
												<div class="a-timeline-events__stacked">
													<div class="a-timeline-events__stacked-item a-timeline-events__stacked-item--level-1"></div>
													<div class="a-timeline-events__stacked-item a-timeline-events__stacked-item--level-2"></div>
													<div class="a-timeline-events__stacked-item a-timeline-events__stacked-item--level-3"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<script type="text/javascript">

				$(".aliClass").click(function(){
					$(".a-timeline-events__item--active").removeClass('a-timeline-events__item--active');
					$(".a-nav__item--active").removeClass('a-nav__item--active');
					$('.a-nav__line').remove();
					var par = $(this).parent().attr("data-event-id");
						$(this).parent().prepend('<li class="a-nav__line a-nav__line--vertical" style="height: 3px;"></li>');

						$(this).parent().addClass("a-nav__item--active");
						
						var aho= $('#'+par).attr('class');
						$('#'+par).addClass("a-timeline-events__item--active");
						
						console.log(par);
						console.log(aho);
					
				});
			</script>

			<div class="vc_row-full-width vc_clearfix"></div>
			<div class="vc_row wpb_row vc_row-fluid vc_custom_1502483983021">
				<div class="a-vc-container container">
					<div class="row">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner">
                            <div class="wpb_wrapper">
                                <div class="a-heading vc_custom_1502964801783 a-heading--align-center"  style="padding-top: 30px; padding-bottom:60px; ">
                                    <h2 class="a-heading__title">Our people</h2>
                                </div>
                                <div class="a-staff a-staff--grid a-staff--grid-cols-4" data-params="{&quot;items_style&quot;:&quot;card-outline&quot;,&quot;posts_per_page&quot;:&quot;4&quot;}">
                                    <div class="a-staff__grid">
                                        <?php 

                                        if(isset($section_4) && !empty($section_4))
                                        {
                                            foreach ($section_4 as $key)
                                            {
                                                $home_section_4_img = $key['home_section_4_img']; 
                                                $home_section_4_name = $key['home_section_4_name']; 
                                                $home_section_4_role = $key['home_section_4_role'];  
                                                $home_section_4_location_title = $key['home_section_4_location_title'];
                                                $home_section_4_location_url = $key['home_section_4_location_url'];
                                                $home_section_4_post_cat = $key['home_section_4_post_cat'];


                                                $home_section_4_p = $key['home_section_4_p']; 
                                                $home_section_4_link = $key['home_section_4_link'];  
                                                ?>



                                                <div class="a-staff__item a-employee a-employee--card-outline">
                                                    <div class="a-employee__wrap">
                                                        <div class="a-employee__image-container a-object-fit-container">
                                                            <a class="a-employee__link" href="#" title="Eric Clapton"><img alt="" class="a-employee__image a-js-has-object-fit wp-post-image" height="380" sizes="(max-width: 370px) 100vw, 370px" src="<?php echo base_url() ?>assets1/images/<?php echo $home_section_4_img ?>" width="370"></a>
                                                            
                                                        </div>
                                                        <div class="a-employee__details">
                                                            <div class="a-employee__header">
                                                                <h4 class="a-employee__title"><a class="a-employee__title-link" href="#" title="Eric Clapton"><?php echo $home_section_4_name; ?></a></h4>
                                                                <div class="a-employee__position">
                                                                   <?php echo $home_section_4_role; ?>
                                                               </div>
                                                               <hr style="border-color: #fff; width: 50px;margin: 0px auto;border-width: 2px;position: relative;top: 13px;">
                                                           </div><a class="a-employee__office" href="<?php echo $home_section_4_location_url; ?>" target="_blank"><span class="a-employee__office-icon mdi mdi-map-marker"></span><?php echo $home_section_4_location_title; ?></a>
                                                           <p class="a-employee__summary"><?php echo  $home_section_4_p ?></p>
                                                           <div class="a-employee__tags">
                                                            <?php 
                                                                //     $a_title=implode(",", $title);
                                                                //     $a_link=implode(",", $link);
                                                                // for($i=0; $i<count($a_title); $i++)
                                                                // {
                                                            ?>
                                                            <a href="<?php //echo "$a_link[$i]"?>" rel="tag"><?php //echo $a_title[$i]; ?></a>
                                                            <?php //} ?>
                                                            <a href="#" rel="tag"><?php echo $home_section_4_post_cat ?></a> <!-- <a href="#" rel="tag">Big Data</a>, <a href="#" rel="tag">Energy</a>, <a href="#" rel="tag">Finance</a>, <a href="#" rel="tag">Oil</a> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php   
                                        }
                                    } 
                                    ?>

                                </div>
                                <input name="advice_load_more_post_type" type="hidden" value="2f246ec9fd"> <input name="advice_filter_post_type" type="hidden" value="ab30f1242a">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
				</div>
			</div>
	
			
		
		<div class="vc_row-full-width vc_clearfix"></div>
		<div class="vc_row wpb_row vc_row-fluid">
			<div class="a-vc-container container">
				<div class="row">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey vc_custom_1495043587757 vc_custom_1495043587757">
									<span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span> <span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="vc_row wpb_row vc_row-fluid vc_custom_1499914997419 vc_row-has-fill" data-vc-full-width="true" data-vc-full-width-init="true" style="position: relative; left: 15px; box-sizing: border-box; width: 100%; padding-left: 0px; padding-right: 0px;">
			<div class="a-vc-container container">
				<div class="row">
					<div class="wpb_column vc_column_container vc_col-sm-12">
						<div class="vc_column-inner">
							<div class="wpb_wrapper">
								<div class="a-heading vc_custom_1495045020175 a-heading--align-center">
									<h2 class="a-heading__title">Why choose us?</h2>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br><br>
		
			<div class="vc_row wpb_row vc_row-fluid vc_custom_1508061295760">
	    		<div class="a-vc-container container">
	        		<div class="row">
									<?php
								         if(isset($section_1) && !empty($section_1))
								         {
								           foreach ($section_1 as $key ) 
								           {
								               $sec_in_p = $key['sec_in_p']; 
								               $sec_in_h = $key['sec_in_h']; 
								               $sec_in_image = $key['sec_in_image']; 
						            ?>

							            <div class="wpb_column vc_column_container vc_col-sm-4">
							                <div class="vc_column-inner vc_custom_1508061668302">
							                    <div class="wpb_wrapper">
							                        <div class="a-icon-box a-icon-box--style-1">
							                            <div class="a-icon-box__wrap">
							                                <div class="a-icon-box__icon-container">
							                                   <img style="max-width: 150px; max-height: 70px " src="<?php echo base_url() ?>assets1/images/<?php echo $sec_in_image; ?>" >
							                               	</div>
							                               	<div class="a-icon-box__body">
							                                	<div class="a-icon-box__title">
							                                    	<?php echo $sec_in_h; ?>
							                                	</div>
							                                	<div class="a-icon-box__text">
							                                  		<?php echo $sec_in_p; ?>
							                              		</div>
							                          		</div>
							                      		</div>
							                  		</div>
							              		</div>
							          		</div>
							      		</div>
									<?php 
										  	}
										} 
									?>
					</div>
										
				</div>
			</div>
		</div>
	
	
			<div class="vc_row-full-width vc_clearfix"></div>
			<br><br>


 <!--  -->
<div class="testimonial-container a-site-bg-color-3">
    <div class="dk-container ">
        <div class="cd-testimonials-wrapper cd-container">
           <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
     <?php  if(isset($videosection) && !empty($videosection))
                            {      ?> 
  <div class="carousel-inner">
     <?php
                             $count=0;
                              $values = array_chunk($videosection, 1);
                            
                               foreach($values as $videosview)
                                { 
                                    // debug($values,true);
                                    ?>

<div class="item a-testimonial__container <?php if($count == 0) {echo 'active'; }else{echo '';} ?>">
      <img src="http://localhost/venturexglobal/assets1/images/Feedbacks.png" alt="New York">
      <div class="carousel-caption a-testimonial__text a-font-family-heading" style="padding-bottom: 6vw;">
      	<br>
      	<br>
        <h3> <?php echo $videosview[0]['home_slider_p']; ?></h3>
            <div class="a-testimonial__author-info author-info author-info--testimonial author-info--testimonial">
                                            <div class="author-info__avatar">
                                                <img src="<?php echo base_url() ?>assets1/images\<?php echo $videosview[0]['home_slider_img']; ?>"  400w" width="70">
                                            </div>
                                                   <p> <div class="author-info__description">
                                                <div class="author-info__title">
                                                    <?php echo $videosview[0]['name']; ?>
                                                </div>
                                                <div class="author-info__position">
                                                    <?php echo $videosview[0]['role']; ?> - <?php echo $videosview[0]['country']; ?>
                                                </div>
                                            </div></p>
                                        </div>

      </div>
    </div>
    <?php  $count ++; } ?>

  </div>
<?php } ?>
  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
    </div>
</div>
</div>

<!--  -->
			<div class="vc_row-full-width vc_clearfix"></div>
<br>
<br>
<!--  -->
<style type="text/css">
    /* Slider */
.slick-slide {
    margin: 0px 20px;
}

.slick-slide img {
    width: 100%;
}

.slick-slider
{
    position: relative;

    display: block;
    box-sizing: border-box;

    -webkit-user-select: none;
       -moz-user-select: none;
        -ms-user-select: none;
            user-select: none;

    -webkit-touch-callout: none;
    -khtml-user-select: none;
    -ms-touch-action: pan-y;
        touch-action: pan-y;
    -webkit-tap-highlight-color: transparent;
}

.slick-list
{
    position: relative;

    display: block;
    overflow: hidden;

    margin: 0;
    padding: 0;
}
.slick-list:focus
{
    outline: none;
}
.slick-list.dragging
{
    cursor: pointer;
    cursor: hand;
}

.slick-slider .slick-track,
.slick-slider .slick-list
{
    -webkit-transform: translate3d(0, 0, 0);
       -moz-transform: translate3d(0, 0, 0);
        -ms-transform: translate3d(0, 0, 0);
         -o-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
}

.slick-track
{
    position: relative;
    top: 0;
    left: 0;

    display: block;
}
.slick-track:before,
.slick-track:after
{
    display: table;

    content: '';
}
.slick-track:after
{
    clear: both;
}
.slick-loading .slick-track
{
    visibility: hidden;
}

.slick-slide
{
    display: none;
    float: left;

    height: 100%;
    min-height: 1px;
}
[dir='rtl'] .slick-slide
{
    float: right;
}
.slick-slide img
{
    display: block;
}
.slick-slide.slick-loading img
{
    display: none;
}
.slick-slide.dragging img
{
    pointer-events: none;
}
.slick-initialized .slick-slide
{
    display: block;
}
.slick-loading .slick-slide
{
    visibility: hidden;
}
.slick-vertical .slick-slide
{
    display: block;

    height: auto;

    border: 1px solid transparent;
}
.slick-arrow.slick-hidden {
    display: none;
}
</style>
<!--  -->
<!--  -->
<div class="wpb_wrapper">
	<div class="a-heading vc_custom_1495048310348 a-heading--align-center">
		<h2 class="a-heading__title">Our license</h2>
	</div>
	<div class="a-text-block vc_custom_1495085435650 a-text-block--simple" style="color:#7e7f80"><p style="text-align: center;"><?php if(isset($licenese_head)){ echo $licenese_head[0]['heading']; }else{ //echo 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br> tempor incididunt ut labore et dolore magna aliqua.';
} ?></p>
	</div>
<br>
<br>
<div class="customer-logos">
	<?php
	if(isset($licenese_img)){
		foreach ($licenese_img as $key ) {
			?>
			<div class="slide"><img  style="min-height: 270px; max-height: 270px;width:100%" src="<?php echo base_url()?>assets1/images/<?php echo $key['logo']; ?>"></div>
	<?php	}
	 }


	  ?>

<!--   <div class="slide"><img src="https://www.solodev.com/assets/carousel/image1.png"></div>
  <div class="slide"><img src="https://www.solodev.com/assets/carousel/image2.png"></div>
  <div class="slide"><img src="https://www.solodev.com/assets/carousel/image3.png"></div>
  <div class="slide"><img src="https://www.solodev.com/assets/carousel/image4.png"></div>
  <div class="slide"><img src="https://www.solodev.com/assets/carousel/image5.png"></div>
  <div class="slide"><img src="https://www.solodev.com/assets/carousel/image6.png"></div>
  <div class="slide"><img src="https://www.solodev.com/assets/carousel/image7.png"></div>
  <div class="slide"><img src="https://www.solodev.com/assets/carousel/image8.png"></div> -->
</div>
</div>
<!--  -->
			<div class="vc_row wpb_row vc_row-fluid vc_custom_1495085544221">
				<div class="a-vc-container container">
					<div class="row">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey vc_custom_1495085461044 vc_custom_1495085461044">
										<span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span> <span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="vc_row wpb_row vc_row-fluid">

				<div class="a-vc-container container">
					<div class="row">
						<div class="wpb_column vc_column_container vc_col-sm-12">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="a-heading vc_custom_1495091660509 a-heading--align-center">
										<h2 class="a-heading__title">Partners</h2>
									</div>
									<div class="a-images-gallery a-images-gallery--style-1 a-images-gallery--cols-4">
										<div class="a-images-gallery__list">
											<?php if (isset($logo) && !empty($logo)) {
												foreach ($logo as $logos) {
													?>
											
											<div class="a-images-gallery__item">
												<div class="a-images-gallery__image a-gallery-image a-gallery-image--style-1">
													<a class="a-gallery-image__link" href="#" target=" _blank"><img alt="" class="a-gallery-image__pic" height="35" sizes="(max-width: 178px) 100vw, 178px" 
														src="<?php echo base_url()?>assets1/images/<?php echo $logos['logo']; ?>"  width="178"></a>
												</div>
											</div>
											<?php	}
											} ?>
									
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<script>
				    var slideIndex = 1;
     showDivs(slideIndex);

     function plusDivs(n) {
     	alert()
       // showDivs(slideIndex += n);
       $(".s_active").fadeOut('slow');
       $(".s_active").next().addClass('s_active');
     }

    function showDivs(n) {
       var i;
      var x = document.getElementsByClassName("mySlides");
       if (n > x.length) {slideIndex = 1}    
       if (n < 1) {slideIndex = x.length}
       for (i = 0; i < x.length; i++) {
         x[i].style.display = "none";  
       }
       x[slideIndex-1].style.display = "block";  
     }
			</script>

			<!-- slider Scripct -->
<script defer src="<?php echo base_url('assets1/java_script/career.js') ?>" type="text/javascript">
    </script> 
    <script defer src="<?php echo base_url('assets1/java_script/slick.min.js') ?>" type="text/javascript">
    </script> 
    <script defer src="<?php echo base_url('assets1/java_script/slick-slider-post.js') ?>" type="text/javascript">
    </script> 
    <script defer src="<?php echo base_url('assets1/java_script/slick-slider-featherlight-helper.js') ?>" type="text/javascript">
    </script> 
    <script defer src="<?php echo base_url('assets1/java_script/slick-slider-init.js') ?>" type="text/javascript">
    </script>
    <script defer src="<?php echo base_url('assets1/java_script/slick-slider-options-media.min.js') ?>" type="text/javascript">
    </script> 
    <script defer src="<?php echo base_url('assets1/java_script/slick-slider-post.min.js') ?>" type="text/javascript">
    </script>

    <script type="text/javascript">
    $(document).ready(function(){
            $('.customer-logos').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 1000,
                arrows: false,
                dots: false,
                    pauseOnHover: false,
                    responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3
                    }
                }, {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 2
                    }
                }]
            });
        });
    $(document).ready(function($) {
  $('.tab_content').hide();
  $('.tab_content:first').show();
  $('.tabs li:first').addClass('active');
  $('.tabs li').click(function(event) {
    $('.tabs li').removeClass('active');
    $(this).addClass('active');
    $('.tab_content').hide();

    var selectTab = $(this).find('a').attr("href");

    $(selectTab).fadeIn();
  });
});
    </script>

		