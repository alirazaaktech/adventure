<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_data')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_data'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>View License Section 
                                </div>
                            </div>
                            <div class="portlet-body">
                            
                                <a href="<?php echo base_url('admin/License/add'); ?>" class="btn blue" type="button" >ADD</a>&nbsp;&nbsp; 
                                    
                                <div class="table-scrollable">
                                    
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="5%">ID</th>
                                                <th width="10%">Logo</th>
                                             
                                                
                                                <th width="25%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          
                                              <?php
                                              
                                                if(isset($products) && !empty($products))
                                                { 
                                              $count =1;
                                                    foreach($products as $value)
                                                    {   
                                                            $id =  $value['id'];
                                                             $heading =  $value['logo'];
                                                           
                                                          
                                            ?>
                                                        <tr>
                                                            <td><?php echo $count++; ?></td>
                                                         <td>
                                                                <img src="<?php echo base_url() ?>assets1/images/<?php echo $heading; ?>" style="max-width:150px; max-height:70px;">
                                                            </td>
                                                        
                                                            <td>
                                                <a href="<?php echo base_url('admin/License/update/'.$id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                  <a href="<?php echo base_url('admin/License/delete/'.$value['id']); ?>" class="btn blue" type="button" >Delete</a></span></td> 
                                              <!--  -->

                                                        </tr>
                                            <?php   

                                                    }
                                                }
                                               $count++
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                              <div class="portlet-body">
                         
                                    
                                <div class="table-scrollable">

                                           <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                               
                                                <th width="10%">Paragraph</th>
                                             
                                                
                                                <th width="25%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          
                                              <?php 
                                          $heading =  $head[0]['heading'];
                                                           
                                                          
                                            ?>
                                                        <tr>
                                                           
                                                         <td>
                                                             <?php echo  $heading ?>
                                                            </td>
                                                        
                                                            <td>
                                                <a href="<?php echo base_url('admin/License/updates/'.$id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                 </td> 
                                              <!--  -->

                                                        </tr>
                                           
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
