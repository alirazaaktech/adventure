  <div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Update License Section

                        </span>
                    </div>                               
                </div>
    <div>  
    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet light bordered" id="portlet">

        <div class="portlet-body form">
            <form role="form" method="Post" enctype="multipart/form-data" action="<?php echo base_url('admin/License/process_update') ?>">
               <input type="hidden" name="id" value="<?php  echo $data['id']; ?>">
               <div class="form-body">
                 <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                        <img src="<?php echo base_url()?>assets1/images/<?php echo $data['logo']; ?>" > 
                   </div>
                   <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                   <div>
                    <span class="btn default btn-file">
                        <span class="fileinput-new"> Select image </span>
                        <span class="fileinput-exists" name="logo"> Change </span>
                        <input type="file" name="logo"> </span>
                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                    </div>

                </div>



                <div class="form-actions noborder">
                    <input type="Submit" class="btn blue" value="Submit">
                    <!-- <button type="button" class="btn default">Cancel</button> -->
                </div>
            </div>
        </form>
    </div>
</div>
</div>
</div>

</div>
