<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_data')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_data'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>View Layout Section
                                </div>
                            </div>
                            <div class="portlet-body">
                           
                                    
                                <div class="table-scrollable">
                                    
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="5%">ID</th>
                                        
                                                <th width="10%">Heading</th>
                                                <th width="45%">Paragraph</th>
                                                <th width="10%">Image</th>
                                                <th width="25%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          
                                              <?php 
                                              $count =1;
                                                    foreach($products as $value)
                                                    {   
                                                            $id =  $value['id'];
                                                           $heading = $value['head_section_h'];
                                                            $paragraph = $value['head_section_p'];
                                                            $img =  $value['head_section_image'];
                                                          
                                                          
                                            ?>
                                                        <tr>

                                                            <td><?php echo $count++; ?></td>
                                                     
                                                             <td><?php echo $heading; ?></td>
                                                            <td><?php echo $paragraph; ?></td>
                                                         
                                                                <td>
                                                                <img src="<?php echo base_url() ?>assets1/images/<?php echo $img; ?>" style="max-width:150px; max-height:70px;">
                                                            </td>
                                                        
                                                            <td>
                                                <a href="<?php echo base_url('admin/Layout/updates/'.$id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                           </span></td> 
                                              <!--  -->

                                                        </tr>
                                            <?php   

                                                    }
                                                // $count++
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
