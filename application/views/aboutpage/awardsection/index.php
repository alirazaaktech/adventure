<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_data')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_data'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>View Awards
                                </div>
                            </div>
                            <div class="portlet-body">
                                  <?php
                                                if(isset($products) && count($products) < 1)
                                                {  ?>
                                <a href="<?php echo base_url('admin/About/add'); ?>" class="btn blue" type="button" >ADD</a>&nbsp;&nbsp; <?php } ?>
                                    
                                <div class="table-scrollable">
                                    
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="5%">ID</th>
                                                <th width="10%">Heading</th>
                                                <th width="45%">Paragraph</th>
                                                <th width="10%">Path</th>
                                                <th width="25%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          
                                              <?php 
                                                    foreach($products as $value)
                                                    {   
                                                            $id =  $value['id'];
                                                             $heading =  $value['heading'];
                                                            $paragraph =  $value['paragraph'];
                                                            $path =  $value['image'];
                                                          
                                            ?>
                                                        <tr>
                                                            <td><?php echo $id; ?></td>
                                                             <td><?php echo $heading; ?></td>
                                                            <td><?php echo $paragraph; ?></td>
                                                            <td><?php echo $path ?></td>
                                                        
                                                            <td>
                                                <a href="<?php echo base_url('About/update/'.$id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                <a href="<?php echo base_url('About/delete/'.$id); ?>" class="btn blue" type="button" >Delete</a> &nbsp;
                                                            </td>

                                                        </tr>
                                            <?php   
                                                    }
                                                
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
