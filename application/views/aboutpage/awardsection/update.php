  <div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Update Award Section

                        </span>
                    </div>                               
                </div>
    <div>   
  
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered" id="portlet">
                              
                                <div class="portlet-body form">
                                    <form role="form" method="Post" enctype="multipart/form-data" action="<?php echo base_url('admin/About/process_update') ?>">
                                         <input type="hidden" name="id" value="<?php  echo $data['id']; ?>">
                                        <div class="form-body">
                                            <label for="form_control_1">Heading</label>
                                              <div class="form-group form-md-line-input">
                                            <textarea rows="8" cols="60"  name="heading"><?php echo (set_value('heading'))?set_value('heading'):( isset($data['heading'])?$data['heading']:'' ) ?></textarea>
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('heading'); ?></span>
                                            </div>

                                            <label for="form_control_1">Paragraph</label>
                                              <div class="form-group form-md-line-input">
                                            <textarea rows="8" cols="60"  name="paragraph"><?php echo (set_value('paragraph'))?set_value('paragraph'):( isset($data['paragraph'])?$data['paragraph']:'' ) ?></textarea>
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('paragraph'); ?></span>
                                            </div>
                                                <label for="form_control_1">Image</label>
                                              <div class="form-group form-md-line-input">
                                            <textarea rows="8" cols="60"  name="image"><?php echo (set_value('image'))?set_value('image'):( isset($data['image'])?$data['image']:'' ) ?></textarea>
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('image'); ?></span>
                                            </div>
                                             
                                              
                                          <div class="form-actions noborder">
                                            <input type="Submit" class="btn blue" value="Submit">
                                            <!-- <button type="button" class="btn default">Cancel</button> -->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

                         
    </div>
