<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Add Value

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/Value/process_add');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                        <div>
                          
                                <h3>Provide details</h3>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Heading
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                       <textarea cols="42" rows="3" required name="heading" placeholder="Provide your heading"></textarea>
                                        <span id="color"><?php echo form_error('heading')?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Paragraph
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea cols="42" rows="3" required name="paragraph" placeholder="Provide your Paragraph"></textarea>
                                        <span id="color"><?php echo form_error('paragraph')?></span>
                                    </div>
                                </div>
                            <div class="form-group" >
                                                <input type="hidden" name="image" value="no_image_select.png">
                                </div>
                      
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists" name="photo"> Change </span>
                                        <input type="file" name="image"> </span>
                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                        </div>

                       

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->





