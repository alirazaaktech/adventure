  <div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Update Value Section

                        </span>
                    </div>                               
                </div>
    <div>  
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered" id="portlet">
                              
                                <div class="portlet-body form">
                                    <form role="form" method="Post" enctype="multipart/form-data" action="<?php echo base_url('admin/Value/process_update') ?>">
                                         <input type="hidden" name="id" value="<?php  echo $data['id']; ?>">
                                        <div class="form-body">
                                            <label for="form_control_1">Heading</label>
                                              <div class="form-group form-md-line-input">
                                            <textarea rows="8" cols="40"  name="heading"><?php echo (set_value('heading'))?set_value('heading'):( isset($data['heading'])?$data['heading']:'' ) ?></textarea>
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('heading'); ?></span>
                                            </div>

                                            <label for="form_control_1">Paragraph</label>
                                              <div class="form-group form-md-line-input">
                                            <textarea rows="8" cols="40"  name="paragraph"><?php echo (set_value('paragraph'))?set_value('paragraph'):( isset($data['paragraph'])?$data['paragraph']:'' ) ?></textarea>
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('paragraph'); ?></span>
                                            </div>
                                              
                            
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                     <img src="<?php echo base_url()?>assets1/images/<?php echo $data['image']; ?>" > 
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists" name="image"> Change </span>
                                        <input type="file" name="image"> </span>
                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            
                        </div>
                    </div>
                   <div class="form-actions noborder">
                                            <input type="Submit" class="btn blue" value="Submit">
                                            <!-- <button type="button" class="btn default">Cancel</button> -->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
</div>
</div>
</div>
                         
    </div>
