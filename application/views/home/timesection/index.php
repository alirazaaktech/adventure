<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_data')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_data'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>View Head Office Section
                                </div>
                            </div>
                            <div class="portlet-body">
                              
                                <a href="<?php echo base_url('admin/Timesection/add'); ?>" class="btn blue" type="button" >ADD</a>&nbsp;&nbsp;
                                    
                                <div class="table-scrollable">
                                    
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="5%">ID</th>
                                                <th width="10%">Office Name</th>
                                                <th width="45%">Country</th>
                                                <th width="10%">Day From</th>
                                                <th width="25%">Time From</th>
                                                 <th width="10%">Day To</th>
                                                <th width="45%">Time To</th>
                                                <th width="10%">Email</th>
                                                <th width="25%">Number</th>
                                                <th width="10%">Address</th>
                                                <th width="10%">Offset</th>
                                                <th width="25%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          
                                              <?php

                                                if(isset($products) && !empty($products))
                                                {  
                                                    foreach($products as $value)
                                                    {   
                                                            $id =  $value['id'];
                                                            $office_name =  $value['office_name'];
                                                            $office_country =  $value['office_country'];
                                                            $office_day_from =  $value['office_day_from'];
                                                            $office_time_from =  $value['office_time_from'];
                                                            $office_day_to =  $value['office_day_to'];
                                                            $office_time_to =  $value['office_time_to'];
                                                            $office_email =  $value['office_email'];
                                                            $office_number =  $value['office_number'];
                                                            $office_address =  $value['office_address'];
                                                            $offset =  $value['offset'];

                                                          
                                            ?>
                                                        <tr>
                                                            <td><?php echo $id; ?></td>
                                                             <td><?php echo $office_name; ?></td>
                                                            <td><?php echo $office_country; ?></td>
                                                            <td><?php echo $office_day_from ?></td>
                                                             <td><?php echo $office_time_from; ?></td>
                                                            <td><?php echo $office_day_to; ?></td>
                                                            <td><?php echo $office_time_to ?></td>
                                                             <td><?php echo $office_email; ?></td>
                                                            <td><?php echo $office_number; ?></td>
                                                            <td><?php echo $office_address ?></td>
                                                             <td><?php echo $offset ?></td>
                                                        
                                                            <td>
                                                <a href="<?php echo base_url('admin/Timesection/update/'.$id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                <a href="<?php echo base_url('admin/Timesection/delete/'.$id); ?>" class="btn blue" type="button" >Delete</a> &nbsp;
                                                            </td>

                                                        </tr>
                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
