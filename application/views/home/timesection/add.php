<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Head Office Add

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/Timesection/process_add');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                        <div>
                          
                                <h3>Provide details</h3>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Office Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                       <input type="text" required name="office_name" placeholder="Provide your heading" class="form-control">
                                        <span id="color"><?php echo form_error('office_name')?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Country Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                      <input type="text" required name="office_country" placeholder="Provide your heading" class="form-control">
                                        <span id="color"><?php echo form_error('office_country')?></span>
                                    </div>
                                </div>

                       <div class="form-group">
                                    <label class="control-label col-md-3">Day From
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                    <input type="text" required name="office_day_from" placeholder="Provide your heading" class="form-control">
                                        <span id="color"><?php echo form_error('office_day_from')?></span>
                                    </div>
                                </div>
                       
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3">Time From
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                       <input type="time" required name="office_time_from" placeholder="Provide your heading" class="form-control">
                                        <span id="color"><?php echo form_error('office_time_from')?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Day To
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                      <input type="text" required name="office_day_to" placeholder="Provide your heading" class="form-control">
                                        <span id="color"><?php echo form_error('office_day_to')?></span>
                                    </div>
                                </div>

                       <div class="form-group">
                                    <label class="control-label col-md-3">Time To
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                       <input type="time" required name="office_time_to" placeholder="Provide your heading" class="form-control">
                                        <span id="color"><?php echo form_error('office_time_to')?></span>
                                    </div>
                                </div>
                                  
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                       <input type="email" required name="office_email" placeholder="Provide your heading" class="form-control">
                                        <span id="color"><?php echo form_error('office_email')?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Number
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                      <input type="text" required name="office_number" placeholder="Provide your heading" class="form-control">
                                        <span id="color"><?php echo form_error('office_number')?></span>
                                    </div>
                                </div>

                       <div class="form-group">
                                    <label class="control-label col-md-3">Address
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                <input type="text" required name="office_address" placeholder="Provide your heading" class="form-control">
                                        <span id="color"><?php echo form_error('office_address')?></span>
                                    </div>
                                </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3">offset
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                <input type="number" required name="offset" step="any" placeholder="Provide your Offcet" class="form-control">
                                        <span id="color"><?php echo form_error('offset')?></span>
                                    </div>
                                </div>
                       

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->





