   
    <div>
                            <!-- BEGIN SAMPLE FORM PORTLET-->
                            <div class="portlet light bordered" id="portlet">
                              
                                <div class="portlet-body form">
                                    <form role="form" method="Post" enctype="multipart/form-data" action="<?php echo base_url('admin/Timesection/process_update') ?>">
                                         <input type="hidden" name="id" value="<?php  echo $data['id']; ?>">
                                        <div class="form-body">
                                            <label for="form_control_1">Office Name</label>
                                              <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" name="office_name" value="<?php echo (set_value('office_name'))?set_value('office_name'):( isset($data['office_name'])?$data['office_name']:'' ) ?>">
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('office_name'); ?></span>
                                            </div>

                                            <label for="form_control_1">Country</label>
                                              <div class="form-group form-md-line-input">
                                         <input type="text" class="form-control" name="office_country" value="<?php echo (set_value('office_country'))?set_value('office_country'):( isset($data['office_country'])?$data['office_country']:'' ) ?>">
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('office_country'); ?></span>
                                            </div>
                                                <label for="form_control_1">Day From</label>
                                              <div class="form-group form-md-line-input">
                                           <input type="text" class="form-control" name="office_day_from" value="<?php echo (set_value('office_day_from'))?set_value('office_day_from'):( isset($data['office_day_from'])?$data['office_day_from']:'' ) ?>">
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('office_day_from'); ?></span>
                                            </div>
                                             <label for="form_control_1">Time From</label>
                                              <div class="form-group form-md-line-input">
                                            <input type="time" class="form-control" name="office_time_from" value="<?php echo (set_value('office_time_from'))?set_value('office_time_from'):( isset($data['office_time_from'])?$data['office_time_from']:'' ) ?>">
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('office_time_from'); ?></span>
                                            </div>

                                            <label for="form_control_1">Day To</label>
                                              <div class="form-group form-md-line-input">
                                         <input type="text" class="form-control" name="office_day_to" value="<?php echo (set_value('office_day_to'))?set_value('office_day_to'):( isset($data['office_day_to'])?$data['office_day_to']:'' ) ?>">
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('office_day_to'); ?></span>
                                            </div>
                                                <label for="form_control_1">Time To</label>
                                              <div class="form-group form-md-line-input">
                                           <input type="time" class="form-control" name="office_time_to" value="<?php echo (set_value('office_time_to'))?set_value('office_time_to'):( isset($data['office_time_to'])?$data['office_time_to']:'' ) ?>">
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('office_time_to'); ?></span>
                                            </div>
                                             
                                              <label for="form_control_1">Email</label>
                                              <div class="form-group form-md-line-input">
                                            <input type="email" class="form-control" name="office_email" value="<?php echo (set_value('office_email'))?set_value('office_email'):( isset($data['office_email'])?$data['office_email']:'' ) ?>">
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('office_email'); ?></span>
                                            </div>

                                            <label for="form_control_1">Number</label>
                                              <div class="form-group form-md-line-input">
                                         <input type="text" class="form-control" name="office_number" value="<?php echo (set_value('office_number'))?set_value('office_number'):( isset($data['office_number'])?$data['office_number']:'' ) ?>">
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('office_number'); ?></span>
                                            </div>
                                                <label for="form_control_1">Address</label>
                                              <div class="form-group form-md-line-input">
                                           <input type="text" class="form-control" name="office_address" value="<?php echo (set_value('office_address'))?set_value('office_address'):( isset($data['office_address'])?$data['office_address']:'' ) ?>">
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('office_address'); ?></span>
                                            </div>
                                                    <label for="form_control_1">Offset</label>
                                              <div class="form-group form-md-line-input">
                                           <input type="number" class="form-control" name="offset" step="any" value="<?php echo (set_value('offset'))?set_value('offset'):( isset($data['offset'])?$data['offset']:'' ) ?>">
                                               
                                                <span class="help-block">Some help goes here...</span>
                                                <span style="color: red"> <?php echo form_error('offset'); ?></span>
                                            </div>
                                             
                                             
                                              
                                          <div class="form-actions noborder">
                                            <input type="Submit" class="btn blue" value="Submit">
                                            <!-- <button type="button" class="btn default">Cancel</button> -->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                         
    </div>
