<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Section_4 Update Expert team

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/Home_section_4/update_data');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                        <div>
                          
                                <h3>Provide details</h3>

                                  <div class="form-group">
                                <label class="control-label col-md-3">Select Categories
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4" >

                                    <select name="cat[]" class="selectpicker form-control" multiple title="Choose Foods" multiple data-max-options="2" data-live-search="true">
                               
                                    <?php  if(isset($categories) && !empty($categories)) 
                                            {   
                                                foreach ($categories as $key) 
                                                {
                                                    $cat_id = $key['home_section_5_post_categories_id'];
                                                    $cat = $key['home_section_5_post_categorie_text'];
                                    ?>

                                                     <option style="height: 30px;" value="<?php echo $cat; ?>"><?php echo $cat;  ?></option>     
                                    <?php
                                                }

                                            } 
                                    ?>
                                    </select>
                                </div>
                            </div>


                                <?php
                                if(isset($data) && !empty($data))
                                {
                                 foreach ($data as $value) 
                                 {

                                     $home_section_4_id = $value['home_section_4_id'];
                                     $home_section_4_name = $value['home_section_4_name'];
                                    $home_section_4_role = $value['home_section_4_role'];
                                    $home_section_4_location_title = $value['home_section_4_location_title'];
                                     $home_section_4_location_url = $value['home_section_4_location_url'];
                                    $home_section_4_link = $value['home_section_4_link'];
                                    $home_section_4_p = $value['home_section_4_p'];
                                    $home_section_4_post_cat = $value['home_section_4_post_cat'];


        
                                    $image = $value['home_section_4_img'];
                                    
                                    
                                 ?>

                                 <div class="form-group">
                                    <label class="control-label col-md-3">Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="home_section_4_name" value="<?php echo  $home_section_4_name;?>" placeholder="Provide your Name" />
                                        <span id="color"><?php echo form_error('home_section_4_name')?></span>
                                    </div>
                                </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3">Categories
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" readonly name="home_section_4_post_cat" value="<?php echo  $home_section_4_post_cat;?>" placeholder="Provide your Location Title" />
                                       
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Role
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="home_section_4_role" value="<?php echo  $home_section_4_role;?>" placeholder="Provide your Role" />
                                        <span id="color"><?php echo form_error('home_section_4_role')?></span>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Location title
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="home_section_4_location_title" value="<?php echo  $home_section_4_location_title;?>" placeholder="Provide your Location Title" />
                                        <span id="color"><?php echo form_error('home_section_4_location_title')?></span>
                                    </div>
                                </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3">Location Url
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="home_section_4_location_url" value="<?php echo  $home_section_4_location_url;?>" placeholder="Provide your Location Url" />
                                        <span id="color"><?php echo form_error('home_section_4_location_url')?></span>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Location Link
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="home_section_4_link" value="<?php echo  $home_section_4_link;?>" placeholder="Provide your Link" />
                                        <span id="color"><?php echo form_error('home_section_4_link')?></span>
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label class="control-label col-md-3">Paragraph
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea cols="44" rows="3" required name="home_section_4_p" value="<?php echo  $home_section_4_p;?>" placeholder="Provide your Paragraph"><?php echo  $home_section_4_p;?></textarea>
                                        <span id="color"><?php echo form_error('home_section_4_p')?></span>
                                    </div>
                                </div>

                                 <div class="form-group" >
                                    <div class="col-md-4">
                                        <input type="hidden" class="form-control"  name="home_section_4_id"  value="<?php echo  $home_section_4_id;?>"  />
                                        <input  type="hidden" class="form-control" name="old_image" value="<?php echo  $image ?>">
                                    </div>
                                </div>
                                    
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                 <img src="<?php echo base_url()?>assets1/images/<?php  echo $image;  ?>" > 
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists" name="photo"> Change </span>
                                                    <input type="file" name="photo"> </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php 
                                    }

                                } 
                    ?>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->





