<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_data')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_data'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>Section 4 Expert team
                                </div>
                            </div>
                            <div class="portlet-body">
                                <a href="<?php echo base_url('admin/home_section_4/add'); ?>" class="btn blue" type="button" >ADD</a>&nbsp;&nbsp;
                                    
                                <div class="table-scrollable">
                                    
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="10px">ID</th>
                                                
                                                <th width="10px">Name</th>
                                                <th>Categories</th>
                                                <th width="10px">Role</th>
                                                <th width="10px">Location Title</th>
                                                 <th width="30px" >location url</th>
                                                <th width="40px">Paragraph</th>
                                                <th width="10px">Link</th>
                                               
                                                <th width="10px">Image</th>
                                                <th width="20px">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($section_4) && !empty($section_4))
                                                {
                                                    $count = 1;
                                                    foreach($section_4 as $value)
                                                    {   
                                                        $home_section_4_id =  $value['home_section_4_id'];
                                                        $home_section_4_name =  $value['home_section_4_name'];
                                                        $home_section_4_post_cat =  $value['home_section_4_post_cat'];
                                                        $home_section_4_role =  $value['home_section_4_role'];
                                                        $home_section_4_location_title =  $value['home_section_4_location_title'];
                                                         $home_section_4_p =  $value['home_section_4_p'];
                                                         $home_section_4_location_url =  $value['home_section_4_location_url'];

                                                          $home_section_4_link =  $value['home_section_4_link'];
                                                           $home_section_4_img =  $value['home_section_4_img'];
                                                          
                                            ?>
                                                        <tr>
                                                            <td><?php echo $count++; ?></td>
                                                            <td><?php echo $home_section_4_name; ?></td>
                                                            <td><?php echo $home_section_4_post_cat; ?></td>
                                                            <td><?php echo $home_section_4_role ?></td>
                                                            <td><?php echo $home_section_4_location_title; ?></td>
                                                            <td><?php echo $home_section_4_location_url ?></td>
                                                            <td><?php echo $home_section_4_p; ?></td>
                                                            <td><?php echo $home_section_4_link ?></td>
                                                          
                                                            <td>
                                                                <img src="<?php echo base_url() ?>assets1/images/<?php echo $home_section_4_img; ?>" style="max-width:150px; max-height:70px;">
                                                            </td>
                                                            <td>
                                                                <a href="<?php echo base_url('admin/home_section_4/update/'.$home_section_4_id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                                <a href="<?php echo base_url('admin/home_section_4/delete/'.$home_section_4_id); ?>" class="btn blue" type="button" >Delete</a> &nbsp;
                                                            </td>

                                                        </tr>
                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
