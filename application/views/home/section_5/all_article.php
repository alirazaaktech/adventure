<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_data')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_data'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>Section 5 Our publications
                                </div>
                            </div>
                            <div class="portlet-body">
                                <a href="<?php echo base_url('admin/home_section_5/add_article'); ?>" class="btn blue" type="button" >ADD</a>&nbsp;&nbsp;
                                    
                                <div class="table-scrollable">
                                    
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Article Categories</th>
                                                <th>Articles Title</th>
                                                <th>Articles Writer</th>
                                                <th>Date</th>
                                                <th>Image</th>
                                                <th >Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($section_5_article) && !empty($section_5_article))
                                                {
                                                    $count = 1;
                                                    foreach($section_5_article as $value)
                                                    {   
                                                        $home_section_5_post_article_id =  $value['home_section_5_post_article_id'];
                                                        $home_section_5_post_cat =  $value['home_section_5_post_cat'];
                                                        $home_section_5_post_article_title =  $value['home_section_5_post_article_title'];
                                                        $home_section_5_post_article_writer =  $value['home_section_5_post_article_writer'];
                                                        $home_section_5_post_article_image =  $value['home_section_5_post_article_image'];
                                                        $home_section_5_post_article_date =  $value['home_section_5_post_article_date'];

                                            ?>
                                                        <tr>
                                                            <td><?php echo $count++; ?></td>
                                                            <td><?php echo $home_section_5_post_cat; ?></td>
                                                            <td><?php echo $home_section_5_post_article_title ?></td>
                                                            <td><?php echo $home_section_5_post_article_writer ?></td>
                                                            <td><?php echo $home_section_5_post_article_date ?></td>
                                            
                                                            <td>
                                                                <img src="<?php echo base_url() ?>assets1/images/<?php echo $home_section_5_post_article_image; ?>" style="max-width:150px; max-height:70px;">
                                                            </td>
                                                            <td>
                                                               
                                                                <a href="<?php echo base_url('admin/home_section_5/update_article/'.$home_section_5_post_article_id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                                <a href="<?php echo base_url('admin/home_section_5/delete_article/'.$home_section_5_post_article_id); ?>" class="btn blue" type="button" >Delete</a> &nbsp;
                                                            </td>

                                                        </tr>
                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
