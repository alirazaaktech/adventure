<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Section_4 Update Expert team

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/Home_section_5/update_article_process');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                        <div>
                          
                            <h3>Provide details</h3>

                             <div class="form-group">
                                <label class="control-label col-md-3">Select Categories
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-6" >

                                    <select name="cat[]" class="selectpicker form-control" multiple title="Choose Foods" multiple data-max-options="2" data-live-search="true">
                               
                                    <?php  if(isset($section_5_cat) && !empty($section_5_cat)) 
                                            {   
                                                foreach ($section_5_cat as $key) 
                                                {
                                                    $cat_id = $key['home_section_5_post_categories_id'];
                                                    $cat = $key['home_section_5_post_categorie_text'];
                                    ?>

                                                     <option style="height: 30px;" value="<?php echo $cat; ?>"><?php echo $cat;  ?></option>     
                                    <?php
                                                }

                                            } 
                                    ?>
                                    </select>
                                </div>
                            </div>
                                
                                <?php
                                
                                if(isset($data) && !empty($data))
                                {
                                 foreach ($data as $value) 
                                 {

                                     $home_section_5_post_article_id = $value['home_section_5_post_article_id'];
                                    $home_section_5_post_article_title = $value['home_section_5_post_article_title'];
                                    $home_section_5_post_article_writer = $value['home_section_5_post_article_writer'];
                                    $home_section_5_post_article_date = $value['home_section_5_post_article_date'];
                                    $image = $value['home_section_5_post_article_image'];
                                    $cat = $value['home_section_5_post_cat'];
                                    
                                    
                                 ?>
                                 <div class="form-group">
                            <label class="control-label col-md-3">Article Title
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" value="<?php echo $home_section_5_post_article_title ;?>" name="home_section_5_post_article_title" placeholder="Provide your Name" />
                                <span id="color"><?php echo form_error('home_section_5_post_article_title')?></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3">Article Writer
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" value="<?php echo $home_section_5_post_article_writer ;?>" name="home_section_5_post_article_writer" placeholder="Provide your Role" />
                                <span id="color"><?php echo form_error('home_section_5_post_article_writer')?></span>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Post cat
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" readonly class="form-control" name="old_cat"  value="<?php echo  $cat  ?>" placeholder="Provide your Location Url" />
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3">Post Date
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" readonly class="form-control" name="date" value="<?php echo date('M  d,  Y'); ?>" placeholder="Provide your Location Url" />
                            </div>
                        </div>

                        <div class="form-group" >
                            <div class="col-md-4">
                                <input type="hidden" class="form-control"  name="home_section_5_post_article_id"  value="<?php echo  $home_section_5_post_article_id;?>"  />
                                <input  type="hidden" class="form-control" name="old_image" value="<?php echo  $image ?>">
                            </div>
                        </div>
                         
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                       <img src="<?php echo base_url()?>assets1/images/<?php  echo $image;  ?>" > 
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                    </div>
                                    <div>
                                        <span class="btn default btn-file">
                                            <span class="fileinput-new"> Select image </span>
                                            <span class="fileinput-exists" name="photo"> Change </span>
                                            <input type="file" name="photo"> </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                                 
                        <?php 
                                    }

                                } 
                    ?>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->





