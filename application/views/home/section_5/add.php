<style type="text/css">

#color
{
    color: red;
}
</style>
    

<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Section 5 Our publications

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/home_section_5/insert');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                        
                        <h3>Provide details</h3>
                        <!--  <div class="form-group">
                             <label class="control-label col-md-3">Select Categories
                                <span class="required"> * </span>
                            </label>
                          <div class="multiselect" id="countries" multiple="multiple" data-target="multi-0">
                            <select name="cat[]">
                            <div class="title noselect">
                                <span class="text">Select</span>
                                <span class="close-icon">&times;</span>
                                <span class="expand-icon">&plus;</span>
                            </div> -->
                            
                            <div class="form-group">
                                <label class="control-label col-md-3">Select Categories
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-6" >

                                    <select name="cat[]" class="selectpicker form-control" multiple title="Choose Foods" multiple data-max-options="2" data-live-search="true">
                               
                                    <?php  if(isset($section_5_cat) && !empty($section_5_cat)) 
                                            {   
                                                foreach ($section_5_cat as $key) 
                                                {
                                                    $cat_id = $key['home_section_5_post_categories_id'];
                                                    $cat = $key['home_section_5_post_categorie_text'];
                                    ?>

                                                     <option style="height: 30px;" value="<?php echo $cat; ?>"><?php echo $cat;  ?></option>     
                                    <?php
                                                }

                                            } 
                                    ?>
                                    </select>
                                </div>
                            </div>
            
                         <div class="form-group">
                            <label class="control-label col-md-3">Post Title
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="home_section_5_post_title" placeholder="Provide your Name" />
                                <span id="color"><?php echo form_error('home_section_5_post_title')?></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="control-label col-md-3">Post Writer
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="home_section_5_post_writer" placeholder="Provide your Role" />
                                <span id="color"><?php echo form_error('home_section_5_post_writer')?></span>
                            </div>
                        </div>

                        <!--  <div class="form-group">
                            <label class="control-label col-md-3">Article Title
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="home_section_5_article_title" placeholder="Provide your Location Title" />
                                <span id="color"><?php //echo form_error('home_section_5_article_title')?></span>
                            </div>
                        </div> -->
 
                        <div class="form-group">
                            <label class="control-label col-md-3">Psot Date
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" readonly class="form-control" name="date" value="<?php echo date('M  d,  Y'); ?>" placeholder="Provide your Location Url" />
                                <span id="color"><?php echo form_error('date')?></span>
                            </div>
                        </div>

                        <!--  -->
                                  <!--  -->
                       
                        <!--  -->

                      



                        <!--  -->
                      <!--   <div class="form-group">
                            <label class="control-label col-md-3">article Paragraph
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-6">
                                <textarea cols="67" rows="8" required name="home_section_5_post_text" placeholder="Provide your Paragraph"></textarea>
                                <span id="color"><?php //echo form_error('home_section_5_post_text')?></span>
                            </div>
                        </div> -->
                            
                        <div class="form-group" >
                                <input type="hidden" name="no_img_select" value="no_image_select.png">
                        </div>
                         
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> 
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                    </div>
                                    <div>
                                        <span class="btn default btn-file">
                                            <span class="fileinput-new"> Select image </span>
                                            <span class="fileinput-exists" name="photo"> Change </span>
                                            <input type="file" name="photo"> </span>
                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->
 

<script>
$(document).ready(function(){
    //alert('edwe');
    // $("#btn1").click(function(){
    //     $("p").append(" <b>Appended text</b>.");
    // });
    // $("#btn2").click(function(){
    //     $("ol").append("<li>Appended item</li>");
    // });
});
</script>
</head>