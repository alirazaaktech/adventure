<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Section_3 Update Industries serve 

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/Home_section_3/update_data');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                        <div>
                          
                                <h3>Provide details</h3>
                                <?php
                                if(isset($data) && !empty($data))
                                {
                                 foreach ($data as $value) 
                                 {

                                     $home_section_3_id = $value['home_section_3_id'];
                                     $home_section_3_h = $value['home_section_3_h'];
                                    $home_section_3_p = $value['home_section_3_p'];
        
                                    $image = $value['home_section_3_img'];
                                    
                                    
                                 ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Heading
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="home_section_3_h" value="<?php echo  $home_section_3_h;?>" placeholder="Provide your Heading" />
                                        <span id="color"><?php echo form_error('home_section_3_h')?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Paragraph
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea cols="42" rows="3" required name="home_section_3_p" value="<?php echo  $home_section_3_p;?>" placeholder="Provide your Paragraph"><?php echo  $home_section_3_p;?></textarea>
                                        <span id="color"><?php echo form_error('sec_in_p')?></span>
                                    </div>
                                </div>

                           
                                
                                <div class="form-group" >
                                    <div class="col-md-4">
                                        <input type="hidden" class="form-control"  name="home_section_3_id"  value="<?php echo  $home_section_3_id;?>"  />
                                        <input  type="hidden" class="form-control" name="old_image" value="<?php echo  $image ?>">
                                    </div>
                                </div>
                        
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                     <img src="<?php echo base_url()?>assets1/images/<?php  echo $image;  ?>" > 
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists" name="photo"> Change </span>
                                        <input type="file" name="photo"> </span>
                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                        </div>
                        <?php 
                                    }

                                } 
                    ?>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->





