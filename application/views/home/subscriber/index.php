<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>All Subscriber
                                </div>
                            </div>
                            <div class="portlet-body">
                                
                                    
                                <div class="table-scrollable">
                                    
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="5%">ID</th>
                                                <th width="20%">Email</th>
                                                <th width="30%">Date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($subscriber) && !empty($subscriber))
                                                {
                                                    $count = 1;
                                                    foreach($subscriber as $value)
                                                    {   
                                                       
                                                         
                                                          $subscriber_email =  $value['subscriber_email'];
                                                           $created_at =  $value['created_at'];
                                                     
                                                          
                                            ?>
                                                        <tr>
                                                             <td><?php echo $count++; ?></td>
                                                            <td><?php echo $subscriber_email; ?></td>
                                                            <td><?php echo $created_at; ?></td>
                                                           

                                                        </tr>
                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
