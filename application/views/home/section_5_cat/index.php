<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_data')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_data'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>Section 5 All categories
                                </div>
                            </div>
                            <div class="portlet-body">
                                <a href="<?php echo base_url('admin/home_section_5/add_cat'); ?>" class="btn blue" type="button" >ADD</a>&nbsp;&nbsp;
                                    
                                <div class="table-scrollable">
                                    
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="5%">ID</th>
            
                                                <th width="10%">Categories</th>
                                                <th width="25%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($cat) && !empty($cat))
                                                {
                                                    $count = 1;
                                                    foreach($cat as $value)
                                                    {   
                                                        $home_section_5_post_categories_id =  $value['home_section_5_post_categories_id'];
                                                         $home_section_5_post_categorie_text =  $value['home_section_5_post_categorie_text'];
    
                                                          
                                            ?>
                                                        <tr>
                                                             <td><?php echo $count++; ?></td>
                                                            <td><?php echo $home_section_5_post_categorie_text; ?></td>
                                
                                                            <td>
                                                                <a href="<?php echo base_url('admin/home_section_5/update_Cat/'.$home_section_5_post_categories_id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                                <a href="<?php echo base_url('admin/home_section_5/delete_cat/'.$home_section_5_post_categories_id); ?>" class="btn blue" type="button" >Delete</a> &nbsp;
                                                            </td>

                                                        </tr>
                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
