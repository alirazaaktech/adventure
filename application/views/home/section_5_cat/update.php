<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase"> Section_5 categories

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/Home_section_5/update_cat_process');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                         
                                <h3>Provide details</h3>
                                <?php
                                if(isset($data) && !empty($data))
                                {
                                 foreach ($data as $value) 
                                 {

                                     $home_section_5_post_categories_id = $value['home_section_5_post_categories_id'];
                                     $home_section_5_post_categorie_text = $value['home_section_5_post_categorie_text'];
                                
                                 ?>
                             
                                <br>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Category title
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" value="<?php echo $home_section_5_post_categorie_text ?>" name="home_section_5_post_categorie_text" placeholder="Provide your Heading" />
                                        <span id="color"><?php echo form_error('home_section_5_post_categorie_text')?></span>
                                    </div>
                                </div>
                                <br>
                                
                                <div class="form-group" >
                                    <div class="col-md-4">
                                        <input type="hidden" class="form-control"  name="home_section_5_post_categories_id"  value="<?php echo  $home_section_5_post_categories_id;?>"  />
                        
                                    </div>
                                </div>
                        
                           
                       
                        <?php 
                                    }

                                } 
                    ?>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                   
            
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->





