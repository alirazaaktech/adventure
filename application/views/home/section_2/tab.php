  <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_data')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_data'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
 <div class="portlet box green">

    <div class="portlet-title">

            
        <div class="caption">
            <i class="fa fa-gift"></i>Industries</div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
            <a href="#portlet-config" data-toggle="modal" class="config"> </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="tabbable-custom ">
            <ul class="nav nav-tabs ">
                <li <?php if ($tab != 'tab_5_2') {
                                        echo 'class="active"';
                                      } ?> >
                    <a href="#tab_5_1" data-toggle="tab"> all tabs </a>
                </li>
                <li <?php
                                if ($tab == 'tab_5_2') {
                                        echo 'class="active"';
                                      } ?> >
                    <a href="#tab_5_2" data-toggle="tab"> all tabs data </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_5_1">
                    <div class="portlet-body">
                                <a href="<?php echo base_url('admin/home_section_2/add_sec_1'); ?>" class="btn blue" type="button" >ADD</a>&nbsp;&nbsp;
                        <div class="table-scrollable">
                                    
                                    <table class="table table-striped table-bordered ">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Name</th>
                                                <th>Web Link</th>
                                                <th>web Icon</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($tab_1) && !empty($tab_1))
                                                {
                                                    $count = 1;
                                                    foreach($tab_1 as $value)
                                                    {
                                                        $home_section_2_tab_id =  $value['home_section_2_tab_id'];   
                                                        $home_section_2_tab_icon =  $value['home_section_2_tab_icon'];
                                                        $home_section_2_tab_tittle =  $value['home_section_2_tab_tittle'];
                                                        $home_section_2_tab_web_link =  $value['home_section_2_tab_web_link'];                                                          
                                            ?>
                                                        <tr>
                                                            <td><?php echo $count++; ?></td>
                                                            <td><?php echo $home_section_2_tab_tittle ; ?></td>
                                                            <td><?php echo $home_section_2_tab_web_link ?></td>
                                                          
                                                            <td class="bg-blue bg-font-blue">
                                                                <img src="<?php echo base_url() ?>assets1/images/<?php echo $home_section_2_tab_icon; ?>" style="max-width:150px; max-height:70px;">
                                                            </td>
                                                            <td>
                                                                <a href="<?php echo base_url('admin/home_section_2/update_sec_1/'.$home_section_2_tab_id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                                <a href="<?php echo base_url('admin/home_section_2/delete/'.$home_section_2_tab_id); ?>" class="btn blue" type="button" >Delete</a> &nbsp;
                                                            </td>

                                                        </tr>
                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                </div>
                <div class="tab-pane" id="tab_5_2">

                     <div class="portlet-body">
                                <a href="<?php echo base_url('admin/home_section_2/add_sec_2'); ?>" class="btn blue" type="button" >ADD</a>&nbsp;&nbsp;
                        <div class="table-scrollable">
                                    
                                    <table class="table table-striped table-bordered ">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Web Name</th>
                                                <th>Title</th>
                                                <th>Image</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($tab_2) && !empty($tab_2))
                                                {
                                                     $value1='';
                                                     $tab='tab_5_2';
                                                    $count = 1;
                                                    foreach($tab_2 as $value)
                                                    {
                                                        $home_section_2_tabs_data_id =  $value['home_section_2_tabs_data_id'];   
                                                        $home_section_2_tab_tittle =  $value['home_section_2_tab_tittle'];
                                                        $home_section_2_tabs_data_text =  $value['home_section_2_tabs_data_text'];
                                                        $home_section_2_tabs_data_img =  $value['home_section_2_tabs_data_img'];

                                            ?>
                                                        <tr>
                                                            <td>
                                                                <?php   if($count!=5)
                                                                        { 
                                                                            echo $count++;
                                                                        }
                                                                        else
                                                                        {
                                                                            $count = 1;
                                                                            echo $count++;
                                                                        } 
                                                                ?>
                                                                            
                                                            </td>
                                                            <td> 
                                                                <?php if($value1!=$home_section_2_tab_tittle) 
                                                                {
                                                                echo $home_section_2_tab_tittle ; 
                                                                 }
                                                                ?>

                                                            </td>
                                                            <td><?php echo $home_section_2_tabs_data_text ; ?></td>
                                                            <td >
                                                                <?php $value1 = $home_section_2_tab_tittle ; ?>
                                                                <img src="<?php echo base_url() ?>assets1/images/<?php echo $home_section_2_tabs_data_img; ?>" style="max-width:150px; max-height:70px;">
                                                            </td>
                                                            <td>
                                                                <a href="<?php echo base_url('admin/home_section_2/update_sec_2/'.$home_section_2_tabs_data_id.'/'.$tab); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                                <a href="<?php echo base_url('admin/home_section_2/delete_tab_2/'.$home_section_2_tabs_data_id.'/'.$tab); ?>" class="btn blue" type="button" >Delete</a> &nbsp;
                                                            </td>

                                                        </tr>
                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                  
                </div>
            </div>
        </div>
    </div>
</div>