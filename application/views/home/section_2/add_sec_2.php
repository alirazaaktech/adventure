<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Section 2 Industries

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/Home_section_2/insert_sec_2');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                        
                          
                                <h3>Provide details</h3>
                                <?php if(isset($tab_1) && !empty($tab_1))
                                {

                               ?>

                                   <div class="form-group">
                                        <label class="control-label col-md-3">Select Website</label>
                                        <div class="col-md-4">
                                           
                                                <select class="form-control" name="home_section_2_tab_id" data-style="btn-info">
                                                    <?php foreach ($tab_1 as $key ) {
                                                      $home_section_2_tab_id = $key['home_section_2_tab_id'];
                                                      $home_section_2_tab_tittle = $key['home_section_2_tab_tittle'];
                                                      
                                                     ?>

                                                    <option value="<?php echo $home_section_2_tab_id ?>">
                                                        <?php echo $home_section_2_tab_tittle ?>       
                                                    </option>
                                                    <?php } ?>
                                                </select>
                                            
                                        </div>
                                    </div>


                                 <div class="form-group">
                                    <label class="control-label col-md-3">Image Title
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="home_section_2_tabs_data_text" placeholder="Provide your Name" />
                                        <span id="color"><?php echo form_error('home_section_2_tabs_data_text')?></span>
                                    </div>
                                </div>                    
                                <div class="form-group" >
                                        <input type="hidden" name="no_img_select" value="no_image_select.png">
                                        <input type="hidden" name="tab" value="#tab_5_2">
                                </div>
                                <div class="row">
                                   <div class="form-group">
                                    <label class="control-label col-md-3">web Icon
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-6">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> 
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists" name="photo"> Change </span>
                                                    <input type="file" name="photo"> </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                               <!--   <div class="form-group">
                                    <label class="control-label col-md-3">Paragraph
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea cols="42" rows="3" required name="home_section_4_p" placeholder="Provide your Paragraph"></textarea>
                                        <span id="color"><?php //   echo form_error('home_section_4_p')?></span>
                                    </div>
                                </div> -->
                      

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             <?php  
                                } 
            ?>
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->




<script>
$(document).ready(function(){
    //alert('edwe');
    // $("#btn1").click(function(){
    //     $("p").append(" <b>Appended text</b>.");
    // });
    // $("#btn2").click(function(){
    //     $("ol").append("<li>Appended item</li>");
    // });
});
</script>
</head>