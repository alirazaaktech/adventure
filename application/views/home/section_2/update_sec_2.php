<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Section_2 Update Industries 

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/Home_section_2/update_data_sec_2');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                        <div>
                          
                                <h3>Provide details</h3>
                                <?php
                                if(isset($tab_1) && !empty($tab_1))
                                {
                                 foreach ($tab_1 as $value) 
                                 {
                                    $home_section_2_tabs_data_id = $value['home_section_2_tabs_data_id'];
                                    $home_section_2_tabs_data_text = $value['home_section_2_tabs_data_text'];
                                    $home_section_2_tab_id = $value['home_section_2_tab_id'];
                                    $image = $value['home_section_2_tabs_data_img'];
                                    
                                    
                                  $specific = select_data($home_section_2_tab_id) ; 
                                  // echo  $specific[0]['home_section_2_tab_tittle'];
                                  // exit;
                                  ?>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Web Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" readonly value="<?php echo  $specific[0]['home_section_2_tab_tittle']; ?>" /><br>
                                         <input type="hidden" class="form-control" name="tab" value="<?php echo $tab; ?>" />

                                    </div>
                                </div>
                                

                                 <div class="form-group">
                                    <label class="control-label col-md-3">Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="home_section_2_tabs_data_text" value="<?php echo $home_section_2_tabs_data_text;?>" placeholder="Provide your Name" />
                                        <span id="color"><?php echo form_error('home_section_2_tabs_data_text')?></span>
                                    </div>
                                </div>
                               
                                 <div class="form-group" >
                                    <div class="col-md-4">
                                        <input type="hidden" class="form-control"  name="home_section_2_tabs_data_id"  value="<?php echo  $home_section_2_tabs_data_id;?>"  />
                                        <input  type="hidden" class="form-control" name="old_image" value="<?php echo  $image ?>">
                                    </div>
                                </div>
                                    
                                <div class="row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                 <img src="<?php echo base_url()?>assets1/images/<?php  echo $image;  ?>" > 
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists" name="photo"> Change </span>
                                                    <input type="file" name="photo"> </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php 
                                    }

                                } 
                    ?>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->





