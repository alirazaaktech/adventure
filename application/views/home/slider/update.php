<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase"> Section_1 Update  Why choose us

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/Home_slider/update_data');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                        <div>
                          
                                <h3>Provide details</h3>
                                <?php
                                if(isset($data) && !empty($data))
                                {
                                 foreach ($data as $value) 
                                 {

                                     $home_slider_id = $value['home_slider_id'];
                                     $name = $value['name'];
                                     $role = $value['role'];
                                    $country = $value['country'];
                                    $home_slider_p = $value['home_slider_p'];
        
                                    $image = $value['home_slider_img'];
                                    
                                    
                                 ?>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Name
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="name" value="<?php echo $name; ?>" placeholder="Provide your Heading" />
                                        <span id="color"><?php echo form_error('name')?></span>
                                    </div>
                                </div>
                                   <div class="form-group">
                                    <label class="control-label col-md-3">Role
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="role"  value="<?php echo $role; ?>" placeholder="Provide your Heading" />
                                        <span id="color"><?php echo form_error('role')?></span>
                                    </div>
                                </div>
                                   <div class="form-group">
                                    <label class="control-label col-md-3">country
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="country"  value="<?php echo $country; ?>" placeholder="Provide your Heading" />
                                        <span id="color"><?php echo form_error('country')?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Paragraph
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea cols="45" rows="4" required name="home_slider_p"  value="<?php echo $home_slider_p; ?>" placeholder="Provide your Paragraph"><?php echo $home_slider_p; ?></textarea>
                                        <span id="color"><?php echo form_error('home_slider_p')?></span>
                                    </div>
                           
                                
                                <div class="form-group" >
                                    <div class="col-md-4">
                                        <input type="hidden" class="form-control"  name="home_slider_id"  value="<?php echo  $home_slider_id;?>"  />
                                        <input  type="hidden" class="form-control" name="old_image" value="<?php echo  $image ?>">
                                    </div>
                                </div>
                        
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                     <img src="<?php echo base_url()?>assets1/images/<?php  echo $image;  ?>" > 
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists" name="photo"> Change </span>
                                        <input type="file" name="photo"> </span>
                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                        </div>
                        <?php 
                                    }

                                } 
                    ?>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->





