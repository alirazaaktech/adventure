<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_data')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_data'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>Section Slider
                                </div>
                            </div>
                            <div class="portlet-body">
                                <a href="<?php echo base_url('admin/home_slider/add'); ?>" class="btn blue" type="button" >ADD</a>&nbsp;&nbsp;
                                    
                                <div class="table-scrollable">
                                    
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="5%">ID</th>
                                                <th width="10%">Name</th>
                                                <th width="10%">Role</th>
                                                <th width="10%">Country</th>
                                                
                                                <th width="45%">Paragraph</th>
                                                <th width="10%">Image</th>
                                                <th width="25%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($slider) && !empty($slider))
                                                {
                                                    $count = 1;
                                                    foreach($slider as $value)
                                                    {   
                                                        $home_slider_id =  $value['home_slider_id'];
                                                         $home_slider_p =  $value['home_slider_p'];
                                                          $home_slider_img =  $value['home_slider_img'];
                                                           $name =  $value['name'];
                                                           $role =  $value['role'];
                                                           $country =  $value['country'];

                                                          
                                            ?>
                                                        <tr>
                                                             <td><?php echo $count++; ?></td>
                                                            <td><?php echo $name; ?></td>
                                                            <td><?php echo $role; ?></td>
                                                            <td><?php echo $country; ?></td>
                                                            <td><?php echo $home_slider_p; ?></td>
                                                            
                                                            <td>
                                                                <img src="<?php echo base_url() ?>assets1/images/<?php echo $home_slider_img; ?>" style="max-width:150px; max-height:70px;">
                                                            </td>
                                                            <td>
                                                                <a href="<?php echo base_url('admin/home_slider/update/'.$home_slider_id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                                <a href="<?php echo base_url('admin/home_slider/delete/'.$home_slider_id); ?>" class="btn blue" type="button" >Delete</a> &nbsp;
                                                            </td>

                                                        </tr>
                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
