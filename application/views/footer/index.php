<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <?php if($this->session->flashdata('success_update')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_update'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_user')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_user'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>Footer
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                
                                                
                                               
                                                <th width="12%">footer_text</th>
                                                <th width="8%">footer_newsletter</th>
                                                <th width="16%">footer_linkedin</th>
                                                <th width="8%">footer_twitter</th>
                                                <th width="16%">footer_facebook</th>
                                                <th width="16%">footer_power_by</th>
                                                 <th width="12%">footer_logo_image</th>
                                                  <th width="12%">footer_background_image</th>
                                                <th width="22%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($footer) && !empty($footer))
                                                {
                                                    foreach($footer as $value)
                                                    {   

                                                        $index_footer_id = $value['index_footer_id'];
                                                        $index_footer_logo_image =$value['index_footer_logo_image'];
                                                        $index_footer_background_image =$value['index_footer_background_image'];
                                                        $index_footer_text =$value['index_footer_text'];
                                                        $index_footer_newsletter =$value['index_footer_newsletter'];
                                                        $index_footer_twitter =$value['index_footer_twitter'];
                                                        $index_footer_linkedin =$value['index_footer_linkedin'];
                                                        $index_footer_facebook = $value['index_footer_facebook'];
                                                        $index_footer_power_by = $value['index_footer_power_by'];     
                                            ?>
                                                        <tr>
                                                            <td>
                                                                <button type="button" class="btn btn-info pull-center" onclick="open_modal('<?php echo  $index_footer_text; ?>');">  <i class='fa fa-pencil fa-2x'></i>
                                                                </button>
                                                                
                                                            </td>
                                                            <td><?php echo  $index_footer_newsletter; ?></td>
                                                            <td><?php echo   $index_footer_twitter; ?></td>
                                                            <td><?php echo   $index_footer_linkedin; ?></td>
                                                             <td><?php echo  $index_footer_facebook; ?></td>
                                                            <td><?php echo  $index_footer_power_by; ?></td>
                                                            <td>
                                                             <img src="<?php echo base_url() ?>assets1/images/<?php echo $index_footer_logo_image; ?>" style="width:70px;height:60px" >
                                                            </td>
                                                             <td>
                                                             <img src="<?php echo base_url() ?>assets1/images/<?php echo $index_footer_background_image; ?>" style="width:70px;height:60px" >
                                                            </td>
                                                            <td>
                                                                <a href="<?php echo base_url('admin/footer/update/'.$index_footer_id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                            </td>

                                                        </tr>

                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>

                                    <div id="myModal" class="modal fade" role="dialog">
                                      <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h3 class="modal-title">Footer Text</h3>
                                        </div>
                                        <div class="modal-body" >
                                            <ul>
                                                <li id="feedback1"></li>
                                            </ul>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

<script type="text/javascript">
    function open_modal(feedback)
    {
        $('#feedback1').html(feedback);
        $('#myModal').modal('show');
    }

</script>