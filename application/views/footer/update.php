<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Footer Update

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/footer/update_data');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                        <div>
                          
                          
                            <?php
                            if(isset($footer_update) && !empty($footer_update))
                            {

                             foreach ($footer_update as $value) 
                             {
                               $index_footer_id = $value['index_footer_id'];
                                $index_footer_logo_image =$value['index_footer_logo_image'];
                                $index_footer_background_image =$value['index_footer_background_image'];
                                $index_footer_text =$value['index_footer_text'];
                                $index_footer_newsletter =$value['index_footer_newsletter'];
                                $index_footer_twitter =$value['index_footer_twitter'];
                                $index_footer_linkedin =$value['index_footer_linkedin'];
                                $index_footer_facebook = $value['index_footer_facebook'];
                                $index_footer_power_by = $value['index_footer_power_by'];                 
                        
                             ?>
                              <div class="form-group">
                                    <label class="control-label col-md-3">footer_text
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea cols="42" rows="3" required name="index_footer_text" value="<?php echo $index_footer_text ; ?>"  placeholder="Your Feedback"><?php echo $index_footer_text ; ?></textarea>
                                        <span id="color"><?php echo form_error('index_footer_text')?></span>
                                    </div>
                                </div>
                                   <div class="form-group">
                                    <label class="control-label col-md-3">footer_newsletter
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" value="<?php echo $index_footer_newsletter ; ?>" name="index_footer_newsletter" placeholder="+339 70 73 66 64" />
                                        <span id="color"><?php echo form_error('index_footer_newsletter')?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">footer_twitter
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" value="<?php echo $index_footer_twitter ; ?>" name="index_footer_twitter" placeholder="Stay with us" />
                                        <span id="color"><?php echo form_error('index_footer_twitter')?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">footer_linkedin
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" value="<?php echo $index_footer_linkedin ; ?>" name="index_footer_linkedin" placeholder="You gonna love it!
" />
                                        <span id="color"><?php echo form_error('index_footer_linkedin')?></span>
                                    </div>
                                </div>
                              <div class="form-group">
                                    <label class="control-label col-md-3">footer_facebook
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" value="<?php echo $index_footer_facebook ; ?>" name="index_footer_facebook" placeholder="You gonna love it!
" />
                                        <span id="color"><?php echo form_error('index_footer_facebook')?></span>
                                    </div>
                                </div>
                              <div class="form-group">
                                    <label class="control-label col-md-3">footer_power_by
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" readonly class="form-control" value="<?php echo $index_footer_power_by ; ?>" name="index_footer_power_by" placeholder="You gonna love it!
" />
                                        <span id="color"><?php echo form_error('index_footer_power_by')?></span>
                                    </div>
                                </div>
                              
                                                      
                               <div class="form-group">
                                
                                <div class="col-md-4">
                                     <input type="hidden" name="old_image" value="<?php echo  $index_footer_logo_image ?>">
                                      <input type="hidden" name="old_image_background" value="<?php echo  $index_footer_background_image ?>">
                                    <input type="hidden" class="form-control" value="<?php echo $index_footer_id; ?>"  name="index_footer_id" />
                                </div>
                            </div>
                            <div class="form-group">
                            
                                <label class="control-label col-md-3">Logo
                                        <span class="required"> * </span>
                                </label></div>
                                <div class="row">
                                <div class="col-md-3"></div>
                                      <div class="col-md-3">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                 <img src="<?php echo base_url()?>assets1/images/<?php  echo $index_footer_logo_image;  ?>" > 
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists" name="photo"> Change </span>
                                                    <input type="file"  name="photo"> </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                       <div class="col-md-3">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                 <img src="<?php echo base_url()?>assets1/images/<?php  echo $index_footer_background_image;  ?>" > 
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                            <div>
                                                <span class="btn default btn-file">
                                                    <span class="fileinput-new"> Select image </span>
                                                    <span class="fileinput-exists" name="background_photo"> Change </span>
                                                    <input type="file"  name="background_photo"> </span>
                                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>


                        </div>

                        <?php
                             }
                        }
                            ?>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </form>                                
    </div>
</div>
</div>