

<div class="page-header page-header--style-1 page-header--title-align-left page-header--light" style=" background:url('<?php echo base_url() ?>assets1/images/<?php echo $head_section[2]['head_section_image']; ?>');">
			<div class="container">
				<div class="page-header__breadcrumbs breadcrumbs breadcrumbs--page-header-style-1">
					<span property="itemListElement" typeof="ListItem"><a class="home" href="index.html" property="item" title="Go to advice." typeof="WebPage"><i class="mdi mdi-home"></i></a></span>
					<meta content="1" property="position"><span class="breadcrumbs__separator"><span class="mdi mdi-chevron-right"></span></span><span property="itemListElement" typeof="ListItem"><span property="name"><?php echo $head_section[2]['head_section_h'] ?></span></span>
					<meta content="2" property="position">
				</div>
				<div class="page-header__heading">
					<h1 class="page-header__title"><?php echo $head_section[2]['head_section_h']; ?></h1>
				</div>
			</div>
			<div class="page-header__overlay"></div>
		</div>
		<br>
		<br><br>
		<div class="content content--main">
			<div class="vc_row wpb_row vc_row-fluid vc_custom_1494424995336">
				<div class="a-vc-container container">
					<div class="row">

						<div class="wpb_column vc_column_container vc_col-sm-6">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="a-heading vc_custom_1494424844792 a-heading--align-left">
										<h3 class="a-heading__title"><?php echo $section_1[0]['sec_in_h']; ?></h3>
									</div>
									<br>
									<div class="a-text-block vc_custom_1494424982933 a-text-block--simple" style="font-size:18px;line-height:1.89">
										<p><?php echo $section_1[0]['sec_in_p']; ?></p>
									</div>
									<!-- <div class="a-text-block a-text-block--simple" style="font-size:18px;line-height:1.89">
										<p>Whether you’re an experienced professional or a recent graduate, working with Advice could be a challenging and rewarding next step in your career.</p>
									</div> -->
								</div>
							</div>
						</div>
						<div class="wpb_column vc_column_container vc_col-sm-6">
							<div class="vc_column-inner">
								<div class="wpb_wrapper">
									<div class="advice-space a-js-space" data-lg="" data-md="" data-sm="35" data-xl="" data-xs="35">
									</div>
									
									<div class="a-video vc_custom_1495434797931 a-video--simple-card a-video--simple-card-style-1 a-video--align-right a-video--play">
										<div class="a-video__inner"> 
											<iframe width="470" height="264" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" type="text/html" __idm_frm__="37" src="<?php echo $section_1[0]['sec_in_video']; ?>" __idm_id__="1012332545">
												
											</iframe> 
											<a href="#" class="a-video__action a-js-video-play" data-video-src="<?php echo $section_1[0]['sec_in_video']; ?>" style="background-image:url(https://advice.owl.team/orion/wp-content/uploads/sites/2/2017/01/Layer-63.jpg)"> 
												<span class="a-video__action-overlay" style="background-color:rgba(38,37,55,0.1)">
												</span>
												<span class="a-video__action-icon mdi mdi-play-circle"></span> 
											</a>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
			<br>
			<br>
			<hr style="width: 88%; margin: 0 auto;">			
			<br>

		

									<div class="a-heading vc_custom_1494567531621 a-heading--align-center">
										<h2 class="a-heading__title">Our vacancies</h2>
									</div>
									<br>
						
<div class="col-sm-1"></div>
													<tr class=" col-sm-11">
														<form name="top_heading"  method="post">
												<td>
												
													
	  <select id="select_1" style="width: 270px; margin-right: 10px; padding: 8px;"><!-- <option></option><option value="366" data-label-is-value="">Energy &amp; Resources</option><option value="367" data-label-is-value="">Consumer Packaged Goods</option></select>  -->

														
														<!-- <select  name="top_heading" class="top_heading"> -->
															<option>Industries</option>
															<?php
																	if(isset($vacancies_top_heading) && !empty($vacancies_top_heading))
																	{
																		foreach ($vacancies_top_heading as $key) 
																		{
															?>
																			<option value="<?php echo $key['top_heading'];?>" >
																				<?php echo $key['top_heading'];?>
																			</option>
																					
															<?php	
																		}
																	} 
												      		?>
														</select>
													

													</td>
													<td>
														<select class="target j_1" id="select_2" style="width: 270px; margin-right: 10px; padding: 8px;">

															<option>Location</option>
															<?php
																	if(isset($vacancies_location) && !empty($vacancies_location))
																	{
																		foreach ($vacancies_location as $key) 
																		{
															?>
																			<option id="drop" value="<?php echo $key['location'];?>" >
																				<?php echo $key['location'];?>
																			</option>
																					
															<?php	
																		}
																	} 
												      		?>
														</select>
													
													</td>
													<td >

														<select class="target j_1" id="select_3" style="width: 270px; margin-right: 10px; padding: 8px;">

															<option>Departments</option>
															<?php
																	if(isset($vacancies_inner_heading) && !empty($vacancies_inner_heading))
																	{
																		foreach ($vacancies_inner_heading as $key) 
																		{
															?>
																			<option id="drop" value="<?php echo $key['inner_heading'];?>" >
																				<?php echo $key['inner_heading'];?>
																			</option>
																					
															<?php	
																		}
																	} 
												      		?>
														</select>
													
													</td>
													<td>
													

														<select  class="target j_1" id="select_4" style="width: 270px; margin-right: 10px; padding: 8px;">

															<option>Roles</option>
															<?php
																	if(isset($vacancies_top_heading) && !empty($vacancies_top_heading))
																	{
																		foreach ($vacancies_top_heading as $key) 
																		{
															?>
																			<option id="drop" value="<?php echo $key['top_heading'];?>" >
																				<?php echo $key['top_heading'];?>
																			</option>
																					
															<?php	
																		}
																	} 
												      		?>
														</select>
													
													</td>
												</form>
											</tr> 
											
									</div>
									<div class="a-jobs" data-params="{&quot;posts_per_page&quot;:3}">
										
											<ul class="a-jobs__list" id="jobs_list">
												<?php
														if(isset($vacancies_section) && !empty($vacancies_section))
														{
															
															
															foreach ($vacancies_section as $key) 
															{
																
												?>

																<li class="a-jobs__item a-job a-job--list-view">
																	<h3 class="a-job__title">
																		<a class="a-job__title-link" href="#" title="Environmental Field Consultant"><?php echo $key['top_heading']; ?></a>
																	</h3>
																	<div class="a-job__department">
																		<?php echo $key['inner_heading']; ?>
																	</div>
																	<div class="a-job__excerpt">
																		<?php echo $key['paragraph']; ?>
																		
																	</div>
																	<div class="a-job__location">
																		<span class="a-job__location-icon mdi mdi-map-marker"></span>
																		<?php echo $key['location'];?>
																	</div>
																</li>


												<?php 		
															
															
															}

															
														} 
												?>
												<form name="top_heading"  method="post">
													<input type="hidden" id="myload" value="<?php if(isset($vacancies_section) && !empty($vacancies_section)){
														echo $vacancies_section[0]['id'];} ?>">

												
										
													<!--		<div class="a-jobs__load-more a-js-jobs-load-more button button--outline-muted button--icon-right" >
															Load more<span class="button__icon mdi mdi-chevron-down"></span>
														</div> -->
															
												<div class="a-jobs__footer"> 
													<a id="data_load"   data-params="{&quot;posts_per_page&quot;:3,&quot;post_count&quot;:3}" class="a-jobs__load-more a-js-jobs-load-more button button--outline-muted button--icon-right">Load more<span class="button__icon mdi mdi-chevron-down"></span>
													</a>
												</div>
												</form>

											</ul>
											<br><br>
												
		

<script type="text/javascript">
$(document).ready(function(){
		$("#data_load").on('click', function(event) {
			var selectedValue =  $('#myload').val();

			if(selectedValue!=''){
       	      var url =  '<?php echo base_url("Careers/get_all_data");?>';
	         $.ajax({
	           url: url,
	           type: "POST",
	           dataType: 'json',
	           data: {all_data : selectedValue},
	           success: function (data) {
	               if(data.response){
	                 $('#jobs_list').empty();
	                 $.each(data.results, function (key, value) {

	                 	var single_data = '<li class="a-jobs__item a-job a-job--list-view"><h3 class="a-job__title"><a class="a-job__title-link" href="#" title="Environmental Field Consultant">'+ value.top_heading +'</a></h3><div class="a-job__department">'+ value.inner_heading +'</div><div class="a-job__excerpt">'+ value.paragraph +'</div><div class="a-job__location"><span class="a-job__location-icon mdi mdi-map-marker"></span>'+ value.location +'</div></li>' ;
	                      
	                      $('#jobs_list').append(single_data);
	                 });

	               }
	               else{

	               }
	           }
	         });
        }


			
		});
	});
</script>
			
	<script type="text/javascript">
		$(document).ready(function(){
    $("#select_1").on('change', function(event) {
       var selectedValue =  $(this).val();
       //alert(selectedValue);

       if(selectedValue!=''){
       	      var url =  '<?php echo base_url("Careers/get_value_by_ajax");?>';
	         $.ajax({
	           url: url,
	           type: "POST",
	           dataType: 'json',
	           data: {top_heading : selectedValue},
	           success: function (data) {
	               if(data.response){
	                 $('#jobs_list').empty();
	                 $.each(data.results, function (key, value) {

	                 	var single_data = '<li class="a-jobs__item a-job a-job--list-view"><h3 class="a-job__title"><a class="a-job__title-link" href="#" title="Environmental Field Consultant">'+ value.top_heading +'</a></h3><div class="a-job__department">'+ value.inner_heading +'</div><div class="a-job__excerpt">'+ value.paragraph +'</div><div class="a-job__location"><span class="a-job__location-icon mdi mdi-map-marker"></span>'+ value.location +'</div></li>' ;
	                      
	                      $('#jobs_list').append(single_data);
	                 });

	               }
	               else{

	               }
	           }
	         });
        }
       
   });


});

	$(document).ready(function(){
		$("#select_2").on('change', function(event) {
			var selectedValue =  $(this).val();
			//alert(selectedValue);

			if(selectedValue!=''){
       	      var url =  '<?php echo base_url("Careers/get_value_by_ajax");?>';
	         $.ajax({
	           url: url,
	           type: "POST",
	           dataType: 'json',
	           data: {top_heading : selectedValue},
	           success: function (data) {
	               if(data.response){
	                 $('#jobs_list').empty();
	                 $.each(data.results, function (key, value) {

	                 	var single_data = '<li class="a-jobs__item a-job a-job--list-view"><h3 class="a-job__title"><a class="a-job__title-link" href="#" title="Environmental Field Consultant">'+ value.top_heading +'</a></h3><div class="a-job__department">'+ value.inner_heading +'</div><div class="a-job__excerpt">'+ value.paragraph +'</div><div class="a-job__location"><span class="a-job__location-icon mdi mdi-map-marker"></span>'+ value.location +'</div></li>' ;
	                      
	                      $('#jobs_list').append(single_data);
	                 });

	               }
	               else{

	               }
	           }
	         });
        }


		});
	});
	$(document).ready(function(){
		$("#select_3").on('change', function(event) {
			var selectedValue =  $(this).val();
			//alert(selectedValue);

			if(selectedValue!=''){
       	      var url =  '<?php echo base_url("Careers/get_value_by_ajax");?>';
	         $.ajax({
	           url: url,
	           type: "POST",
	           dataType: 'json',
	           data: {top_heading : selectedValue},
	           success: function (data) {
	               if(data.response){
	                 $('#jobs_list').empty();
	                 $.each(data.results, function (key, value) {

	                 	var single_data = '<li class="a-jobs__item a-job a-job--list-view"><h3 class="a-job__title"><a class="a-job__title-link" href="#" title="Environmental Field Consultant">'+ value.top_heading +'</a></h3><div class="a-job__department">'+ value.inner_heading +'</div><div class="a-job__excerpt">'+ value.paragraph +'</div><div class="a-job__location"><span class="a-job__location-icon mdi mdi-map-marker"></span>'+ value.location +'</div></li>' ;
	                      
	                      $('#jobs_list').append(single_data);
	                 });

	               }
	               else{

	               }
	           }
	         });
        }
		});
	});
	$(document).ready(function(){
		$("#select_4").on('change', function(event) {
			var selectedValue =  $(this).val();
			//alert(selectedValue);

			if(selectedValue!=''){
       	      var url =  '<?php echo base_url("Careers/get_value_by_ajax");?>';
	         $.ajax({
	           url: url,
	           type: "POST",
	           dataType: 'json',
	           data: {top_heading : selectedValue},
	           success: function (data) {
	               if(data.response){
	                 $('#jobs_list').empty();
	                 $.each(data.results, function (key, value) {

	                 	var single_data = '<li class="a-jobs__item a-job a-job--list-view"><h3 class="a-job__title"><a class="a-job__title-link" href="#" title="Environmental Field Consultant">'+ value.top_heading +'</a></h3><div class="a-job__department">'+ value.inner_heading +'</div><div class="a-job__excerpt">'+ value.paragraph +'</div><div class="a-job__location"><span class="a-job__location-icon mdi mdi-map-marker"></span>'+ value.location +'</div></li>' ;
	                      
	                      $('#jobs_list').append(single_data);
	                 });

	               }
	               else{

	               }
	           }
	         });
        }
		});
	});
	

	
	</script>