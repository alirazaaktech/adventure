<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase"> Section_1 Update  Careers

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/Career_section_1/update_data');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                        
                          
                                <h3>Provide details</h3>
                                <?php
                                if(isset($data) && !empty($data))
                                {
                                 foreach ($data as $value) 
                                 {

                                     $sec_in_id = $value['sec_in_id'];
                                     $sec_in_h = $value['sec_in_h'];
                                    $sec_in_p = $value['sec_in_p'];
        
                                    $sec_in_video = $value['sec_in_video'];
                                    
                                    
                                 ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Heading
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="sec_in_h" value="<?php echo  $sec_in_h;?>" placeholder="Provide your Heading" />
                                        <span id="color"><?php echo form_error('sec_in_h')?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Paragraph
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea cols="42" rows="3" required name="sec_in_p" value="<?php echo  $sec_in_p;?>" placeholder="Provide your Paragraph"><?php echo  $sec_in_p;?></textarea>
                                        <span id="color"><?php echo form_error('sec_in_p')?></span>
                                    </div>
                                </div>

                                 <div class="form-group">
                                    <label class="control-label col-md-3">Video Url
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea cols="42" rows="3" required name="sec_in_video" value="<?php echo  $sec_in_video;?>" placeholder="Provide your Paragraph"><?php echo  $sec_in_video;?></textarea>
                                        <span id="color"><?php echo form_error('sec_in_video')?></span>
                                    </div>
                                </div>
                           
                                
                                <div class="form-group" >
                                    <div class="col-md-4">
                                        <input type="hidden" class="form-control"  name="sec_in_id"  value="<?php echo  $sec_in_id;?>"  />
                                    </div>
                                </div>
                        
                           
                        <?php 
                                    }

                                } 
                    ?>

                       

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->





