<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_data')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_data'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>Section 1 Careers
                                </div>
                            </div>
                            <div class="portlet-body">
                                <a href="<?php echo base_url('admin/Career_section_1/add'); ?>" class="btn blue" type="button" >ADD</a>&nbsp;&nbsp;
                                    
                                <div class="table-scrollable">
                                    
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="5%">ID</th>
                                                <th width="10%">Heading</th>
                                                <th width="45%">Paragraph</th>
                                                <th width="10%">Video</th>
                                                <th width="25%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($section_1) && !empty($section_1))
                                                {
                                                    $count = 1;
                                                    foreach($section_1 as $value)
                                                    {   
                                                        $sec_in_id =  $value['sec_in_id'];
                                                         $sec_in_h =  $value['sec_in_h'];
                                                          $sec_in_p =  $value['sec_in_p'];
                                                           $sec_in_video =  $value['sec_in_video'];
                                                          
                                            ?>
                                                        <tr>
                                                             <td><?php echo $count++; ?></td>
                                                            <td><?php echo $sec_in_h; ?></td>
                                                            <td><?php echo $sec_in_p ?></td>
                                                            <td>
                                                                <iframe width="200" height="150" src="<?php echo $sec_in_video; ?>">
                                                                </iframe>

                                                               
                                                            </td>
                                                            <td>
                                                                <a href="<?php echo base_url('admin/Career_section_1/update/'.$sec_in_id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                                <a href="<?php echo base_url('admin/Career_section_1/delete/'.$sec_in_id); ?>" class="btn blue" type="button" >Delete</a> &nbsp;
                                                            </td>

                                                        </tr>
                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
