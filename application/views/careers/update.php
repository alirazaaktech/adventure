   
<div>
    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet light bordered" id="portlet">

        <div class="portlet-body form">
            <form role="form" method="Post" enctype="multipart/form-data" action="<?php echo base_url('admin/Layout/process_updatess') ?>">
               <input type="hidden" name="id" value="<?php  echo $data['id']; ?>">
               <div class="form-body">

                <label for="form_control_1">Heading</label>
                <div class="form-group form-md-line-input">
                    <textarea cols="40" rows="20" name="head_section_h"><?php echo (set_value('head_section_h'))?set_value('head_section_h'):( isset($data['head_section_h'])?$data['head_section_h']:'' ) ?></textarea>

                    <span class="help-block">Some help goes here...</span>
                    <span style="color: red"> <?php echo form_error('head_section_h'); ?></span>
                </div>
            

                <div class="form-body">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="<?php echo base_url()?>assets1/images/<?php echo $data['head_section_image']; ?>" > 
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                        <div>
                            <span class="btn default btn-file">
                                <span class="fileinput-new"> Select image </span>
                                <span class="fileinput-exists" name="head_section_image"> Change </span>
                                <input type="file" name="head_section_image"> </span>
                                <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                            </div>

                        </div>


                        <div class="form-actions noborder">
                            <input type="Submit" class="btn blue" value="Submit">
                            <!-- <button type="button" class="btn default">Cancel</button> -->
                        </div>
                    </div>
                </form>
            </div>
        </div>


    </div>
