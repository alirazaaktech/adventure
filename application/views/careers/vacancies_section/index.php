<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_data')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_data'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>Section Vacancies
                                </div>
                            </div>
                            <div class="portlet-body">
                                <a href="<?php echo base_url('admin/Career_vacancies_section/add'); ?>" class="btn blue" type="button" >ADD</a>&nbsp;&nbsp;
                                    
                                <div class="table-scrollable">
                                    
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th width="5%">ID</th>
                                                <th width="10%">Top Heading</th>
                                                <th width="10%">Inner Heading</th>
                                                <th width="45%">Paragraph</th>
                                                <th width="10%">Location</th>
                                                <th width="25%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($vacancies_section) && !empty($vacancies_section))
                                                {
                                                    $count = 1;
                                                    foreach($vacancies_section as $value)
                                                    {   
                                                        $id =  $value['id'];
                                                         $top_heading =  $value['top_heading'];
                                                          $inner_heading =  $value['inner_heading'];
                                                          $paragraph =  $value['paragraph'];
                                                          $location =  $value['location'];
                                                        
                                                          
                                            ?>
                                                        <tr>
                                                             <td><?php echo $count++; ?></td>
                                                            <td><?php echo $top_heading; ?></td>
                                                            <td><?php echo $inner_heading; ?></td>
                                                            <td>
                                                                <?php echo $paragraph; ?>
                                                            </td>
                                                            <td>
                                                                <?php echo $location; ?>
                                                            </td>
                                                            <td>
                                                                <a href="<?php echo base_url('admin/Career_vacancies_section/update/'.$id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                                <a href="<?php echo base_url('admin/Career_vacancies_section/delete/'.$id); ?>" class="btn blue" type="button" >Delete</a> &nbsp;
                                                            </td>

                                                        </tr>
                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
