<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
            <?php if($this->session->flashdata('success_update')) { ?>
                    <div class="alert alert-success">
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success_update'); ?>
                    </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_user')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_user'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
                    
                        <div class="portlet box green">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class=""></i>Top NavBar
                                </div>
                            </div>


                            <div class="portlet-body">
                                <div class="table-scrollable">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                
                                                <th width="8%">navbar_info</th>
                                                <th width="12%">navbar_phone</th>
                                                <!-- <th width="12%">navbar_text_1</th> -->
                                                <!-- <th width="8%">navbar_text_2</th> -->
                                                <th width="16%">navbar_logo_image</th>
                                                <!-- <th width="16%">navbar_background_image</th> -->
                                                <th width="22%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                if(isset($top_nav) && !empty($top_nav))
                                                {
                                                    foreach($top_nav as $value)
                                                    {   

                                                        $index_navbar_id = $value['index_navbar_id'];
                                                        $index_navbar_logo_image =$value['index_navbar_logo_image'];
                                                        // $index_navbar_background_image =$value['index_navbar_background_image'];
                                                        $index_navbar_info =$value['index_navbar_info'];
                                                        $index_navbar_phone =$value['index_navbar_phone']; 
                                                        // $index_navbar_text_1 =$value['index_navbar_text_1'];
                                                        // $index_navbar_text_2 = $value['index_navbar_text_2'];

                                            ?>
                                                        <tr>
                                                            <td><?php echo  $index_navbar_info; ?></td>
                                                            <td><?php echo  $index_navbar_phone; ?></td>
                                                        <!--     <td><?php //echo   $index_navbar_text_1; ?></td>
                                                            <td><?php //echo   $index_navbar_text_2; ?></td> -->
                                                            <td>
                                                             <img src="<?php echo base_url() ?>assets1/images/<?php echo $index_navbar_logo_image; ?>" style="width:70%;height:60%" >
                                                            </td>
                                                            <!--  <td>
                                                             <img src="<?php //echo base_url() ?>assets1/images/<?php //echo $index_navbar_background_image; ?>" style="width:70%;height:6%" >
                                                            </td> -->
                                                            <td>
                                                                <a href="<?php echo base_url('admin/top_navbar/update/'.$index_navbar_id); ?>" class="btn blue" type="button" >Update</a>&nbsp;&nbsp;
                                                            </td>

                                                        </tr>

                                            <?php   
                                                    }
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div> 
                   
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
</div>
