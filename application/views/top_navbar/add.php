<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase"> Top Navbar

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('top_navbar/add_process');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                        <div>
                          
                                <h3>Provide details</h3>
                                <div class="form-group">
                                    <label class="control-label col-md-3">index_navbar_info
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="index_navbar_info" placeholder="info@ventureXglobal.com" />
                                        <span id="color"><?php echo form_error('index_navbar_info')?></span>
                                    </div>
                                </div>
                                   <div class="form-group">
                                    <label class="control-label col-md-3">index_navbar_phone
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="index_navbar_phone" placeholder="+339 70 73 66 64" />
                                        <span id="color"><?php echo form_error('index_navbar_phone')?></span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">index_navbar_text_1
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="index_navbar_text_1" placeholder="Stay with us" />
                                        <span id="color"><?php echo form_error('index_navbar_text_1')?></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">index_navbar_text_2
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="index_navbar_text_2" placeholder="You gonna love it!
" />
                                        <span id="color"><?php echo form_error('index_navbar_text_2')?></span>
                                    </div>
                                </div>
                              
                                                      
                             <!--   <div class="form-group">
                                
                                <div class="col-md-4">
                                    <input type="hidden" class="form-control" value="<?php //echo date('y-m-d');?>" name="date" />
                                </div>
                            </div> -->
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""> </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists" name="photo"> Change </span>
                                        <input type="file" name="photo"> </span>
                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                        </div>

                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </form>                                
    </div>
</div>
</div>

<!-- END PAGE BASE CONTENT -->
</div>



<!-- END CONTENT BODY -->





