<style type="text/css">

#color
{
    color: red;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content" style="margin-left: 0px !important; padding: 0px !important">
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="page-toolbar">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-red"></i>
                        <span class="caption-subject font-red bold uppercase">Top Navbar

                        </span>
                    </div>                               
                </div>
                <form class="form-horizontal" action="<?php echo base_url('admin/top_navbar/update_data');?>" enctype= "multipart/form-data" method="POST">
                    <div class="form-body">
                    
                          
                                <h3>Update Top Navebar</h3>
                                <?php
                                if(isset($top_nav_update) && !empty($top_nav_update))
                                {

                                 foreach ($top_nav_update as $value) 
                                 {
                                     $index_navbar_id = $value['index_navbar_id'];
                                     $index_navbar_logo_image = $value['index_navbar_logo_image'];
                                

                                     
                                    $index_navbar_info = $value['index_navbar_info'];
                                    $index_navbar_phone = $value['index_navbar_phone'];
                                                      
                            
                                 ?>
                              <div class="form-group">
                                    <label class="control-label col-md-3">index_navbar_info
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" value="<?php echo $index_navbar_info ; ?>" name="index_navbar_info"  placeholder="info@ventureXglobal.com" />
                                        <span id="color"><?php echo form_error('index_navbar_info')?></span>
                                    </div>
                                </div>
                                   <div class="form-group">
                                    <label class="control-label col-md-3">index_navbar_phone
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" value="<?php echo $index_navbar_phone ; ?>" name="index_navbar_phone" placeholder="+339 70 73 66 64" />
                                        <span id="color"><?php echo form_error('index_navbar_phone')?></span>
                                    </div>
                                </div>

                                     <input type="hidden" name="old_image" value="<?php echo  $index_navbar_logo_image ?>">
                                      <input type="hidden" class="form-control" value="<?php echo $index_navbar_id; ?>"  name="index_navbar_id" />
                              
                            
                            <div class="row">
                               <div class="col-md-3"></div>
                                  <div class="col-md-3">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                             <img src="<?php echo base_url()?>assets1/images/<?php  echo $index_navbar_logo_image;  ?>" > 
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                        <div>
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists" name="photo"> Change </span>
                                                <input type="file"  name="photo"> </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                              
                        <?php
                             }
                        }
                            ?>
                            </div>



                      

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn btn-circle green">Submit</button>
                                    <button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
        </form>                                
    </div>
</div>
</div>