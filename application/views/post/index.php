
<div class="content content--main content--padd-bot-reset">

<div class="page-header page-header--style-2" style="background-image: url('<?php echo base_url() ?>assets1/images/<?php echo $head_section[1]['home_head_section_image']; ?>'); height: 350px; margin-top: 0px;">
			<div class="container">
				<div class="page-header__heading">
					<h1 class="page-header__title" style="padding-top:150px;"> <?php echo $head_section[1]['home_head_section_h'];  ?></h1>
				</div>
			</div>
			
			<div class="page-header__overlay"></div>
		</div>
		<div class="content content--main">
			<div class="container">
				<div class="row">
					<div class="col-xl-9 col-lg-8 col-md-12">
						<div class="posts posts--grid posts--grid-cols-3 posts--has-pagination">
							<div class="posts__row">

                             <?php if (isset($head_sections) && !empty($head_sections)) {
                             	$count = count($head_sections);
                             	$index = 1;
                             	foreach ($head_sections as $head_sections) {
                             		?>

								<div class="posts__item post--card-outline posts__item--large post--large post-256 post type-post status-publish format-standard has-post-thumbnail sticky hentry category-analytics category-analytics-2 category-digital-strategy category-innovation category-innovation-1 category-innovation-1-1 category-innovation-2 tag-hardware tag-hi-tech tag-software tag-transformation" id="post-256" style="width:<?php echo ($index==1 || $index==$count)?'63%':'30%';?>">
									<div class="post__wrap">
										<div class="post__image-container a-object-fit-container">
											<a class="post__link" href=""><img alt="" class="post__image a-js-has-object-fit wp-post-image" height="<?php echo ($index==1 || $index==$count)?'360':'492' ;?>" sizes="<?php echo ($index==1 || $index==$count)?'(max-width: 580px) 100vw, 580px':'(max-width: 480px) 100vw, 480px' ;?>" src="<?php echo base_url() ?>assets1/images/<?php echo $head_sections['home_section_5_post_image']?>" srcset="<?php echo base_url() ?>assets1/images/<?php echo $head_sections['home_section_5_post_image']?>" width="<?php echo ($index==1 || $index==$count)?'580':'480' ;?>"></a>
										</div>
										<div class="post__body">
											<ul class="post__meta a-meta a-meta--list-inline">
												<li class="a-meta__item a-meta__item--author">
													<span class="font-family-heading">by</span> <a class="a-meta__link" href=""><?php echo $head_sections['home_section_5_post_writer']?></a>
												</li>
												<li class="a-meta__item">
													<a class="a-meta__link" href=""><time class="a-meta__date" datetime="2017-02-18T12:02:33+00:00"><?php echo $head_sections['home_section_5_post_date']?></time></a>
												</li>
											</ul>
											<div class="post__title a-font-family-heading">
												<a class="post__title-link" href="" rel="bookmark" title="From hardware to software: How Hi Tech companies can lead a successful transformation"><?php echo $head_sections['home_section_5_post_title']?></a>
											</div>
											<div class="post__tags">
												<a href="" rel="tag">Hardware</a>, <a href="" rel="tag"><?php echo $head_sections['home_section_5_post_cat']?></a>
											</div>
										</div>
									</div>
								</div>

                            <?php 
                             	$index++; 
                             	}
                            } 
                            ?>
								
							</div>
						</div>
						<div class="pagination">
							<div class="pagination__next">
								<a class="next page-numbers" href=""><?php echo $links; ?> </a>
							</div>
							<!-- <div class="pagination__numbers">
								<span class="page-numbers current"><p></p></a>
							</div> -->
						</div>
					</div>
					<div class="col-xl-3 col-lg-4 hidden-md-down">
						<div class="sidebar" role="complementary">
							<div class="widget widget--news widget--mailchimp-subscribe" id="advice-widget-mailchimp-subscribe-3">
								<h3 class="widget__title">Subscribe</h3>
								<div class="mce-subscribe mce-subscribe--style-2">
									<form action="<?php echo base_url('Home/subscribe')?>" class=""  method="post" >
										<div class="mc-subscribe__field-group">
											<div class="mce-subscribe__field">
												<input aria-required="true" autocomplete="off" class="mce-subscribe__input required" name="EMAIL" placeholder="Enter your e-mail address" type="email" value="">
												<div class="mce-subscribe-msg mce-subscribe-msg--success"></div>
												<div class="mce-subscribe-msg mce-subscribe-msg--error"></div>
											</div>
											<div aria-hidden="true" class="mce-subscribe__security" style="position: absolute; left: -5000px;">
												<input name="b_b127b819234fc3583636ebfb3_12a5b62d37" tabindex="-1" type="text" value="">
											</div>
											<div class="mce-subscribe__action">
												<button class="mce-subscribe__submit" name="subscribe" type="submit"><span class="mdi mdi-telegram"></span></button>
											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="widget widget--news widget_categories" id="categories-3">
								<h3 class="widget__title">Categories</h3>
								<ul>
									
									<?php if(isset($categories) && !empty($categories) ){
										foreach ($categories as $key) {
											$cat = $key['home_section_5_post_categorie_text'];

									?>
									<li class="cat-item cat-item-10">
										<a href=""><?php echo $cat; ?></a>
									</li>
									<?php 		
											}
									} 
 									?>
									
								</ul>
							</div>
							<div class="widget widget--news widget--recent-posts" id="advice-recent-posts-2">
								<h3 class="widget__title">Trending Now</h3>
								<ul class="posts posts--recent-list">
									<?php
									    if (isset($section_5_post) && !empty($section_5_post)) 
                                    	{
                                        	foreach ($section_5_post as $key)
                                        	{
										  		$post_title = $key['home_section_5_post_title']; 
	                                            $post_writer = $key['home_section_5_post_writer']; 
	                                            $post_image = $key['home_section_5_post_image'];  
	                                            $post_date = $key['home_section_5_post_date'];
									?>

									<li class="posts__post post--recent-list post-970 post type-post status-publish format-standard has-post-thumbnail hentry category-digital-strategy category-uncategorized tag-health tag-insurance tag-medical tag-usa">
										<div class="post__thumbnail">
											<a class="post__thumbnail-link" href=""><img alt="" class="attachment-140x140 size-140x140 wp-post-image" height="140" sizes="(max-width: 140px) 100vw, 140px" src="<?php echo base_url() ?>assets1/images/<?php echo $post_image; ?>"  width="140"></a>
										</div>
										<div class="post__body">
											<ul class="post__meta a-meta a-meta--list-inline">
												<li class="a-meta__item a-meta__item--author">
													<span class="font-family-heading">by</span> <a class="a-meta__link" href="" title="Anna Kendall"><?php echo $post_writer; ?></a>
												</li>
												<li class="a-meta__item">
													<a class="a-meta__link" href=""><time class="a-meta__date" datetime="2017-05-25T10:41:11+00:00"><?php echo $post_date ?></time></a>
												</li>
											</ul><a class="post__title" href=""><?php echo $post_title ?></a>
										</div>
									</li>
								<?php }
							} ?>
									
								</ul>
							</div>
							
						</div>
					</div>
				</div>
			</div>