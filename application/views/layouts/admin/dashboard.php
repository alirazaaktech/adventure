<!DOCTYPE html>
<html lang="en">
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
        <script src="<?php echo base_url('assets/DataTables/datatables.css');?>"></script> 
        <script src="<?php echo base_url('assets/DataTables/datatables.js');?>"></script>
        <meta charset="utf-8" />
        <title>AK Tech</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #4 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <!--  -->
         
          <link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
<!--  -->
        <link href="<?php echo base_url();?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo base_url();?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url();?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url();?>assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url();?>assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
        <link href="<?php echo base_url();?>/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url();?>/assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
         </head>
    
    <!-- END HEAD -->
    <style>
      .nav-link:focus{
  color: #red!important;
  }

       /*    .page-sidebar .page-sidebar-menu>li.open>a, .page-sidebar .page-sidebar-menu>li:hover>a, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu>li.open>a, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu>li:hover>a {
            background: #f2f6f9;
            color: #000000;
         }

        .page-sidebar-menu .sub-menu li:focus{
           color: #red !important; 
           background-color: red;
        }*/


    </style>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<?php echo base_url('admin/Dashboard/dashboard');?>">
                        <img src="<?php echo base_url();?>/assets/layouts/layout4/img/Logo2.png" alt="logo" class="logo-default" style="width:150px;margin-top: 2px " /> </a>

                    <div class="menu-toggler sidebar-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN PAGE ACTIONS -->
                <!-- DOC: Remove "hide" class to enable the page header actions -->
                <!-- END PAGE ACTIONS -->
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    <!-- BEGIN HEADER SEARCH BOX -->
                    <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
                  
                    <!-- END HEADER SEARCH BOX -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="separator hide"> </li>
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <!-- DOC: Apply "dropdown-hoverable" class after "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                            <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
                             <li class="dropdown dropdown-extended dropdown-tasks dropdown-dark" id="header_task_bar">
                             <!--    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="icon-bell" style="font-size:18px;"></span>
                                    <span class="label label-pill label-danger count" style="border-radius:10px;"></span>
                                </a> -->
                                <ul class="dropdown-menu extended tasks">       
                                    <a href="<?php echo base_url('')?>" style="text-decoration: none;">                          
                                    <li>
                                        <ul class="dropdown-menu-list"></ul>
                                    </li>
                              </a>
                                </ul>
                            </li>
                            <!-- END NOTIFICATION DROPDOWN -->
                           
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                          
                            <!-- END INBOX DROPDOWN -->
                         
                            <!-- BEGIN TODO DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            
                            <!-- END TODO DROPDOWN -->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                           <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile uppercase" style="margin-right: 10px">  <?php echo $this->session->userdata('user_name') ?> </span><span><img src="<?php echo base_url('assets/login.png')?>" style="width: 10px" ></span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                    
                                    <img alt="" class="img-circle" src="<?php echo base_url('assets1/images/')?><?php echo $this->session->userdata('image') ?>"/></a>
                                    <img alt="" class="img-circle" src="<?php echo base_url('')?>"/></a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="<?php echo base_url('admin/profile/index');?>">
                                        <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li class="divider"> $profile </li>
                                 <!--    <li>
                                        <a href="page_user_lock_1.html">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li> -->
                                    <li>
                                        <a href="<?php echo base_url('admin/login/unset_session')?>">
                                            <i class="fa fa-sign-out"></i> Log Out </a>

                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                           
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

                    
                        <li class="nav-item start active open">
                            <a href="<?php echo base_url('admin/Dashboard/dashboard');?>" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                                <span class="selected"></span>
                            </a>
                        </li>
                      
                        <li class="heading">
                            <h3 class="uppercase">USER</h3>
                        </li>
                        <li class="nav-item <?php echo (activate_menu('user') !='')?'active':''; ?> ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="glyphicon  glyphicon-user" ></i>
                                <span class="title">User</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                          
                              <!--   <li class="nav-item  ">
                                    <a href="<?php //echo base_url('admin/user/add');?>" class="nav-link ">
                                        <span class="title">Add</span>
                                    </a>
                                </li> -->
                     
                
                                <li class="nav-item <?php echo (activate_sub_menu('admin/user/index') != '' || activate_sub_menu('admin/user/add') != '' )?'active':'';?>">
                                    <a href="<?php echo base_url('admin/user/index');?>" class="nav-link ">
                                        <span class="title">View</span>
                                    </a>
                                </li>
                               
                            </ul>
                        </li>
                       <!--   <li class="heading">
                            <h3 class="uppercase">Home</h3>
                        </li> -->

                        <li class="nav-item <?php echo (activate_menu('top_navbar') !=''|| activate_menu('Subscriber')!='' || activate_menu('home_section_1') != ''|| activate_menu('home_section_2')!=''|| activate_menu('home_section_3')!=''|| activate_menu('home_section_4')!=''|| activate_menu('Home_slider')!=''|| activate_menu('home_section_5')!=''|| activate_menu('Timesection')!=''|| activate_menu('Twiter')!=''|| activate_menu('LayoutHome')!='' || activate_menu('footer')!='')?'active':''; ?> ">

                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="glyphicon  glyphicon-home" ></i>
                                <span class="title">Home Page</span>
                                <span class="arrow"></span>
                            </a>

                            <ul class="sub-menu">
                          
                             <li class="nav-item  <?php echo (activate_sub_menu('top_navbar/index') != '' || activate_sub_menu('top_navbar/add') != '' )?'active':'';?> "> 
                                    <a href="<?php echo base_url('admin/top_navbar');?>" class="nav-link ">
                                        <span class="title">Top Navbar</span>
                                    </a>
                                </li>

                                   <li class="nav-item  <?php echo (activate_sub_menu('LayoutHome/index') != '' || activate_sub_menu('LayoutHome/add') != '' )?'active':'';?> "> 
                                    <a href="<?php echo base_url('admin/LayoutHome/');?>" class="nav-link ">
                                        <span class="title">Set Layout</span>
                                    </a>
                                </li>
                                 <li class="nav-item <?php echo (activate_sub_menu('Subscriber/index') != '' || activate_sub_menu('Subscriber/add') != '' )?'active':'';?>"> 
                                    <a href="<?php echo base_url('admin/Subscriber');?>" class="nav-link ">
                                        <span class="title">Sbscriber</span>
                                    </a>
                                </li>

                                   <li class="nav-item <?php echo (activate_sub_menu('home_section_1/index') != '' || activate_sub_menu('home_section_1/add') != '' )?'active':'';?>"> 
                                    <a href="<?php echo base_url('admin/home_section_1');?>" class="nav-link ">
                                        <span class="title">Why choose us</span>
                                    </a>
                                </li>
                                 
                           
                                <li class="nav-item <?php echo (activate_sub_menu('home_section_2/index') != '' || activate_sub_menu('home_section_2/add') != '' )?'active':'';?>"> 
                                    <a href="<?php echo base_url('admin/home_section_2');?>" class="nav-link ">
                                        <span class="title">Industries</span>
                                    </a>
                                </li>
                                      
                                

                                 <li class="nav-item <?php echo (activate_sub_menu('home_section_3/index') != '' || activate_sub_menu('home_section_3/add') != '' )?'active':'';?>"> 
                                    <a href="<?php echo base_url('admin/home_section_3');?>" class="nav-link ">
                                        <span class="title">Industries serve</span>
                                    </a>
                                </li>


                                  <li class="nav-item <?php echo (activate_sub_menu('home_section_4/index') != '' || activate_sub_menu('home_section_4/add') != '' )?'active':'';?>"> 
                                    <a href="<?php echo base_url('admin/home_section_4');?>" class="nav-link ">

                                        <span class="title">Expert team</span>
                                    </a>
                                </li>

                                  <li class="nav-item <?php echo (activate_sub_menu('Home_slider/index') != '' || activate_sub_menu('Home_slider/add') != '' )?'active':'';?>"> 
                                    <a href="<?php echo base_url('admin/Home_slider');?>" class="nav-link ">
                                        <span class="title">Home Slider</span>
                                    </a>
                                </li>
                                
                                
                                   <li class="nav-item <?php echo (activate_sub_menu('Timesection/index') != '' || activate_sub_menu('Timesection/add') != '' )?'active':'';?>"> 
                                    <a href="<?php echo base_url('admin/Timesection/index');?>" class="nav-link ">
                                        <span class="title">Add Office Section</span>
                                    </a>
                                </li>
                                    <li class="nav-item <?php echo (activate_sub_menu('Twiter/index') != '' || activate_sub_menu('Twiter/add') != '' )?'active':'';?>"> 
                                    <a href="<?php echo base_url('admin/Twiter');?>" class="nav-link ">
                                        <span class="title">Twiter Slider</span>
                                    </a>
                                </li>


                                 <li class="nav-item <?php echo (activate_menu('home_section_5') !='' || activate_menu('LayoutHome') !='')?'active':''; ?> ">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <i class="glyphicon  glyphicon-user" ></i>
                                        <span class="title">Our publications</span>
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item <?php echo (activate_sub_menu('home_section_5/index') != '' || activate_sub_menu('home_section_5/add') != '' )?'active':'';?>">
                                            <a href="<?php echo base_url('admin/home_section_5');?>" class="nav-link ">
                                                <span class="title">Post</span>
                                            </a>
                                        </li>
                                 <li class="nav-item  <?php echo (activate_sub_menu('LayoutHome/post') != '' || activate_sub_menu('LayoutHome/add') != '' )?'active':'';?> "> 
                                    <a href="<?php echo base_url('admin/LayoutHome/post');?>" class="nav-link ">
                                        <span class="title">Set post Layout</span>
                                    </a>
                                </li>
                                        <li class="nav-item <?php echo (activate_sub_menu('home_section_5/all_article') != '' || activate_sub_menu('home_section_5/all_article') != '' )?'active':'';?>">
                                            <a href="<?php echo base_url('admin/home_section_5/all_article');?>" class="nav-link ">
                                                <span class="title">Aritcle</span>
                                            </a>
                                        </li>
                                        <li class="nav-item <?php echo (activate_sub_menu('home_section_5/all_cat') != '' || activate_sub_menu('home_section_5/all_cat') != '' )?'active':'';?>">
                                            <a href="<?php echo base_url('admin/home_section_5/all_cat');?>" class="nav-link ">
                                                <span class="title">Categories</span>
                                            </a>
                                        </li>
                                
                                    </ul>
                                </li>
                
                                <li class="nav-item  <?php echo (activate_menu('footer') !='')?'active':''; ?> ">
                                    <a href="<?php echo base_url('admin/footer');?>" class="nav-link ">
                                        <span class="title">Footer</span>
                                    </a>
                                </li>
                               
                               
                            </ul>
                        </li>


                         <li class="nav-item  <?php echo (activate_menu('About') !=''|| activate_menu('Value')!='' || activate_menu('History') != ''|| activate_menu('Layout') != ''|| activate_menu('Partner')!=''|| activate_menu('License')!='')?'active':''; ?> ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="glyphicon  glyphicon-user" ></i>
                                <span class="title">About Page</span>
                                <span class="arrow"></span>

                            </a>
                            <ul class="sub-menu">
                          
                                <li class="nav-item <?php echo (activate_sub_menu('About/index') != '' || activate_sub_menu('About/add') != '' )?'active':'';?>">

                                    <a href="<?php echo base_url('admin/About/index');?>" class="nav-link ">
                                        <span class="title">Top Section</span>
                                    </a>
                                </li>
                                  <li class="nav-item <?php echo (activate_sub_menu('Layout/aboutus') != '')?'active':'';?>">
                                    <a href="<?php echo base_url('admin/Layout/aboutus');?>" class="nav-link ">
                                        <span class="title">Set Layout</span>
                                    </a>
                                </li>
                                   <li class="nav-item <?php echo (activate_sub_menu('Value/index') != '' || activate_sub_menu('Value/add') != '' )?'active':'';?>">
                                    <a href="<?php echo base_url('admin/Value/index');?>" class="nav-link ">
                                        <span class="title">Value Section</span>
                                    </a>
                                </li>
                                   <li class="nav-item <?php echo (activate_sub_menu('History/index') != '' || activate_sub_menu('History/add') != '' )?'active':'';?>">
                                    <a href="<?php echo base_url('admin/History/index');?>" class="nav-link ">
                                        <span class="title">History Section</span>
                                    </a>
                                </li>
                                 <li class="nav-item <?php echo (activate_sub_menu('Partner/index') != '' || activate_sub_menu('Partner/add') != '' )?'active':'';?>">
                                    <a href="<?php echo base_url('admin/Partner/index');?>" class="nav-link ">
                                        <span class="title">Partner Section</span>
                                    </a>
                                </li>
                                    <li class="nav-item <?php echo (activate_sub_menu('License/index') != '' || activate_sub_menu('License/add') != '' )?'active':'';?>">
                                    <a href="<?php echo base_url('admin/License/index');?>" class="nav-link ">
                                        <span class="title">License Section</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                         <li class="nav-item <?php echo (activate_menu('AddContect') !='' || activate_menu('Layout') !='')?'active':''; ?> ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="glyphicon  glyphicon-user" ></i>
                                <span class="title">Contect Us Page</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                          
                                <li class="nav-item <?php echo (activate_sub_menu('AddContect/index') != '' || activate_sub_menu('AddContect/add') != '' )?'active':'';?>">
                                    <a href="<?php echo base_url('admin/AddContect/index');?>" class="nav-link ">
                                        <span class="title">Add Contect</span>
                                    </a>
                                </li>
                               <li class="nav-item <?php echo (activate_sub_menu('Layout/contectus') != '')?'active':'';?>">
                                    <a href="<?php echo base_url('admin/Layout/contectus');?>" class="nav-link ">
                                        <span class="title">Set Layout</span>
                                    </a>
                                </li>
                              </ul>
                        </li>
                        <li class="nav-item  <?php echo (activate_menu('Career_section_1') !='' || activate_menu('Layout') !='')?'active':''; ?> ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="glyphicon  glyphicon-user" ></i>
                                <span class="title">Careers Page</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item <?php echo (activate_sub_menu('Career_section_1/index') != '' || activate_sub_menu('Career_section_1/add') != '' )?'active':'';?>">

                                    <a href="<?php echo base_url('admin/Career_section_1');?>" class="nav-link ">
                                        <span class="title">Top Section</span>
                                    </a>
                                </li>
                                             <li class="nav-item <?php echo (activate_sub_menu('Career_vacancies_section/index') != '' || activate_sub_menu('Career_vacancies_section/add') != '' )?'active':'';?>">

                                    <a href="<?php echo base_url('admin/Career_vacancies_section');?>" class="nav-link ">
                                        <span class="title">Vacancies Section</span>
                                    </a>
                                </li> 
                                <li class="nav-item <?php echo (activate_sub_menu('Layout/carears') != '')?'active':'';?>">
                                    <a href="<?php echo base_url('admin/Layout/carears');?>" class="nav-link ">
                                        <span class="title">Set Layout</span>
                                    </a>
                                </li>      
                               
                               
                            </ul>
                        </li>
                     

                      
                    </ul>   

                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->

            <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">  
                 <!-- BEGIN CONTENT BODY -->                   
                 <div class="page-content" style="margin-top :-10px; " >
                    

                 
                            {_yield}
                      
                       
                 </div>
                 <!-- END CONTENT -->
                </div>
           <!-- END CONTENT BODY -->  
          
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div style="text-align: center;">
        <div class="page-footer" >
            <div class="page-footer-inner"> Powered by
                <a target="_blank" href="http://aktechzone.com">Ak Teck company</a> &nbsp;|&nbsp;
               
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN QUICK NAV -->
        
        <div class="quick-nav-overlay"></div>
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?php echo base_url();?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/morris/morris.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/morris/raphael-min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/themes/patterns.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amcharts/themes/chalk.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jquery.sparkline.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url();?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url();?>assets/pages/scripts/dashboard.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url();?>assets/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>/assets/pages/scripts/profile.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
