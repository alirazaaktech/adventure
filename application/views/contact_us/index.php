	

<div class="page-header page-header--style-1 page-header--title-align-left page-header--light" style=" background:url('<?php echo base_url() ?>assets1/images/<?php echo $head_section[0]['head_section_image']; ?> ');">
			<div class="container">
				<div class="page-header__breadcrumbs breadcrumbs breadcrumbs--page-header-style-1">
					<span property="itemListElement" typeof="ListItem"><a class="home" href="index.html" property="item" title="Go to advice." typeof="WebPage"><i class="mdi mdi-home"></i></a></span>
					<meta content="1" property="position"><span class="breadcrumbs__separator"><span class="mdi mdi-chevron-right"></span></span><span property="itemListElement" typeof="ListItem"><span property="name"><?php echo $head_section[0]['head_section_h'] ?></span></span>
					<meta content="2" property="position">
				</div>
				<div class="page-header__heading" style="flex-direction: column;
    display: flex;
    height: 380px;
    align-items: center;
    justify-content: center;">
					<h1 style="color: #fff;"><?php echo $head_section[0]['head_section_h']; ?></h1>
					<p style="    width: 45%;
    color: #fff;
    text-align: center;"><?php echo $head_section[0]['head_section_p'] ?></p>
				</div>
			</div>
			<div class="page-header__overlay"></div>
		</div>
		<div class="vc_row wpb_row vc_row-fluid vc_custom_1495182790078 vc_row-no-padding" data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" style="position: relative; left: 15px; box-sizing: border-box; width: 100%;margin-top:-115px;">
			<div class="wpb_column vc_column_container vc_col-sm-12">
				<div class="vc_column-inner">
					<div class="wpb_wrapper">
						<div class="a-contact-form a-contact-form--style-1">
							<div class="a-contact-form__form-wrapper form-wrapper form-wrapper--contact-form-style-1">
								<div class="container">
									<div class="form-wrapper__inner">
										<div class="a-contact-form__form-container form-container form-container--contact-form-style-1">
											<div class="form-container__header">
												<h3 class="form-container__title">Make an appointment</h3>
												<div class="form-container__subtitle">
													Fill out the form below to recieve a free and confidential intial consultation.
												</div>
											</div>

											<div class="form-container__form">
												<div class="wpcf7" dir="ltr" id="wpcf7-f835-p98-o1" lang="en-US" role="form">
													<div class="screen-reader-response"></div>
													 <?php if($this->session->flashdata('success_message')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
                </div>
            <?php } ?>
              <?php if($this->session->flashdata('success_user')) { ?>
                <div class="alert alert-success">
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_user'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('error_message')) { ?>
                <div class="alert alert-danger">
                    <strong>Error!</strong> <?php echo $this->session->flashdata('error_message'); ?>
                </div>
            <?php } ?>
            <?php if($this->session->flashdata('warning_message')) { ?>
                <div class="alert alert-warning">
                    <strong>Warning!</strong> <?php echo $this->session->flashdata('warning_message'); ?>
                </div>
            <?php } ?>
            <div class="portlet-body form">
                <?php 
                if(isset($error_message))
                    {?>
                        <div class="alert alert-danger">
                            <strong>Error!</strong> <?php echo $error_message; ?>
                        </div>                    
                        <?php 
                    } ?>
                </div>
													<form action="<?php echo base_url('Contact_us/process_add'); ?>" class="wpcf7-form" method="post">
														<div style="display: none;">
															<input name="_wpcf7" type="hidden" value="835"> <input name="_wpcf7_version" type="hidden" value="5.0.2"> <input name="_wpcf7_locale" type="hidden" value="en_US" > <input name="_wpcf7_unit_tag" type="hidden" value="wpcf7-f835-p98-o1"> <input name="_wpcf7_container_post" type="hidden" value="98">
														</div>
														<div class="row">
															<div class="col-sm-6 col-xs-12">
																<span class="wpcf7-form-control-wrap your-name">
																	<input class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" name="name" placeholder="Name *" size="40" type="text"  required/>
																</span><br>
																<span class="wpcf7-form-control-wrap your-email"><input aria-invalid="false"  class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" name="email" placeholder="E-mail *" size="40" type="email" required/></span><br>
																<span class="wpcf7-form-control-wrap your-phone-number"><input aria-invalid="false"  class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" name="phone" placeholder="Phone *" size="40" type="tel" value="" required/></span>
															</div>
															<div class="col-sm-6 col-xs-12">
																<span class="wpcf7-form-control-wrap your-message">
																<textarea aria-invalid="false"  class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" cols="40" name="message" placeholder="Message *" rows="10" required/></textarea></span><br>
																<button class="button button--primary button--icon-right" type="submit">Submit <i class="button__icon mdi mdi-trending-neutral"></i></button>

															</div>
														</div>

														<div class="wpcf7-response-output wpcf7-display-none"></div>
													</form>
												</div>
											</div>
										</div>
										<div class="a-contact-form__contact-details-container contact-details-container contact-details-container--contact-form-style-1" style="background-image:url()">
											<div class="contact-details-container__header">
												<h3 class="contact-details-container__title">Contacts</h3>
												<div class="contact-details-container__subtitle">
													Feel free to contacts with us
												</div>
											</div>
											<ul class="a-contact-form__contact-details contact-details contact-details--vertical contact-details--circle contact-details--light contact-details--contact-form-style-1">
												<li class="contact-details__item">
													<span class="contact-details__item-icon mdi mdi-phone"></span>
													<div class="contact-details__item-content">
														<a class="contact-details__item-link" href="tel:+1212-736-3100"><?php echo (isset($products[0]['phone'])?($products[0]['phone']):''); ?></a>
													</div>
												</li>
												<li class="contact-details__item">
													<span class="contact-details__item-icon mdi mdi-email-variant"></span>
													<div class="contact-details__item-content">
														<a class="contact-details__item-link" href="mailto:usa@example.com"><?php echo (isset($products[0]['email'])?($products[0]['email']):''); ?></a>
													</div>
												</li>
												<li class="contact-details__item">
													<span class="contact-details__item-icon mdi mdi-map-marker"></span>
													<div class="contact-details__item-content">
														<span class="contact-details__item-text" id="pac-input"><?php echo (isset($products[0]['address'])?($products[0]['address']):''); ?></span>
													</div>
													
  
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
				

							<div class="a-contact-form__map a-map a-map--contact-form-style-1">		
								<?php 
										$address=$products[0]['address'];
										echo '<iframe frameborder="0" width="100%" height="800" src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=' . str_replace(",", "", str_replace(" ", "+", $address)) . '&z=14&output=embed" ></iframe>';
								?>					
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>

