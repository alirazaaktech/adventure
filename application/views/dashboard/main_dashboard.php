
            <!-- BEGIN CONTENT -->
            
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
<!-- 
                        <div class="page-title">
                            <h1> Dashboard
                                <small>statistics, charts, recent events and reports</small>
                            </h1>
                        </div> -->
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   <!--  <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="#">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span class="active">Dashboard</span>
                        </li>
                    </ul> -->
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <!-- BEGIN DASHBOARD STATS 1-->
                    <div class="row">
                         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 grey-gallery" href="<?php echo base_url('admin/Home_section_5'); ?>">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo (isset($total_post)?$total_post:'0');?>"><?php echo (isset($total_post)?$total_post:'0');?> </span>
                                        <span class="glyphicon glyphicon-signal"></span>
                                        <!-- <i class="glyphicon glyphicon-signal"></i> -->
                                        <!-- <span class="title" style="margin-left: 20px">Done Follow Up</span>  -->
                                    </div>
                                    <div class="desc">Total Post</div>
                                </div>
                            </a>
                        </div>
                         <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 blue-chambray" href="<?php ?>">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo (isset($total_industries)?$total_industries:'0');?>"><?php echo (isset($total_industries)?$total_industries:'0');?></span>
                                        <i class="fa fa-institution" style="margin-left: 5px" ></i>
                                        <!-- <span class="title" style="margin-left: 20px">Done Follow Up</span>  -->
                                    </div>
                                    <div class="desc">Total Industries</div>
                                </div>
                            </a>
                        </div>
                        
                        
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" href="<?php ?>">
                                <div class="visual">
                                    <i class="fa fa-shopping-cart"></i>
                                </div>
                                <div class="details">
                                   
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php echo (isset($our_officess)?$our_officess:'0');?>"><?php echo (isset($our_officess)?$our_officess:'0');?> </span>
                                        <i class="fa fa-clock-o" style="margin-left: 5px" ></i>
                                    </div>
                                    <div class="desc">Our Offices</div>
                                </div>
                            </a>
                        </div>
                       <!--  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 purple" href="<?php ?>">
                                <div class="visual">
                                    <i class="fa fa-globe"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="<?php ?>">echo (isset($total_industries)?$total_industries:'0');?><?php ?></span>
                                         <i class="fa fa-phone fa 1px" style="margin-left: 5px"></i>

                                    </div>
                                    <div class="desc"> ABC </div>
                                </div>
                            </a>
                        </div> -->
                    </div>
                   