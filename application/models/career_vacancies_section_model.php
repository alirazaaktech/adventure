
<?php 
/** 
* SBP Admins Model 
*
* Model to manage admins/users table 
*
* @package 		Admin Pannel Authentication 
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://punjabsportsboard.com
*/
include_once('abstract_model.php');

class Career_vacancies_section_model extends Abstract_model 
{

    protected $table_name = "careers_vacancies_section";
	protected $is_error;
	public $admin_exists;
	public $admin_salt;
	public $admin_info;

	//Model Constructor
    function __construct() 
    {
        $this->table_name = "careers_vacancies_section";
		parent::__construct();
    }
    public function all_heading($table)
    {   
        $query = $this->db->get($table);
        return $query->result_array(); 
    }
    public function delete_data($table,$colum,$sec_in_id)
    {
        $this->db->where($colum, $sec_in_id);
        $this->db->delete($table);
    }
     public function insert_data($data) 
    {
        $this->db->insert($this->table_name, $data);
        return true;
    }

    public function specfic_data($table,$industry_id)
    {

        $this->db->where('top_heading',$industry_id);
        $this->db->or_where( 'location',$industry_id);
        $this->db->or_where('inner_heading',$industry_id); 
        $query = $this->db->get($table);
        return $query->result_array();
        // echo $this->db->last_query(); 
        // exit;
    }

    public function get_search($job_industries,$job_location,$job_department,$job_role)
    { 
        echo $job_industries;
        echo "<br>"; 
        echo $job_location;
         echo "<br>";  
        echo $job_department;
         echo "<br>";  
        echo $job_role;

        exit;
        $this->db->select();
        $this->db->from('careers_vacancies_section');
        if(isset($job_industries) && !empty($job_industries)){
            $this->db->where('top_heading',$job_industries);
        }

        if(isset($job_location) && !empty($job_location)){
            $this->db->where('location',$job_location);
        }

         if(isset($job_department) && !empty($job_department)){
            $this->db->where('inner_heading',$job_department);
        }

        if(isset($job_role) && !empty($job_role)){
            $this->db->where('top_heading',$job_role);
        }
        $query =$this->db->get();
         $query->result_array(); 
        echo $this->db->last_query();
        exit;
    }


    

    
}
?>