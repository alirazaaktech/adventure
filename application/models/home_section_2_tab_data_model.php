<?php 
/** 
* SBP Admins Model 
*
* Model to manage admins/users table 
*
* @package 		Admin Pannel Authentication 
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://punjabsportsboard.com
*/
include_once('abstract_model.php');

class Home_section_2_tab_data_model extends Abstract_model 
{
    protected $table_name = "home_section_2_tabs_data";
	protected $is_error;
	public $admin_exists;
	public $admin_salt;
	public $admin_info;

	//Model Constructor
    function __construct() 
    {
        $this->table_name = "home_section_2_tabs_data";
		parent::__construct();
    }
    public function all_tabs($table,$home_section_2_tab_id)
    {   
        $this->db->where('home_section_2_tab_id',$home_section_2_tab_id);
        $query = $this->db->get($table);
        return $query->result_array(); 
    }
    public function all_data()
    {
        $this->db->select('*');
        $this->db->from('home_section_2_tabs_data');
        $this->db->join('home_section_2_tabs','home_section_2_tabs.home_section_2_tab_id = home_section_2_tabs_data.home_section_2_tab_id');
        // $this->db->where('follow_up.home_section_2_tab_id',$id);
        // $this->db->order_by("follow_up_feedback.feedback_id","desc");
        $data=$this->db->get();
        return $data->result_array();
    }
    public function delete_data($table,$colum,$sec_in_id)
    {
        $this->db->where($colum, $sec_in_id);
        $this->db->delete($table);
    }
     public function insert_data($data) 
    {
        $this->db->insert($this->table_name, $data);
        return true;
    }


    
}
?>