<?php 
/** 
* SBP Admins Model 
*
* Model to manage admins/users table 
*
* @package 		Admin Pannel Authentication 
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://punjabsportsboard.com
*/
include_once('abstract_model.php');

class Footer_model extends Abstract_model 
{

    protected $table_name = "index_footer";
	protected $is_error;
	public $admin_exists;
	public $admin_salt;
	public $admin_info;

	//Model Constructor
    function __construct() 
    {
        $this->table_name = "index_footer";
		parent::__construct();
    }

    public function all_footer($table)
    {
    	$query = $this->db->get($table);
        return $query->result_array(); 
    }

    public function update_all_footer($table,$index_footer_id)
    {
        $this->db->where('index_footer_id',$index_footer_id);
        $query = $this->db->get($table);
        return $query->result_array(); 
    }
    
}
?>