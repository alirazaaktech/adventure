<?php 

include_once('abstract_model.php');
  
//protected $table_name = "";
  
class User_model extends Abstract_model 
{

	public function __construct() 
	{
        $this->table_name = "users";
		    parent::__construct();
    }
     public function insert_user($data) 
	{
        $this->db->insert($this->table_name, $data);
        return true;
    }
    public function delet_data($table,$user_id)
    {
        $this->db->where('user_id', $user_id);
       return $this->db->delete($table); 
    }
    public function password_compare($current_password,$user_id)
    {
        $this->db->select();
        $this->db->from($this->table_name);
        $this->db->where('user_id',$user_id);
        $this->db->where('user_password',$current_password);
        $data= $this->db->get();  
        if($data->num_rows()>0) 
        {
            return $data->result_array();
        }
    }
}
?>