<?php 
/** 
* SBP Admins Model 
*
* Model to manage admins/users table 
*
* @package 		Admin Pannel Authentication 
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://punjabsportsboard.com
*/
include_once('abstract_model.php');

class Home_slider_model extends Abstract_model 
{

    protected $table_name = "home_slider";
	protected $is_error;
	public $admin_exists;
	public $admin_salt;
	public $admin_info;

	//Model Constructor
    function __construct() 
    {
        $this->table_name = "home_slider";
		parent::__construct();
    }

    public function all_footer($table)
    {
    	$query = $this->db->get($table);
        return $query->result_array(); 
    }

     public function delete_data($table,$colum,$id)
    {
        $this->db->where($colum, $id);
        $this->db->delete($table);
    }

    
}
?>