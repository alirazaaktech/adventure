<?php

/** 
* RDA Admins Model 
*
* Model to manage Admins Table 
*
* @package 		Admin Pannel  
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://
*/

include_once('Abstract_model.php');

class Licenese_model extends Abstract_model
{
	/**
	* @var stirng
	* @access protected
	*/
    protected $table_name = "";
	
	/** 
	*  Model constructor
	* 
	* @access public 
	*/
    public function __construct() 
	{
        $this->table_name = "about_license";
		parent::__construct();
    }
    public function delete_data($table,$colum,$sec_in_id)
    {
        $this->db->where($colum, $sec_in_id);
        $this->db->delete($table);
    }
       public function get_para()
    {
    	$this->db->select('heading');
    	 $this->db->From('about_license');
        $this->db->where('id',1);
        $query= $this->db->get();
        // debug($query,true);
        return $query->result_array();
       
    }

  

}