<?php 
/** 
* SBP Admins Model 
*
* Model to manage admins/users table 
*
* @package 		Admin Pannel Authentication 
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://punjabsportsboard.com
*/
include_once('abstract_model.php');

class Home_section_5_model extends Abstract_model 
{

    protected $table_name = "home_section_5_post";
	protected $is_error;
	public $admin_exists;
	public $admin_salt;
	public $admin_info;

	//Model Constructor
    function __construct() 
    {
        $this->table_name = "home_section_5_post";
		parent::__construct();
    }
    public function all_data($table)
    {
         $query = $this->db->get($table);
        return $query->result_array(); 
    }
     
    public function delete_data($table,$colum,$sec_in_id)
    
    {
        $this->db->where($colum, $sec_in_id);
        $this->db->delete($table);
    }
     public function insert_into($table,$data) 
    {
        $this->db->insert($table, $data);
        return true;
    }
    public function specfic_data($table,$colum,$id)
    {
        $this->db->where($colum,$id);
        $query = $this->db->get($table);
        return $query->result_array(); 
    }
    public function limted_colum($table,$limit,$colum,$order)
    {
        $this->db->limit($limit);
        $this->db->order_by($colum,$order);
        $query = $this->db->get($table);
        // echo $this->db->last_query();
        // exit;
         return $query->result_array(); 
    }
    public function row()
    {
     $this->db->from('home_section_5_post');
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        // print_r($rowcount); exit();
        return $rowcount;
    }


     public function show($start,$limit)
    {   
        $this->db->select('*');
        $this->db->from('home_section_5_post');
        $this->db->limit($start, $limit);
        $query = $this->db->get();
        // print_r($query->result()); exit();
        return $query->result_array();
    }

     public function updated_data($table,$colum,$row_id,$data) 
    {
        $this->db->where($colum, $row_id);
        return $this->db->update($table,$data);
    }






   
}
?>

   