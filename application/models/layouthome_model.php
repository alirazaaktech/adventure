<?php

/** 
* RDA Admins Model 
*
* Model to manage Admins Table 
*
* @package 		Admin Pannel  
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://
*/

include_once('Abstract_model.php');

class layouthome_model extends Abstract_model
{
	/**
	* @var stirng
	* @access protected
	*/
    protected $table_name = "";
	
	/** 
	*  Model constructor
	* 
	* @access public 
	*/
    public function __construct() 
	{
        $this->table_name = "home_head_section";
		parent::__construct();
    }
  
        public function get_contects()
    {
        $this->db->select("*");
        $this->db->from('home_head_section');
        $this->db->where('id',1);
       $query= $this->db->get();
       return $query->result_array();
    }

         public function get_contectss()
    {
        $this->db->select("*");
        $this->db->from('home_head_section');
        $this->db->where('id',2);
       $query= $this->db->get();
       return $query->result_array();
    }

  

}