<?php 
/** 
* SBP Admins Model 
*
* Model to manage admins/users table 
*
* @package 		Admin Pannel Authentication 
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://punjabsportsboard.com
*/
include_once('abstract_model.php');

class Top_nav_model extends Abstract_model 
{

    protected $table_name = "index_navbar";
	protected $is_error;
	public $admin_exists;
	public $admin_salt;
	public $admin_info;

	//Model Constructor
    function __construct() 
    {
        $this->table_name = "index_navbar";
		parent::__construct();
    }
    public function all_top_nav($table)
    {
	 	$query = $this->db->get($table);
        return $query->result_array(); 
    }
   
    public function update_top_nav($table,$index_navbar_id)
    {
        $this->db->where('index_navbar_id',$index_navbar_id);
        $query = $this->db->get($table);
        return $query->result_array(); 
    }
 
    
}
?>