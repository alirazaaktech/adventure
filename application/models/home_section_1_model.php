<?php 
/** 
* SBP Admins Model 
*
* Model to manage admins/users table 
*
* @package 		Admin Pannel Authentication 
* @subpackage 	Model
* @author 		Muhammad Khalid<muhammad.khalid@pitb.gov.pk>  
* @link 		http://punjabsportsboard.com
*/
include_once('abstract_model.php');

class Home_section_1_model extends Abstract_model 
{

    protected $table_name = "home_section_1";
	protected $is_error;
	public $admin_exists;
	public $admin_salt;
	public $admin_info;

	//Model Constructor
    function __construct() 
    {
        $this->table_name = "home_section_1";
		parent::__construct();
    }
    public function all_heading($table)
    {   
        $query = $this->db->get($table);
        return $query->result_array(); 
    }
    public function delete_data($table,$colum,$sec_in_id)
    {
        $this->db->where($colum, $sec_in_id);
        $this->db->delete($table);
    }
     public function insert_data($data) 
    {
        $this->db->insert($this->table_name, $data);
        return true;
    }



    

    
}
?>